<?php
// GENERATED CODE -- DO NOT EDIT!

namespace Gsbridge;

/**
 */
class FoNotificationServiceClient extends \Grpc\BaseStub {

    /**
     * @param string $hostname hostname
     * @param array $opts channel options
     * @param \Grpc\Channel $channel (optional) re-use channel object
     */
    public function __construct($hostname, $opts, $channel = null) {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * FoChangeNotification will receive a list of ids of the related entity. Entity types could not be mixed.
     * An array will contain only one type on entity.
     * @param \Gsbridge\FoNotificationMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FoChangeNotification(\Gsbridge\FoNotificationMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/gsbridge.FoNotificationService/FoChangeNotification',
        $argument,
        ['\Common\EmptyMessage', 'decode'],
        $metadata, $options);
    }

    /**
     * FoChangeNotificationById will get the id of the entity changed
     * @param \Gsbridge\FoNotificationByIdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FoChangeNotificationById(\Gsbridge\FoNotificationByIdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/gsbridge.FoNotificationService/FoChangeNotificationById',
        $argument,
        ['\Common\EmptyMessage', 'decode'],
        $metadata, $options);
    }

}
