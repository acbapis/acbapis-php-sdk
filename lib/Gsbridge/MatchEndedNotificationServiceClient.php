<?php
// GENERATED CODE -- DO NOT EDIT!

namespace Gsbridge;

/**
 */
class MatchEndedNotificationServiceClient extends \Grpc\BaseStub {

    /**
     * @param string $hostname hostname
     * @param array $opts channel options
     * @param \Grpc\Channel $channel (optional) re-use channel object
     */
    public function __construct($hostname, $opts, $channel = null) {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * @param \Gsbridge\MatchEndedMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function PostMatchEnded(\Gsbridge\MatchEndedMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/gsbridge.MatchEndedNotificationService/PostMatchEnded',
        $argument,
        ['\Common\EmptyMessage', 'decode'],
        $metadata, $options);
    }

}
