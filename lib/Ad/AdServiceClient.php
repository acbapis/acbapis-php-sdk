<?php
// GENERATED CODE -- DO NOT EDIT!

namespace Ad;

/**
 */
class AdServiceClient extends \Grpc\BaseStub {

    /**
     * @param string $hostname hostname
     * @param array $opts channel options
     * @param \Grpc\Channel $channel (optional) re-use channel object
     */
    public function __construct($hostname, $opts, $channel = null) {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * ListGames obtiene la lista de todos los partidos
     * @param \Ad\GameFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListGames(\Ad\GameFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ad.AdService/ListGames',
        $argument,
        ['\Ad\GameArray', 'decode'],
        $metadata, $options);
    }

    /**
     * CreateGame permite la creación de un partido. Este partido tendrá un tratamiento
     * diferente al resto. Se ubicará en una zona temporal hasta su validación.
     * @param \Ad\Game $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateGame(\Ad\Game $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ad.AdService/CreateGame',
        $argument,
        ['\Ad\Game', 'decode'],
        $metadata, $options);
    }

    /**
     * GetGame obtiene los datos básicos del partido junto con los actores
     * que intervienen.
     * @param \Ad\GetGameFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetGame(\Ad\GetGameFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ad.AdService/GetGame',
        $argument,
        ['\Ad\Game', 'decode'],
        $metadata, $options);
    }

    /**
     * UpdateGame envía actualización de los datos de un partido que se producen
     * en pista y modifican la configuración original. El partido se identifica con
     * su identificador único y se envían los datos como una entidad Partido estándar.
     * @param \Ad\Game $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateGame(\Ad\Game $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ad.AdService/UpdateGame',
        $argument,
        ['\Ad\Game', 'decode'],
        $metadata, $options);
    }

    /**
     * DeleteGame elimina un partido
     * @param \Ad\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteGame(\Ad\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ad.AdService/DeleteGame',
        $argument,
        ['\Ad\Game', 'decode'],
        $metadata, $options);
    }

    /**
     * GetGameScoresheetEmails devuelve los emails a los que se envía
     * el acta al finalizar el partido
     * @param \Ad\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetGameScoresheetEmails(\Ad\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ad.AdService/GetGameScoresheetEmails',
        $argument,
        ['\Ad\EmailsArray', 'decode'],
        $metadata, $options);
    }

    /**
     * * Start Designation endpoints *
     *
     * AssignAccount asigna una cuenta de usuario a un partido. Esto permite que la
     * cuenta envíe eventos de ese partido
     * @param \Ad\Designation $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function AssignAccount(\Ad\Designation $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ad.AdService/AssignAccount',
        $argument,
        ['\Ad\Designation', 'decode'],
        $metadata, $options);
    }

    /**
     * ListAssignedAccounts obtiene los usuarios asignados a un partido
     * @param \Ad\DesignationFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListAssignedAccounts(\Ad\DesignationFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ad.AdService/ListAssignedAccounts',
        $argument,
        ['\Ad\DesignationArray', 'decode'],
        $metadata, $options);
    }

    /**
     * ListMyAssignedGames obtiene los partidos asignados al usuario que realiza la petición
     * @param \Ad\MyAssignedGamesFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListMyAssignedGames(\Ad\MyAssignedGamesFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ad.AdService/ListMyAssignedGames',
        $argument,
        ['\Ad\GameArray', 'decode'],
        $metadata, $options);
    }

    /**
     * ListAssignedGames obtiene los partidos asignados a un usuario
     * @param \Ad\AssignedGamesFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListAssignedGames(\Ad\AssignedGamesFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ad.AdService/ListAssignedGames',
        $argument,
        ['\Ad\GameArray', 'decode'],
        $metadata, $options);
    }

    /**
     * ListDesignations obtiene todas las designaciones
     * @param \Ad\DesignationFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListDesignations(\Ad\DesignationFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ad.AdService/ListDesignations',
        $argument,
        ['\Ad\DesignationArray', 'decode'],
        $metadata, $options);
    }

    /**
     * GetDesignation obtiene una designación a partir de su id
     * @param \Ad\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetDesignation(\Ad\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ad.AdService/GetDesignation',
        $argument,
        ['\Ad\Designation', 'decode'],
        $metadata, $options);
    }

    /**
     * UpdateDesignationAccount actualiza el accountId de una designación. También actualiza
     * el accountId de los eventos que hubiera creado el usuario anterior para el
     * partido de la designación original.
     * @param \Ad\Designation $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateDesignationAccount(\Ad\Designation $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ad.AdService/UpdateDesignationAccount',
        $argument,
        ['\Ad\Designation', 'decode'],
        $metadata, $options);
    }

    /**
     * UpdateDesignation actualiza una designación. Si se modifica el accountId también
     * actualiza los eventos que hubiera creado el usuario anterior para el partido
     * de la designación original.
     * @param \Ad\Designation $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateDesignation(\Ad\Designation $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ad.AdService/UpdateDesignation',
        $argument,
        ['\Ad\Designation', 'decode'],
        $metadata, $options);
    }

    /**
     * DeleteDesignation elimina una designación
     * @param \Ad\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteDesignation(\Ad\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ad.AdService/DeleteDesignation',
        $argument,
        ['\Ad\Designation', 'decode'],
        $metadata, $options);
    }

    /**
     * * Start Actor endpoints *
     *
     * CreateActor permite crear un actor y asignarlo a un partido
     * @param \Ad\Actor $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateActor(\Ad\Actor $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ad.AdService/CreateActor',
        $argument,
        ['\Ad\Actor', 'decode'],
        $metadata, $options);
    }

    /**
     * GetActor obtiene los datos de un actor
     * @param \Ad\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetActor(\Ad\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ad.AdService/GetActor',
        $argument,
        ['\Ad\Actor', 'decode'],
        $metadata, $options);
    }

    /**
     * GetActorByLicenseId obtiene un actor a usando un id de licencia
     * @param \Ad\GetLicenseId $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetActorByLicenseId(\Ad\GetLicenseId $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ad.AdService/GetActorByLicenseId',
        $argument,
        ['\Ad\Actor', 'decode'],
        $metadata, $options);
    }

    /**
     * GetReferees obtiene un listado de árbitros activos transformados en actores
     * @param \Ad\RefereeFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetReferees(\Ad\RefereeFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ad.AdService/GetReferees',
        $argument,
        ['\Ad\ActorArray', 'decode'],
        $metadata, $options);
    }

    /**
     * ListActors obtiene un listado de actores
     * @param \Ad\ActorFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListActors(\Ad\ActorFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ad.AdService/ListActors',
        $argument,
        ['\Ad\ActorArray', 'decode'],
        $metadata, $options);
    }

    /**
     * UpdateActor permite modificar la configuración de un actor después
     * de ser creado
     * @param \Ad\Actor $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateActor(\Ad\Actor $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ad.AdService/UpdateActor',
        $argument,
        ['\Ad\Actor', 'decode'],
        $metadata, $options);
    }

    /**
     * DeleteActor elimina un actor
     * @param \Ad\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteActor(\Ad\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ad.AdService/DeleteActor',
        $argument,
        ['\Ad\Actor', 'decode'],
        $metadata, $options);
    }

    /**
     * * Start Filter endpoints *
     *
     * ListSeasons permite obtener un listado de las diferentes temporadas
     * que hay guardadas
     * @param \Ad\EmptyMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListSeasons(\Ad\EmptyMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ad.AdService/ListSeasons',
        $argument,
        ['\Ad\FilterArray', 'decode'],
        $metadata, $options);
    }

    /**
     * ListWeeks permite obtener un listado de las diferentes jornadas
     * que hay guardadas
     * @param \Ad\EmptyMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListWeeks(\Ad\EmptyMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ad.AdService/ListWeeks',
        $argument,
        ['\Ad\FilterArray', 'decode'],
        $metadata, $options);
    }

    /**
     * * Start LicenseRol endpoints *
     *
     * CreateLicenseRol crea un tipo
     * @param \Ad\LicenseRol $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateLicenseRol(\Ad\LicenseRol $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ad.AdService/CreateLicenseRol',
        $argument,
        ['\Ad\LicenseRol', 'decode'],
        $metadata, $options);
    }

    /**
     * UpdateLicenseRol actualiza un tipo
     * @param \Ad\LicenseRol $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateLicenseRol(\Ad\LicenseRol $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ad.AdService/UpdateLicenseRol',
        $argument,
        ['\Ad\LicenseRol', 'decode'],
        $metadata, $options);
    }

    /**
     * FindLicenseRolByID obtiene un tipo por ID
     * @param \Ad\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindLicenseRolByID(\Ad\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ad.AdService/FindLicenseRolByID',
        $argument,
        ['\Ad\LicenseRol', 'decode'],
        $metadata, $options);
    }

    /**
     * ListLicenseRoles obtiene todos los roles disponibles
     * @param \Ad\EmptyMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListLicenseRoles(\Ad\EmptyMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ad.AdService/ListLicenseRoles',
        $argument,
        ['\Ad\LicenseRolArray', 'decode'],
        $metadata, $options);
    }

    /**
     * DeleteLicenseRol elimina un tipo
     * @param \Ad\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteLicenseRol(\Ad\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ad.AdService/DeleteLicenseRol',
        $argument,
        ['\Ad\LicenseRol', 'decode'],
        $metadata, $options);
    }

    /**
     * * Start LicenseType endpoints *
     *
     * CreateLicenseType crea un tipo
     * @param \Ad\LicenseType $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateLicenseType(\Ad\LicenseType $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ad.AdService/CreateLicenseType',
        $argument,
        ['\Ad\LicenseType', 'decode'],
        $metadata, $options);
    }

    /**
     * UpdateLicenseType actualiza un tipo
     * @param \Ad\LicenseType $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateLicenseType(\Ad\LicenseType $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ad.AdService/UpdateLicenseType',
        $argument,
        ['\Ad\LicenseType', 'decode'],
        $metadata, $options);
    }

    /**
     * FindLicenseTypeByID obtiene un tipo por ID
     * @param \Ad\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindLicenseTypeByID(\Ad\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ad.AdService/FindLicenseTypeByID',
        $argument,
        ['\Ad\LicenseType', 'decode'],
        $metadata, $options);
    }

    /**
     * ListLicenseTypes obtiene todos los tipos disponibles
     * @param \Ad\EmptyMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListLicenseTypes(\Ad\EmptyMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ad.AdService/ListLicenseTypes',
        $argument,
        ['\Ad\LicenseTypeArray', 'decode'],
        $metadata, $options);
    }

    /**
     * DeleteLicenseType elimina un tipo
     * @param \Ad\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteLicenseType(\Ad\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ad.AdService/DeleteLicenseType',
        $argument,
        ['\Ad\LicenseType', 'decode'],
        $metadata, $options);
    }

    /**
     * * Start Team endpoints *
     *
     * GetTeam obtiene los datos de un equipo
     * @param \Ad\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetTeam(\Ad\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ad.AdService/GetTeam',
        $argument,
        ['\Ad\Team', 'decode'],
        $metadata, $options);
    }

    /**
     * UpdateTeam permite modificar la configuración de un equipo
     * @param \Ad\Team $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateTeam(\Ad\Team $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ad.AdService/UpdateTeam',
        $argument,
        ['\Ad\Team', 'decode'],
        $metadata, $options);
    }

    /**
     * * Start Event endpoints *
     *
     * SendEvent envía un evento para su incorporación a la base de datos.
     * Devuelve error si el envío no se ha podido realizar correctamente.
     * @param \Ad\Event $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function SendEvent(\Ad\Event $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ad.AdService/SendEvent',
        $argument,
        ['\Ad\Event', 'decode'],
        $metadata, $options);
    }

    /**
     * UpdateEvent actualiza un evento
     * @param \Ad\Event $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateEvent(\Ad\Event $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ad.AdService/UpdateEvent',
        $argument,
        ['\Ad\Event', 'decode'],
        $metadata, $options);
    }

    /**
     * ListEvents obtiena la lista de todos los eventos. Permite especificar desde qué evento
     * y/o hasta qué evento se quiere obtener.
     * @param \Ad\EventFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListEvents(\Ad\EventFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ad.AdService/ListEvents',
        $argument,
        ['\Ad\EventArray', 'decode'],
        $metadata, $options);
    }

    /**
     * ListEventStream obtiene una conexión persistente con el servidor en la que recibe los eventos
     * de todos los partidos
     *
     * Los eventos se reciben en tiempo real. Si se desea obtener eventos anteriores, utilizar
     * la función ListEvents(EventFilter) EventArray.
     * @param \Ad\EventFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListEventStream(\Ad\EventFilter $argument,
      $metadata = [], $options = []) {
        return $this->_serverStreamRequest('/ad.AdService/ListEventStream',
        $argument,
        ['\Ad\Event', 'decode'],
        $metadata, $options);
    }

    /**
     * SendEventStream envía un stream de eventos para su incorporación a la base de datos.
     * Devuelve el mensaje de error si se produce algún problema durante el envío.
     * Este método mantiene una conexión persistente con el servidor hasta que no hay más eventos que enviar.
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function SendEventStream($metadata = [], $options = []) {
        return $this->_clientStreamRequest('/ad.AdService/SendEventStream',
        ['\Ad\EmptyMessage','decode'],
        $metadata, $options);
    }

    /**
     * ListEventsByGame obtiene la lista de eventos de un partido. Permite especificar desde qué evento
     * y/o hasta qué evento se quiere obtener la lista. Añade un filtro por accountId a partir
     * del token de autorización del usuario.
     * @param \Ad\EventFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListEventsByGame(\Ad\EventFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ad.AdService/ListEventsByGame',
        $argument,
        ['\Ad\EventArray', 'decode'],
        $metadata, $options);
    }

    /**
     * ListEventStreamByGame obtiene una conexión persistente con el servidor en la que recibe los eventos
     * correspondientes al partido identificado por su id único.
     *
     * Los eventos se reciben en tiempo real. Si se desea obtener eventos anteriores, utilizar
     * la función ListEvents(EventFilter) EventArray.
     * @param \Ad\EventFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListEventStreamByGame(\Ad\EventFilter $argument,
      $metadata = [], $options = []) {
        return $this->_serverStreamRequest('/ad.AdService/ListEventStreamByGame',
        $argument,
        ['\Ad\Event', 'decode'],
        $metadata, $options);
    }

    /**
     * DeleteEvent elimina un evento
     * @param \Ad\Event $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteEvent(\Ad\Event $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ad.AdService/DeleteEvent',
        $argument,
        ['\Ad\Event', 'decode'],
        $metadata, $options);
    }

    /**
     * TODO (jllopis). Añadir informe. EventData específica del tipo de evento.
     * TODO (jllopis). Añadir firma. EventData específica del tipo de evento.
     *
     * * Start Client endpoints *
     *
     * GetClientLastVersion permite obtener la última versión disponible del cliente oficial.
     * La versión se codifica mediante Semantic versioning v2.0.0
     * @param \Ad\EmptyMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetClientLastVersion(\Ad\EmptyMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ad.AdService/GetClientLastVersion',
        $argument,
        ['\Ad\ClientVersion', 'decode'],
        $metadata, $options);
    }

    /**
     * * Start TemplateConfig endpoints *
     *
     * CreateTemplateConfig crea una configuración
     * @param \Ad\TemplateConfig $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateTemplateConfig(\Ad\TemplateConfig $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ad.AdService/CreateTemplateConfig',
        $argument,
        ['\Ad\TemplateConfig', 'decode'],
        $metadata, $options);
    }

    /**
     * UpdateTemplateConfig actualiza una configuración
     * @param \Ad\TemplateConfig $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateTemplateConfig(\Ad\TemplateConfig $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ad.AdService/UpdateTemplateConfig',
        $argument,
        ['\Ad\TemplateConfig', 'decode'],
        $metadata, $options);
    }

    /**
     * FindTemplateConfigByID obtiene una configuración por ID
     * @param \Ad\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindTemplateConfigByID(\Ad\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ad.AdService/FindTemplateConfigByID',
        $argument,
        ['\Ad\TemplateConfig', 'decode'],
        $metadata, $options);
    }

    /**
     * ListTemplateConfigs obtiene todos los roles disponibles
     * @param \Ad\EmptyMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListTemplateConfigs(\Ad\EmptyMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ad.AdService/ListTemplateConfigs',
        $argument,
        ['\Ad\TemplateConfigArray', 'decode'],
        $metadata, $options);
    }

    /**
     * DeleteTemplateConfig elimina una configuración
     * @param \Ad\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteTemplateConfig(\Ad\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ad.AdService/DeleteTemplateConfig',
        $argument,
        ['\Ad\TemplateConfig', 'decode'],
        $metadata, $options);
    }

    /**
     * * Start ReportTemplate endpoints *
     *
     * CreateReportTemplate crea una configuración
     * @param \Ad\ReportTemplate $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateReportTemplate(\Ad\ReportTemplate $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ad.AdService/CreateReportTemplate',
        $argument,
        ['\Ad\ReportTemplate', 'decode'],
        $metadata, $options);
    }

    /**
     * UpdateReportTemplate actualiza una configuración
     * @param \Ad\ReportTemplate $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateReportTemplate(\Ad\ReportTemplate $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ad.AdService/UpdateReportTemplate',
        $argument,
        ['\Ad\ReportTemplate', 'decode'],
        $metadata, $options);
    }

    /**
     * FindReportTemplateByID obtiene una configuración por ID
     * @param \Ad\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindReportTemplateByID(\Ad\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ad.AdService/FindReportTemplateByID',
        $argument,
        ['\Ad\ReportTemplate', 'decode'],
        $metadata, $options);
    }

    /**
     * ListReportTemplates obtiene todos los roles disponibles
     * @param \Ad\ReportTemplateFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListReportTemplates(\Ad\ReportTemplateFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ad.AdService/ListReportTemplates',
        $argument,
        ['\Ad\ReportTemplateArray', 'decode'],
        $metadata, $options);
    }

    /**
     * DeleteReportTemplate elimina una configuración
     * @param \Ad\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteReportTemplate(\Ad\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ad.AdService/DeleteReportTemplate',
        $argument,
        ['\Ad\ReportTemplate', 'decode'],
        $metadata, $options);
    }

    /**
     * * Start Context endpoints *
     * @param \Ad\EmptyMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListContexts(\Ad\EmptyMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ad.AdService/ListContexts',
        $argument,
        ['\Ad\ContextArray', 'decode'],
        $metadata, $options);
    }

    /**
     * * Start Score Sheet endpoints *
     * @param \Ad\SendScoreSheetMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function SendScoreSheet(\Ad\SendScoreSheetMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ad.AdService/SendScoreSheet',
        $argument,
        ['\Ad\EmptyMessage', 'decode'],
        $metadata, $options);
    }

    /**
     * SendScoreSheetNotification sends the game's scoresheet to everyone involved
     * @param \Ad\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function SendScoreSheetNotification(\Ad\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ad.AdService/SendScoreSheetNotification',
        $argument,
        ['\Ad\EmptyMessage', 'decode'],
        $metadata, $options);
    }

    /**
     * * Start Log endpoints *
     * @param \Ad\LogEntry $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function Log(\Ad\LogEntry $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ad.AdService/Log',
        $argument,
        ['\Ad\IdMessage', 'decode'],
        $metadata, $options);
    }

    /**
     * * Status endpoints *
     * @param \Ad\EmptyMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetServerTime(\Ad\EmptyMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ad.AdService/GetServerTime',
        $argument,
        ['\Ad\ServerTimeMessage', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Ad\EmptyMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetVersion(\Ad\EmptyMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ad.AdService/GetVersion',
        $argument,
        ['\Ad\Version', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Ad\EmptyMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetGlobalServiceStatus(\Ad\EmptyMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ad.AdService/GetGlobalServiceStatus',
        $argument,
        ['\Ad\ServerStatusMessage', 'decode'],
        $metadata, $options);
    }

}
