<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: ad/ad.proto

namespace Ad;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>ad.LogEntry</code>
 */
class LogEntry extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>int64 id = 1;</code>
     */
    private $id = 0;
    /**
     * Generated from protobuf field <code>.ad.LogLevel level = 2;</code>
     */
    private $level = 0;
    /**
     * Generated from protobuf field <code>string message = 3;</code>
     */
    private $message = '';
    /**
     * Generated from protobuf field <code>string deviceId = 4;</code>
     */
    private $deviceId = '';
    /**
     * Generated from protobuf field <code>string accountId = 5;</code>
     */
    private $accountId = '';
    /**
     * Generated from protobuf field <code>string accountLabel = 6;</code>
     */
    private $accountLabel = '';

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type int|string $id
     *     @type int $level
     *     @type string $message
     *     @type string $deviceId
     *     @type string $accountId
     *     @type string $accountLabel
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Ad\Ad::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>int64 id = 1;</code>
     * @return int|string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Generated from protobuf field <code>int64 id = 1;</code>
     * @param int|string $var
     * @return $this
     */
    public function setId($var)
    {
        GPBUtil::checkInt64($var);
        $this->id = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>.ad.LogLevel level = 2;</code>
     * @return int
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Generated from protobuf field <code>.ad.LogLevel level = 2;</code>
     * @param int $var
     * @return $this
     */
    public function setLevel($var)
    {
        GPBUtil::checkEnum($var, \Ad\LogLevel::class);
        $this->level = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string message = 3;</code>
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Generated from protobuf field <code>string message = 3;</code>
     * @param string $var
     * @return $this
     */
    public function setMessage($var)
    {
        GPBUtil::checkString($var, True);
        $this->message = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string deviceId = 4;</code>
     * @return string
     */
    public function getDeviceId()
    {
        return $this->deviceId;
    }

    /**
     * Generated from protobuf field <code>string deviceId = 4;</code>
     * @param string $var
     * @return $this
     */
    public function setDeviceId($var)
    {
        GPBUtil::checkString($var, True);
        $this->deviceId = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string accountId = 5;</code>
     * @return string
     */
    public function getAccountId()
    {
        return $this->accountId;
    }

    /**
     * Generated from protobuf field <code>string accountId = 5;</code>
     * @param string $var
     * @return $this
     */
    public function setAccountId($var)
    {
        GPBUtil::checkString($var, True);
        $this->accountId = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string accountLabel = 6;</code>
     * @return string
     */
    public function getAccountLabel()
    {
        return $this->accountLabel;
    }

    /**
     * Generated from protobuf field <code>string accountLabel = 6;</code>
     * @param string $var
     * @return $this
     */
    public function setAccountLabel($var)
    {
        GPBUtil::checkString($var, True);
        $this->accountLabel = $var;

        return $this;
    }

}

