<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: ad/ad.proto

namespace Ad;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>ad.ReportTemplateArray</code>
 */
class ReportTemplateArray extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>repeated .ad.ReportTemplate reportTemplates = 1;</code>
     */
    private $reportTemplates;
    /**
     * Generated from protobuf field <code>int32 total = 2;</code>
     */
    private $total = 0;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type \Ad\ReportTemplate[]|\Google\Protobuf\Internal\RepeatedField $reportTemplates
     *     @type int $total
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Ad\Ad::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>repeated .ad.ReportTemplate reportTemplates = 1;</code>
     * @return \Google\Protobuf\Internal\RepeatedField
     */
    public function getReportTemplates()
    {
        return $this->reportTemplates;
    }

    /**
     * Generated from protobuf field <code>repeated .ad.ReportTemplate reportTemplates = 1;</code>
     * @param \Ad\ReportTemplate[]|\Google\Protobuf\Internal\RepeatedField $var
     * @return $this
     */
    public function setReportTemplates($var)
    {
        $arr = GPBUtil::checkRepeatedField($var, \Google\Protobuf\Internal\GPBType::MESSAGE, \Ad\ReportTemplate::class);
        $this->reportTemplates = $arr;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int32 total = 2;</code>
     * @return int
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Generated from protobuf field <code>int32 total = 2;</code>
     * @param int $var
     * @return $this
     */
    public function setTotal($var)
    {
        GPBUtil::checkInt32($var);
        $this->total = $var;

        return $this;
    }

}

