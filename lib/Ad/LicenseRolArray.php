<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: ad/ad.proto

namespace Ad;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>ad.LicenseRolArray</code>
 */
class LicenseRolArray extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>repeated .ad.LicenseRol licenseRoles = 1;</code>
     */
    private $licenseRoles;
    /**
     * Generated from protobuf field <code>int32 total = 2;</code>
     */
    private $total = 0;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type \Ad\LicenseRol[]|\Google\Protobuf\Internal\RepeatedField $licenseRoles
     *     @type int $total
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Ad\Ad::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>repeated .ad.LicenseRol licenseRoles = 1;</code>
     * @return \Google\Protobuf\Internal\RepeatedField
     */
    public function getLicenseRoles()
    {
        return $this->licenseRoles;
    }

    /**
     * Generated from protobuf field <code>repeated .ad.LicenseRol licenseRoles = 1;</code>
     * @param \Ad\LicenseRol[]|\Google\Protobuf\Internal\RepeatedField $var
     * @return $this
     */
    public function setLicenseRoles($var)
    {
        $arr = GPBUtil::checkRepeatedField($var, \Google\Protobuf\Internal\GPBType::MESSAGE, \Ad\LicenseRol::class);
        $this->licenseRoles = $arr;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int32 total = 2;</code>
     * @return int
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Generated from protobuf field <code>int32 total = 2;</code>
     * @param int $var
     * @return $this
     */
    public function setTotal($var)
    {
        GPBUtil::checkInt32($var);
        $this->total = $var;

        return $this;
    }

}

