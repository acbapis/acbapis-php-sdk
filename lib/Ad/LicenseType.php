<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: ad/ad.proto

namespace Ad;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * LicenseType Messages
 *
 * Generated from protobuf message <code>ad.LicenseType</code>
 */
class LicenseType extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>int64 id = 1;</code>
     */
    private $id = 0;
    /**
     * Generated from protobuf field <code>int64 externalId = 2;</code>
     */
    private $externalId = 0;
    /**
     * Generated from protobuf field <code>string description = 3;</code>
     */
    private $description = '';
    /**
     * Generated from protobuf field <code>string code = 4;</code>
     */
    private $code = '';

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type int|string $id
     *     @type int|string $externalId
     *     @type string $description
     *     @type string $code
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Ad\Ad::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>int64 id = 1;</code>
     * @return int|string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Generated from protobuf field <code>int64 id = 1;</code>
     * @param int|string $var
     * @return $this
     */
    public function setId($var)
    {
        GPBUtil::checkInt64($var);
        $this->id = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 externalId = 2;</code>
     * @return int|string
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * Generated from protobuf field <code>int64 externalId = 2;</code>
     * @param int|string $var
     * @return $this
     */
    public function setExternalId($var)
    {
        GPBUtil::checkInt64($var);
        $this->externalId = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string description = 3;</code>
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Generated from protobuf field <code>string description = 3;</code>
     * @param string $var
     * @return $this
     */
    public function setDescription($var)
    {
        GPBUtil::checkString($var, True);
        $this->description = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string code = 4;</code>
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Generated from protobuf field <code>string code = 4;</code>
     * @param string $var
     * @return $this
     */
    public function setCode($var)
    {
        GPBUtil::checkString($var, True);
        $this->code = $var;

        return $this;
    }

}

