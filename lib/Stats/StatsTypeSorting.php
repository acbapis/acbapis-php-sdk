<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: stats/stats.proto

namespace Stats;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Sorting fields, every field accepts 'DESC' or 'ASC'
 *
 * Generated from protobuf message <code>stats.StatsTypeSorting</code>
 */
class StatsTypeSorting extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>string id = 1;</code>
     */
    private $id = '';
    /**
     * Generated from protobuf field <code>string description = 2;</code>
     */
    private $description = '';
    /**
     * Generated from protobuf field <code>string codeId = 3;</code>
     */
    private $codeId = '';

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type string $id
     *     @type string $description
     *     @type string $codeId
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Stats\Stats::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>string id = 1;</code>
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Generated from protobuf field <code>string id = 1;</code>
     * @param string $var
     * @return $this
     */
    public function setId($var)
    {
        GPBUtil::checkString($var, True);
        $this->id = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string description = 2;</code>
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Generated from protobuf field <code>string description = 2;</code>
     * @param string $var
     * @return $this
     */
    public function setDescription($var)
    {
        GPBUtil::checkString($var, True);
        $this->description = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string codeId = 3;</code>
     * @return string
     */
    public function getCodeId()
    {
        return $this->codeId;
    }

    /**
     * Generated from protobuf field <code>string codeId = 3;</code>
     * @param string $var
     * @return $this
     */
    public function setCodeId($var)
    {
        GPBUtil::checkString($var, True);
        $this->codeId = $var;

        return $this;
    }

}

