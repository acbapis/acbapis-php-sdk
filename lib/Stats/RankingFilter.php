<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: stats/stats.proto

namespace Stats;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>stats.RankingFilter</code>
 */
class RankingFilter extends \Google\Protobuf\Internal\Message
{
    /**
     **
     * Estadística ranking:
     * 141: assists
     * 128: 1pt throws
     * 14:  1pt percentage
     * 129: 2pt throws
     * 12:  2pt percentage
     * 130: 3pt throws
     * 10:  3pt percentage
     * 142: fouls
     * 143: fouls received
     * 134: dunks
     * 158: plus/minus (más/menos)
     * 140: turnovers
     * 149: points
     * 138: defensive rebounds
     * 135: offensive rebounds
     * 151: rebounds
     * 137: steals
     * 150: seconds played
     * 136: blocks
     * 139: blocks received
     * 152: valoración
     *
     * Generated from protobuf field <code>int64 conceptoId = 1;</code>
     */
    private $conceptoId = 0;
    /**
     * Generated from protobuf field <code>int64 competicionId = 2;</code>
     */
    private $competicionId = 0;
    /**
     * Generated from protobuf field <code>int64 edicionId = 3;</code>
     */
    private $edicionId = 0;
    /**
     * Generated from protobuf field <code>int64 jornadaId = 4;</code>
     */
    private $jornadaId = 0;
    /**
     * Generated from protobuf field <code>int64 partidoId = 5;</code>
     */
    private $partidoId = 0;
    /**
     * Generated from protobuf field <code>int64 equipoId = 6;</code>
     */
    private $equipoId = 0;
    /**
     * Generated from protobuf field <code>repeated int64 faseIds = 7;</code>
     */
    private $faseIds;
    /**
     * Generated from protobuf field <code>int64 entidadId = 8;</code>
     */
    private $entidadId = 0;
    /**
     * Criterio ranking: 1 = media; 2 = máximo; 3 = total
     *
     * Generated from protobuf field <code>int64 tipoRanking = 9;</code>
     */
    private $tipoRanking = 0;
    /**
     * Generated from protobuf field <code>int64 inicio = 10;</code>
     */
    private $inicio = 0;
    /**
     * Generated from protobuf field <code>int64 fin = 11;</code>
     */
    private $fin = 0;
    /**
     * Generated from protobuf field <code>int32 offset = 12;</code>
     */
    private $offset = 0;
    /**
     * Generated from protobuf field <code>int32 limit = 13;</code>
     */
    private $limit = 0;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type int|string $conceptoId
     *          *
     *           Estadística ranking:
     *           141: assists
     *           128: 1pt throws
     *           14:  1pt percentage
     *           129: 2pt throws
     *           12:  2pt percentage
     *           130: 3pt throws
     *           10:  3pt percentage
     *           142: fouls
     *           143: fouls received
     *           134: dunks
     *           158: plus/minus (más/menos)
     *           140: turnovers
     *           149: points
     *           138: defensive rebounds
     *           135: offensive rebounds
     *           151: rebounds
     *           137: steals
     *           150: seconds played
     *           136: blocks
     *           139: blocks received
     *           152: valoración
     *     @type int|string $competicionId
     *     @type int|string $edicionId
     *     @type int|string $jornadaId
     *     @type int|string $partidoId
     *     @type int|string $equipoId
     *     @type int[]|string[]|\Google\Protobuf\Internal\RepeatedField $faseIds
     *     @type int|string $entidadId
     *     @type int|string $tipoRanking
     *           Criterio ranking: 1 = media; 2 = máximo; 3 = total
     *     @type int|string $inicio
     *     @type int|string $fin
     *     @type int $offset
     *     @type int $limit
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Stats\Stats::initOnce();
        parent::__construct($data);
    }

    /**
     **
     * Estadística ranking:
     * 141: assists
     * 128: 1pt throws
     * 14:  1pt percentage
     * 129: 2pt throws
     * 12:  2pt percentage
     * 130: 3pt throws
     * 10:  3pt percentage
     * 142: fouls
     * 143: fouls received
     * 134: dunks
     * 158: plus/minus (más/menos)
     * 140: turnovers
     * 149: points
     * 138: defensive rebounds
     * 135: offensive rebounds
     * 151: rebounds
     * 137: steals
     * 150: seconds played
     * 136: blocks
     * 139: blocks received
     * 152: valoración
     *
     * Generated from protobuf field <code>int64 conceptoId = 1;</code>
     * @return int|string
     */
    public function getConceptoId()
    {
        return $this->conceptoId;
    }

    /**
     **
     * Estadística ranking:
     * 141: assists
     * 128: 1pt throws
     * 14:  1pt percentage
     * 129: 2pt throws
     * 12:  2pt percentage
     * 130: 3pt throws
     * 10:  3pt percentage
     * 142: fouls
     * 143: fouls received
     * 134: dunks
     * 158: plus/minus (más/menos)
     * 140: turnovers
     * 149: points
     * 138: defensive rebounds
     * 135: offensive rebounds
     * 151: rebounds
     * 137: steals
     * 150: seconds played
     * 136: blocks
     * 139: blocks received
     * 152: valoración
     *
     * Generated from protobuf field <code>int64 conceptoId = 1;</code>
     * @param int|string $var
     * @return $this
     */
    public function setConceptoId($var)
    {
        GPBUtil::checkInt64($var);
        $this->conceptoId = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 competicionId = 2;</code>
     * @return int|string
     */
    public function getCompeticionId()
    {
        return $this->competicionId;
    }

    /**
     * Generated from protobuf field <code>int64 competicionId = 2;</code>
     * @param int|string $var
     * @return $this
     */
    public function setCompeticionId($var)
    {
        GPBUtil::checkInt64($var);
        $this->competicionId = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 edicionId = 3;</code>
     * @return int|string
     */
    public function getEdicionId()
    {
        return $this->edicionId;
    }

    /**
     * Generated from protobuf field <code>int64 edicionId = 3;</code>
     * @param int|string $var
     * @return $this
     */
    public function setEdicionId($var)
    {
        GPBUtil::checkInt64($var);
        $this->edicionId = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 jornadaId = 4;</code>
     * @return int|string
     */
    public function getJornadaId()
    {
        return $this->jornadaId;
    }

    /**
     * Generated from protobuf field <code>int64 jornadaId = 4;</code>
     * @param int|string $var
     * @return $this
     */
    public function setJornadaId($var)
    {
        GPBUtil::checkInt64($var);
        $this->jornadaId = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 partidoId = 5;</code>
     * @return int|string
     */
    public function getPartidoId()
    {
        return $this->partidoId;
    }

    /**
     * Generated from protobuf field <code>int64 partidoId = 5;</code>
     * @param int|string $var
     * @return $this
     */
    public function setPartidoId($var)
    {
        GPBUtil::checkInt64($var);
        $this->partidoId = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 equipoId = 6;</code>
     * @return int|string
     */
    public function getEquipoId()
    {
        return $this->equipoId;
    }

    /**
     * Generated from protobuf field <code>int64 equipoId = 6;</code>
     * @param int|string $var
     * @return $this
     */
    public function setEquipoId($var)
    {
        GPBUtil::checkInt64($var);
        $this->equipoId = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>repeated int64 faseIds = 7;</code>
     * @return \Google\Protobuf\Internal\RepeatedField
     */
    public function getFaseIds()
    {
        return $this->faseIds;
    }

    /**
     * Generated from protobuf field <code>repeated int64 faseIds = 7;</code>
     * @param int[]|string[]|\Google\Protobuf\Internal\RepeatedField $var
     * @return $this
     */
    public function setFaseIds($var)
    {
        $arr = GPBUtil::checkRepeatedField($var, \Google\Protobuf\Internal\GPBType::INT64);
        $this->faseIds = $arr;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 entidadId = 8;</code>
     * @return int|string
     */
    public function getEntidadId()
    {
        return $this->entidadId;
    }

    /**
     * Generated from protobuf field <code>int64 entidadId = 8;</code>
     * @param int|string $var
     * @return $this
     */
    public function setEntidadId($var)
    {
        GPBUtil::checkInt64($var);
        $this->entidadId = $var;

        return $this;
    }

    /**
     * Criterio ranking: 1 = media; 2 = máximo; 3 = total
     *
     * Generated from protobuf field <code>int64 tipoRanking = 9;</code>
     * @return int|string
     */
    public function getTipoRanking()
    {
        return $this->tipoRanking;
    }

    /**
     * Criterio ranking: 1 = media; 2 = máximo; 3 = total
     *
     * Generated from protobuf field <code>int64 tipoRanking = 9;</code>
     * @param int|string $var
     * @return $this
     */
    public function setTipoRanking($var)
    {
        GPBUtil::checkInt64($var);
        $this->tipoRanking = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 inicio = 10;</code>
     * @return int|string
     */
    public function getInicio()
    {
        return $this->inicio;
    }

    /**
     * Generated from protobuf field <code>int64 inicio = 10;</code>
     * @param int|string $var
     * @return $this
     */
    public function setInicio($var)
    {
        GPBUtil::checkInt64($var);
        $this->inicio = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 fin = 11;</code>
     * @return int|string
     */
    public function getFin()
    {
        return $this->fin;
    }

    /**
     * Generated from protobuf field <code>int64 fin = 11;</code>
     * @param int|string $var
     * @return $this
     */
    public function setFin($var)
    {
        GPBUtil::checkInt64($var);
        $this->fin = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int32 offset = 12;</code>
     * @return int
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     * Generated from protobuf field <code>int32 offset = 12;</code>
     * @param int $var
     * @return $this
     */
    public function setOffset($var)
    {
        GPBUtil::checkInt32($var);
        $this->offset = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int32 limit = 13;</code>
     * @return int
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * Generated from protobuf field <code>int32 limit = 13;</code>
     * @param int $var
     * @return $this
     */
    public function setLimit($var)
    {
        GPBUtil::checkInt32($var);
        $this->limit = $var;

        return $this;
    }

}

