<?php
// GENERATED CODE -- DO NOT EDIT!

namespace Stats;

/**
 */
class AccionServiceClient extends \Grpc\BaseStub {

    /**
     * @param string $hostname hostname
     * @param array $opts channel options
     * @param \Grpc\Channel $channel (optional) re-use channel object
     */
    public function __construct($hostname, $opts, $channel = null) {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * @param \Stats\Accion $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateAccion(\Stats\Accion $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.AccionService/CreateAccion',
        $argument,
        ['\Stats\Accion', 'decode'],
        $metadata, $options);
    }

    /**
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function BulkCreateAccion($metadata = [], $options = []) {
        return $this->_clientStreamRequest('/stats.AccionService/BulkCreateAccion',
        ['\Common\EmptyMessage','decode'],
        $metadata, $options);
    }

    /**
     * ListAccion
     * @param \Common\EmptyMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListAccion(\Common\EmptyMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.AccionService/ListAccion',
        $argument,
        ['\Stats\AccionArray', 'decode'],
        $metadata, $options);
    }

    /**
     * GetAccion
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetAccion(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.AccionService/GetAccion',
        $argument,
        ['\Stats\Accion', 'decode'],
        $metadata, $options);
    }

    /**
     * UpdateAccion
     * @param \Stats\Accion $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateAccion(\Stats\Accion $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.AccionService/UpdateAccion',
        $argument,
        ['\Stats\Accion', 'decode'],
        $metadata, $options);
    }

}
