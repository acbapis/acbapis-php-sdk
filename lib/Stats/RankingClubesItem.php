<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: stats/stats.proto

namespace Stats;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>stats.RankingClubesItem</code>
 */
class RankingClubesItem extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>int32 orden = 1;</code>
     */
    private $orden = 0;
    /**
     * Generated from protobuf field <code>int64 equipoId = 2;</code>
     */
    private $equipoId = 0;
    /**
     * Generated from protobuf field <code>int32 numPartidos = 3;</code>
     */
    private $numPartidos = 0;
    /**
     * Generated from protobuf field <code>int32 numVictorias = 4;</code>
     */
    private $numVictorias = 0;
    /**
     * Generated from protobuf field <code>int32 numDerrotas = 5;</code>
     */
    private $numDerrotas = 0;
    /**
     * Generated from protobuf field <code>int32 numEmpates = 6;</code>
     */
    private $numEmpates = 0;
    /**
     * Generated from protobuf field <code>int32 puntosFavor = 7;</code>
     */
    private $puntosFavor = 0;
    /**
     * Generated from protobuf field <code>int32 puntosContra = 8;</code>
     */
    private $puntosContra = 0;
    /**
     * Generated from protobuf field <code>int32 puntosDiff = 9;</code>
     */
    private $puntosDiff = 0;
    /**
     * Generated from protobuf field <code>int32 maxDiffFavor = 10;</code>
     */
    private $maxDiffFavor = 0;
    /**
     * Generated from protobuf field <code>int32 maxDiffContra = 11;</code>
     */
    private $maxDiffContra = 0;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type int $orden
     *     @type int|string $equipoId
     *     @type int $numPartidos
     *     @type int $numVictorias
     *     @type int $numDerrotas
     *     @type int $numEmpates
     *     @type int $puntosFavor
     *     @type int $puntosContra
     *     @type int $puntosDiff
     *     @type int $maxDiffFavor
     *     @type int $maxDiffContra
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Stats\Stats::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>int32 orden = 1;</code>
     * @return int
     */
    public function getOrden()
    {
        return $this->orden;
    }

    /**
     * Generated from protobuf field <code>int32 orden = 1;</code>
     * @param int $var
     * @return $this
     */
    public function setOrden($var)
    {
        GPBUtil::checkInt32($var);
        $this->orden = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 equipoId = 2;</code>
     * @return int|string
     */
    public function getEquipoId()
    {
        return $this->equipoId;
    }

    /**
     * Generated from protobuf field <code>int64 equipoId = 2;</code>
     * @param int|string $var
     * @return $this
     */
    public function setEquipoId($var)
    {
        GPBUtil::checkInt64($var);
        $this->equipoId = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int32 numPartidos = 3;</code>
     * @return int
     */
    public function getNumPartidos()
    {
        return $this->numPartidos;
    }

    /**
     * Generated from protobuf field <code>int32 numPartidos = 3;</code>
     * @param int $var
     * @return $this
     */
    public function setNumPartidos($var)
    {
        GPBUtil::checkInt32($var);
        $this->numPartidos = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int32 numVictorias = 4;</code>
     * @return int
     */
    public function getNumVictorias()
    {
        return $this->numVictorias;
    }

    /**
     * Generated from protobuf field <code>int32 numVictorias = 4;</code>
     * @param int $var
     * @return $this
     */
    public function setNumVictorias($var)
    {
        GPBUtil::checkInt32($var);
        $this->numVictorias = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int32 numDerrotas = 5;</code>
     * @return int
     */
    public function getNumDerrotas()
    {
        return $this->numDerrotas;
    }

    /**
     * Generated from protobuf field <code>int32 numDerrotas = 5;</code>
     * @param int $var
     * @return $this
     */
    public function setNumDerrotas($var)
    {
        GPBUtil::checkInt32($var);
        $this->numDerrotas = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int32 numEmpates = 6;</code>
     * @return int
     */
    public function getNumEmpates()
    {
        return $this->numEmpates;
    }

    /**
     * Generated from protobuf field <code>int32 numEmpates = 6;</code>
     * @param int $var
     * @return $this
     */
    public function setNumEmpates($var)
    {
        GPBUtil::checkInt32($var);
        $this->numEmpates = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int32 puntosFavor = 7;</code>
     * @return int
     */
    public function getPuntosFavor()
    {
        return $this->puntosFavor;
    }

    /**
     * Generated from protobuf field <code>int32 puntosFavor = 7;</code>
     * @param int $var
     * @return $this
     */
    public function setPuntosFavor($var)
    {
        GPBUtil::checkInt32($var);
        $this->puntosFavor = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int32 puntosContra = 8;</code>
     * @return int
     */
    public function getPuntosContra()
    {
        return $this->puntosContra;
    }

    /**
     * Generated from protobuf field <code>int32 puntosContra = 8;</code>
     * @param int $var
     * @return $this
     */
    public function setPuntosContra($var)
    {
        GPBUtil::checkInt32($var);
        $this->puntosContra = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int32 puntosDiff = 9;</code>
     * @return int
     */
    public function getPuntosDiff()
    {
        return $this->puntosDiff;
    }

    /**
     * Generated from protobuf field <code>int32 puntosDiff = 9;</code>
     * @param int $var
     * @return $this
     */
    public function setPuntosDiff($var)
    {
        GPBUtil::checkInt32($var);
        $this->puntosDiff = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int32 maxDiffFavor = 10;</code>
     * @return int
     */
    public function getMaxDiffFavor()
    {
        return $this->maxDiffFavor;
    }

    /**
     * Generated from protobuf field <code>int32 maxDiffFavor = 10;</code>
     * @param int $var
     * @return $this
     */
    public function setMaxDiffFavor($var)
    {
        GPBUtil::checkInt32($var);
        $this->maxDiffFavor = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int32 maxDiffContra = 11;</code>
     * @return int
     */
    public function getMaxDiffContra()
    {
        return $this->maxDiffContra;
    }

    /**
     * Generated from protobuf field <code>int32 maxDiffContra = 11;</code>
     * @param int $var
     * @return $this
     */
    public function setMaxDiffContra($var)
    {
        GPBUtil::checkInt32($var);
        $this->maxDiffContra = $var;

        return $this;
    }

}

