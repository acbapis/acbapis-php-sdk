<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: stats/stats.proto

namespace Stats\StatsBox;

use UnexpectedValueException;

/**
 * Protobuf type <code>stats.StatsBox.VoD</code>
 */
class VoD
{
    /**
     * Generated from protobuf enum <code>EMPATE = 0;</code>
     */
    const EMPATE = 0;
    /**
     * Generated from protobuf enum <code>VICTORIA = 1;</code>
     */
    const VICTORIA = 1;
    /**
     * Generated from protobuf enum <code>DERROTA = 2;</code>
     */
    const DERROTA = 2;

    private static $valueToName = [
        self::EMPATE => 'EMPATE',
        self::VICTORIA => 'VICTORIA',
        self::DERROTA => 'DERROTA',
    ];

    public static function name($value)
    {
        if (!isset(self::$valueToName[$value])) {
            throw new UnexpectedValueException(sprintf(
                    'Enum %s has no name defined for value %s', __CLASS__, $value));
        }
        return self::$valueToName[$value];
    }


    public static function value($name)
    {
        $const = __CLASS__ . '::' . strtoupper($name);
        if (!defined($const)) {
            throw new UnexpectedValueException(sprintf(
                    'Enum %s has no value defined for name %s', __CLASS__, $name));
        }
        return constant($const);
    }
}

// Adding a class alias for backwards compatibility with the previous class name.
class_alias(VoD::class, \Stats\StatsBox_VoD::class);

