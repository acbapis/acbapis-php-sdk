<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: stats/stats.proto

namespace Stats;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>stats.TemporadasJugador</code>
 */
class TemporadasJugador extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>repeated .stats.TemporadasJugadorItem items = 1;</code>
     */
    private $items;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type \Stats\TemporadasJugadorItem[]|\Google\Protobuf\Internal\RepeatedField $items
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Stats\Stats::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>repeated .stats.TemporadasJugadorItem items = 1;</code>
     * @return \Google\Protobuf\Internal\RepeatedField
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * Generated from protobuf field <code>repeated .stats.TemporadasJugadorItem items = 1;</code>
     * @param \Stats\TemporadasJugadorItem[]|\Google\Protobuf\Internal\RepeatedField $var
     * @return $this
     */
    public function setItems($var)
    {
        $arr = GPBUtil::checkRepeatedField($var, \Google\Protobuf\Internal\GPBType::MESSAGE, \Stats\TemporadasJugadorItem::class);
        $this->items = $arr;

        return $this;
    }

}

