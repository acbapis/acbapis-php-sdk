<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: stats/stats.proto

namespace Stats;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>stats.TemporadasJugadorItem</code>
 */
class TemporadasJugadorItem extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>int64 entidadId = 1;</code>
     */
    private $entidadId = 0;
    /**
     * Generated from protobuf field <code>int64 competicionId = 2;</code>
     */
    private $competicionId = 0;
    /**
     * Generated from protobuf field <code>int64 edicionId = 3;</code>
     */
    private $edicionId = 0;
    /**
     * Generated from protobuf field <code>int64 faseId = 4;</code>
     */
    private $faseId = 0;
    /**
     * Generated from protobuf field <code>int64 equipoId = 5;</code>
     */
    private $equipoId = 0;
    /**
     * Generated from protobuf field <code>int32 numPartidos = 6;</code>
     */
    private $numPartidos = 0;
    /**
     * Generated from protobuf field <code>int64 minutos = 7;</code>
     */
    private $minutos = 0;
    /**
     * Generated from protobuf field <code>int32 cincoIni = 8;</code>
     */
    private $cincoIni = 0;
    /**
     * Generated from protobuf field <code>float numPuntos = 9;</code>
     */
    private $numPuntos = 0.0;
    /**
     * Generated from protobuf field <code>int32 maxPuntos = 10;</code>
     */
    private $maxPuntos = 0;
    /**
     * Generated from protobuf field <code>float t3c = 11;</code>
     */
    private $t3c = 0.0;
    /**
     * Generated from protobuf field <code>float t3i = 12;</code>
     */
    private $t3i = 0.0;
    /**
     * Generated from protobuf field <code>float t3p = 13;</code>
     */
    private $t3p = 0.0;
    /**
     * Generated from protobuf field <code>float t2c = 14;</code>
     */
    private $t2c = 0.0;
    /**
     * Generated from protobuf field <code>float t2i = 15;</code>
     */
    private $t2i = 0.0;
    /**
     * Generated from protobuf field <code>float t2p = 16;</code>
     */
    private $t2p = 0.0;
    /**
     * Generated from protobuf field <code>float t1c = 17;</code>
     */
    private $t1c = 0.0;
    /**
     * Generated from protobuf field <code>float t1i = 18;</code>
     */
    private $t1i = 0.0;
    /**
     * Generated from protobuf field <code>float t1p = 19;</code>
     */
    private $t1p = 0.0;
    /**
     * Generated from protobuf field <code>float rd = 20;</code>
     */
    private $rd = 0.0;
    /**
     * Generated from protobuf field <code>float ro = 21;</code>
     */
    private $ro = 0.0;
    /**
     * Generated from protobuf field <code>float rt = 22;</code>
     */
    private $rt = 0.0;
    /**
     * Generated from protobuf field <code>float asis = 23;</code>
     */
    private $asis = 0.0;
    /**
     * Generated from protobuf field <code>float br = 24;</code>
     */
    private $br = 0.0;
    /**
     * Generated from protobuf field <code>float bp = 25;</code>
     */
    private $bp = 0.0;
    /**
     * Generated from protobuf field <code>float tf = 26;</code>
     */
    private $tf = 0.0;
    /**
     * Generated from protobuf field <code>float tc = 27;</code>
     */
    private $tc = 0.0;
    /**
     * Generated from protobuf field <code>float mates = 28;</code>
     */
    private $mates = 0.0;
    /**
     * Generated from protobuf field <code>float fc = 29;</code>
     */
    private $fc = 0.0;
    /**
     * Generated from protobuf field <code>float fr = 30;</code>
     */
    private $fr = 0.0;
    /**
     * Generated from protobuf field <code>int32 masmenos = 31;</code>
     */
    private $masmenos = 0;
    /**
     * Generated from protobuf field <code>float val = 32;</code>
     */
    private $val = 0.0;
    /**
     * Generated from protobuf field <code>int32 victorias = 33;</code>
     */
    private $victorias = 0;
    /**
     * Generated from protobuf field <code>int32 derrotas = 34;</code>
     */
    private $derrotas = 0;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type int|string $entidadId
     *     @type int|string $competicionId
     *     @type int|string $edicionId
     *     @type int|string $faseId
     *     @type int|string $equipoId
     *     @type int $numPartidos
     *     @type int|string $minutos
     *     @type int $cincoIni
     *     @type float $numPuntos
     *     @type int $maxPuntos
     *     @type float $t3c
     *     @type float $t3i
     *     @type float $t3p
     *     @type float $t2c
     *     @type float $t2i
     *     @type float $t2p
     *     @type float $t1c
     *     @type float $t1i
     *     @type float $t1p
     *     @type float $rd
     *     @type float $ro
     *     @type float $rt
     *     @type float $asis
     *     @type float $br
     *     @type float $bp
     *     @type float $tf
     *     @type float $tc
     *     @type float $mates
     *     @type float $fc
     *     @type float $fr
     *     @type int $masmenos
     *     @type float $val
     *     @type int $victorias
     *     @type int $derrotas
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Stats\Stats::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>int64 entidadId = 1;</code>
     * @return int|string
     */
    public function getEntidadId()
    {
        return $this->entidadId;
    }

    /**
     * Generated from protobuf field <code>int64 entidadId = 1;</code>
     * @param int|string $var
     * @return $this
     */
    public function setEntidadId($var)
    {
        GPBUtil::checkInt64($var);
        $this->entidadId = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 competicionId = 2;</code>
     * @return int|string
     */
    public function getCompeticionId()
    {
        return $this->competicionId;
    }

    /**
     * Generated from protobuf field <code>int64 competicionId = 2;</code>
     * @param int|string $var
     * @return $this
     */
    public function setCompeticionId($var)
    {
        GPBUtil::checkInt64($var);
        $this->competicionId = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 edicionId = 3;</code>
     * @return int|string
     */
    public function getEdicionId()
    {
        return $this->edicionId;
    }

    /**
     * Generated from protobuf field <code>int64 edicionId = 3;</code>
     * @param int|string $var
     * @return $this
     */
    public function setEdicionId($var)
    {
        GPBUtil::checkInt64($var);
        $this->edicionId = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 faseId = 4;</code>
     * @return int|string
     */
    public function getFaseId()
    {
        return $this->faseId;
    }

    /**
     * Generated from protobuf field <code>int64 faseId = 4;</code>
     * @param int|string $var
     * @return $this
     */
    public function setFaseId($var)
    {
        GPBUtil::checkInt64($var);
        $this->faseId = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 equipoId = 5;</code>
     * @return int|string
     */
    public function getEquipoId()
    {
        return $this->equipoId;
    }

    /**
     * Generated from protobuf field <code>int64 equipoId = 5;</code>
     * @param int|string $var
     * @return $this
     */
    public function setEquipoId($var)
    {
        GPBUtil::checkInt64($var);
        $this->equipoId = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int32 numPartidos = 6;</code>
     * @return int
     */
    public function getNumPartidos()
    {
        return $this->numPartidos;
    }

    /**
     * Generated from protobuf field <code>int32 numPartidos = 6;</code>
     * @param int $var
     * @return $this
     */
    public function setNumPartidos($var)
    {
        GPBUtil::checkInt32($var);
        $this->numPartidos = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 minutos = 7;</code>
     * @return int|string
     */
    public function getMinutos()
    {
        return $this->minutos;
    }

    /**
     * Generated from protobuf field <code>int64 minutos = 7;</code>
     * @param int|string $var
     * @return $this
     */
    public function setMinutos($var)
    {
        GPBUtil::checkInt64($var);
        $this->minutos = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int32 cincoIni = 8;</code>
     * @return int
     */
    public function getCincoIni()
    {
        return $this->cincoIni;
    }

    /**
     * Generated from protobuf field <code>int32 cincoIni = 8;</code>
     * @param int $var
     * @return $this
     */
    public function setCincoIni($var)
    {
        GPBUtil::checkInt32($var);
        $this->cincoIni = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>float numPuntos = 9;</code>
     * @return float
     */
    public function getNumPuntos()
    {
        return $this->numPuntos;
    }

    /**
     * Generated from protobuf field <code>float numPuntos = 9;</code>
     * @param float $var
     * @return $this
     */
    public function setNumPuntos($var)
    {
        GPBUtil::checkFloat($var);
        $this->numPuntos = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int32 maxPuntos = 10;</code>
     * @return int
     */
    public function getMaxPuntos()
    {
        return $this->maxPuntos;
    }

    /**
     * Generated from protobuf field <code>int32 maxPuntos = 10;</code>
     * @param int $var
     * @return $this
     */
    public function setMaxPuntos($var)
    {
        GPBUtil::checkInt32($var);
        $this->maxPuntos = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>float t3c = 11;</code>
     * @return float
     */
    public function getT3C()
    {
        return $this->t3c;
    }

    /**
     * Generated from protobuf field <code>float t3c = 11;</code>
     * @param float $var
     * @return $this
     */
    public function setT3C($var)
    {
        GPBUtil::checkFloat($var);
        $this->t3c = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>float t3i = 12;</code>
     * @return float
     */
    public function getT3I()
    {
        return $this->t3i;
    }

    /**
     * Generated from protobuf field <code>float t3i = 12;</code>
     * @param float $var
     * @return $this
     */
    public function setT3I($var)
    {
        GPBUtil::checkFloat($var);
        $this->t3i = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>float t3p = 13;</code>
     * @return float
     */
    public function getT3P()
    {
        return $this->t3p;
    }

    /**
     * Generated from protobuf field <code>float t3p = 13;</code>
     * @param float $var
     * @return $this
     */
    public function setT3P($var)
    {
        GPBUtil::checkFloat($var);
        $this->t3p = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>float t2c = 14;</code>
     * @return float
     */
    public function getT2C()
    {
        return $this->t2c;
    }

    /**
     * Generated from protobuf field <code>float t2c = 14;</code>
     * @param float $var
     * @return $this
     */
    public function setT2C($var)
    {
        GPBUtil::checkFloat($var);
        $this->t2c = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>float t2i = 15;</code>
     * @return float
     */
    public function getT2I()
    {
        return $this->t2i;
    }

    /**
     * Generated from protobuf field <code>float t2i = 15;</code>
     * @param float $var
     * @return $this
     */
    public function setT2I($var)
    {
        GPBUtil::checkFloat($var);
        $this->t2i = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>float t2p = 16;</code>
     * @return float
     */
    public function getT2P()
    {
        return $this->t2p;
    }

    /**
     * Generated from protobuf field <code>float t2p = 16;</code>
     * @param float $var
     * @return $this
     */
    public function setT2P($var)
    {
        GPBUtil::checkFloat($var);
        $this->t2p = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>float t1c = 17;</code>
     * @return float
     */
    public function getT1C()
    {
        return $this->t1c;
    }

    /**
     * Generated from protobuf field <code>float t1c = 17;</code>
     * @param float $var
     * @return $this
     */
    public function setT1C($var)
    {
        GPBUtil::checkFloat($var);
        $this->t1c = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>float t1i = 18;</code>
     * @return float
     */
    public function getT1I()
    {
        return $this->t1i;
    }

    /**
     * Generated from protobuf field <code>float t1i = 18;</code>
     * @param float $var
     * @return $this
     */
    public function setT1I($var)
    {
        GPBUtil::checkFloat($var);
        $this->t1i = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>float t1p = 19;</code>
     * @return float
     */
    public function getT1P()
    {
        return $this->t1p;
    }

    /**
     * Generated from protobuf field <code>float t1p = 19;</code>
     * @param float $var
     * @return $this
     */
    public function setT1P($var)
    {
        GPBUtil::checkFloat($var);
        $this->t1p = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>float rd = 20;</code>
     * @return float
     */
    public function getRd()
    {
        return $this->rd;
    }

    /**
     * Generated from protobuf field <code>float rd = 20;</code>
     * @param float $var
     * @return $this
     */
    public function setRd($var)
    {
        GPBUtil::checkFloat($var);
        $this->rd = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>float ro = 21;</code>
     * @return float
     */
    public function getRo()
    {
        return $this->ro;
    }

    /**
     * Generated from protobuf field <code>float ro = 21;</code>
     * @param float $var
     * @return $this
     */
    public function setRo($var)
    {
        GPBUtil::checkFloat($var);
        $this->ro = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>float rt = 22;</code>
     * @return float
     */
    public function getRt()
    {
        return $this->rt;
    }

    /**
     * Generated from protobuf field <code>float rt = 22;</code>
     * @param float $var
     * @return $this
     */
    public function setRt($var)
    {
        GPBUtil::checkFloat($var);
        $this->rt = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>float asis = 23;</code>
     * @return float
     */
    public function getAsis()
    {
        return $this->asis;
    }

    /**
     * Generated from protobuf field <code>float asis = 23;</code>
     * @param float $var
     * @return $this
     */
    public function setAsis($var)
    {
        GPBUtil::checkFloat($var);
        $this->asis = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>float br = 24;</code>
     * @return float
     */
    public function getBr()
    {
        return $this->br;
    }

    /**
     * Generated from protobuf field <code>float br = 24;</code>
     * @param float $var
     * @return $this
     */
    public function setBr($var)
    {
        GPBUtil::checkFloat($var);
        $this->br = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>float bp = 25;</code>
     * @return float
     */
    public function getBp()
    {
        return $this->bp;
    }

    /**
     * Generated from protobuf field <code>float bp = 25;</code>
     * @param float $var
     * @return $this
     */
    public function setBp($var)
    {
        GPBUtil::checkFloat($var);
        $this->bp = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>float tf = 26;</code>
     * @return float
     */
    public function getTf()
    {
        return $this->tf;
    }

    /**
     * Generated from protobuf field <code>float tf = 26;</code>
     * @param float $var
     * @return $this
     */
    public function setTf($var)
    {
        GPBUtil::checkFloat($var);
        $this->tf = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>float tc = 27;</code>
     * @return float
     */
    public function getTc()
    {
        return $this->tc;
    }

    /**
     * Generated from protobuf field <code>float tc = 27;</code>
     * @param float $var
     * @return $this
     */
    public function setTc($var)
    {
        GPBUtil::checkFloat($var);
        $this->tc = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>float mates = 28;</code>
     * @return float
     */
    public function getMates()
    {
        return $this->mates;
    }

    /**
     * Generated from protobuf field <code>float mates = 28;</code>
     * @param float $var
     * @return $this
     */
    public function setMates($var)
    {
        GPBUtil::checkFloat($var);
        $this->mates = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>float fc = 29;</code>
     * @return float
     */
    public function getFc()
    {
        return $this->fc;
    }

    /**
     * Generated from protobuf field <code>float fc = 29;</code>
     * @param float $var
     * @return $this
     */
    public function setFc($var)
    {
        GPBUtil::checkFloat($var);
        $this->fc = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>float fr = 30;</code>
     * @return float
     */
    public function getFr()
    {
        return $this->fr;
    }

    /**
     * Generated from protobuf field <code>float fr = 30;</code>
     * @param float $var
     * @return $this
     */
    public function setFr($var)
    {
        GPBUtil::checkFloat($var);
        $this->fr = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int32 masmenos = 31;</code>
     * @return int
     */
    public function getMasmenos()
    {
        return $this->masmenos;
    }

    /**
     * Generated from protobuf field <code>int32 masmenos = 31;</code>
     * @param int $var
     * @return $this
     */
    public function setMasmenos($var)
    {
        GPBUtil::checkInt32($var);
        $this->masmenos = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>float val = 32;</code>
     * @return float
     */
    public function getVal()
    {
        return $this->val;
    }

    /**
     * Generated from protobuf field <code>float val = 32;</code>
     * @param float $var
     * @return $this
     */
    public function setVal($var)
    {
        GPBUtil::checkFloat($var);
        $this->val = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int32 victorias = 33;</code>
     * @return int
     */
    public function getVictorias()
    {
        return $this->victorias;
    }

    /**
     * Generated from protobuf field <code>int32 victorias = 33;</code>
     * @param int $var
     * @return $this
     */
    public function setVictorias($var)
    {
        GPBUtil::checkInt32($var);
        $this->victorias = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int32 derrotas = 34;</code>
     * @return int
     */
    public function getDerrotas()
    {
        return $this->derrotas;
    }

    /**
     * Generated from protobuf field <code>int32 derrotas = 34;</code>
     * @param int $var
     * @return $this
     */
    public function setDerrotas($var)
    {
        GPBUtil::checkInt32($var);
        $this->derrotas = $var;

        return $this;
    }

}

