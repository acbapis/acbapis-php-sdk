<?php
// GENERATED CODE -- DO NOT EDIT!

namespace Stats;

/**
 */
class StatsFilesServiceClient extends \Grpc\BaseStub {

    /**
     * @param string $hostname hostname
     * @param array $opts channel options
     * @param \Grpc\Channel $channel (optional) re-use channel object
     */
    public function __construct($hostname, $opts, $channel = null) {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * @param \Stats\XmlPartidoQry $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetXmlPartido(\Stats\XmlPartidoQry $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsFilesService/GetXmlPartido',
        $argument,
        ['\Stats\XmlPartidoMsg', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Stats\XmlPartidoQry $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetXmlPartidoAsXml(\Stats\XmlPartidoQry $argument,
      $metadata = [], $options = []) {
        return $this->_serverStreamRequest('/stats.StatsFilesService/GetXmlPartidoAsXml',
        $argument,
        ['\Stats\XmlPartidoMsgAsXml', 'decode'],
        $metadata, $options);
    }

}
