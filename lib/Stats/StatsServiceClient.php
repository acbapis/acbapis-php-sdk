<?php
// GENERATED CODE -- DO NOT EDIT!

namespace Stats;

/**
 */
class StatsServiceClient extends \Grpc\BaseStub {

    /**
     * @param string $hostname hostname
     * @param array $opts channel options
     * @param \Grpc\Channel $channel (optional) re-use channel object
     */
    public function __construct($hostname, $opts, $channel = null) {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * @param \Stats\RankingFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetRanking(\Stats\RankingFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/GetRanking',
        $argument,
        ['\Stats\Ranking', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Stats\RankingsEquiposFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetRankingsEquipos(\Stats\RankingsEquiposFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/GetRankingsEquipos',
        $argument,
        ['\Stats\RankingsEquipos', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Stats\RankingEquiposListaFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetRankingEquiposLista(\Stats\RankingEquiposListaFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/GetRankingEquiposLista',
        $argument,
        ['\Stats\RankingEquiposLista', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Stats\RankingClubesFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetRankingClubes(\Stats\RankingClubesFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/GetRankingClubes',
        $argument,
        ['\Stats\RankingClubes', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Stats\PartidosJugadorFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetPartidosJugador(\Stats\PartidosJugadorFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/GetPartidosJugador',
        $argument,
        ['\Stats\PartidosJugador', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Stats\TemporadaEquipoFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetTemporadaEquipo(\Stats\TemporadaEquipoFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/GetTemporadaEquipo',
        $argument,
        ['\Stats\TemporadaEquipo', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Stats\TemporadasEntrenadorFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetTemporadasEntrenador(\Stats\TemporadasEntrenadorFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/GetTemporadasEntrenador',
        $argument,
        ['\Stats\TemporadasEntrenador', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetTopesEntrenador(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/GetTopesEntrenador',
        $argument,
        ['\Stats\TopesEntrenador', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Stats\HistoriaEntrenadorFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetHistoriaEntrenador(\Stats\HistoriaEntrenadorFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/GetHistoriaEntrenador',
        $argument,
        ['\Stats\HistoriaEntrenador', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Stats\TemporadasJugadorFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetTemporadasJugador(\Stats\TemporadasJugadorFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/GetTemporadasJugador',
        $argument,
        ['\Stats\TemporadasJugador', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetTopesJugador(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/GetTopesJugador',
        $argument,
        ['\Stats\TopesJugador', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Stats\RankingJugadoresListaFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetRankingJugadoresLista(\Stats\RankingJugadoresListaFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/GetRankingJugadoresLista',
        $argument,
        ['\Stats\RankingJugadoresLista', 'decode'],
        $metadata, $options);
    }

    /**
     * GetStatsCollection utiliza el filtro para selecciona la colección estadística que retornará. Es imprescindible indicar el tipo
     * puesto que no existe uno por defecto a diferencia de GetLideres, GetTopes, GetHistoricos, GetRecords que devolverán los tipos
     * estándar predeterminados en caso de obviar el tipo.
     * Si no se indicar valores para la competición, edición y fase se tomarán por defecto los de la que se encuentre en curso.
     * @param \Stats\StatsItemCollectionFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetStatsCollection(\Stats\StatsItemCollectionFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/GetStatsCollection',
        $argument,
        ['\Stats\StatsItemCollection', 'decode'],
        $metadata, $options);
    }

    /**
     * GetLideres devuelve los líderes según el filtro que suele ser por competición, num_edicion, num jornada (cualquiera de los tres o ninguno).
     * lideres : MÁXIMOS POR PROMEDIO/SUMA EN COMPETICIÓN-EDICIÓN
     * Contiene puntos, rebotes, asistencias y valoración.
     * Si se añade el param agrupacion=equipo permite obtener los líderes en un equipo en lugar de jugadores. Si no se especifica, por defecto, se sobreentiende que agrupacion=jugador
     * @param \Stats\StatsItemCollectionFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetLideres(\Stats\StatsItemCollectionFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/GetLideres',
        $argument,
        ['\Stats\StatsItemCollection', 'decode'],
        $metadata, $options);
    }

    /**
     * topes : MÁXIMOS POR PARTIDO
     * GetTopes construye el Top5 de puntos, rebotes, asistencias y valoración en una jornada, ordenados del 1 al 5.
     * @param \Stats\StatsItemCollectionFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetTopes(\Stats\StatsItemCollectionFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/GetTopes',
        $argument,
        ['\Stats\Topes', 'decode'],
        $metadata, $options);
    }

    /**
     * GetHistoricos devuelve los registros históricos por categoría atendiendo al filtro proporcionado.
     * historicos : MÁXIMOS DE SUMA DE TODAS EDICIONES EN COMPETICIÓN
     * @param \Stats\StatsItemCollectionFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetHistoricos(\Stats\StatsItemCollectionFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/GetHistoricos',
        $argument,
        ['\Stats\StatsItemCollection', 'decode'],
        $metadata, $options);
    }

    /**
     * GetRecords devuelve los máximos topes agrupados atendiendo al filtro proporcionado
     * records: MÁXIMO DE TOPES
     * @param \Stats\StatsItemCollectionFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetRecords(\Stats\StatsItemCollectionFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/GetRecords',
        $argument,
        ['\Stats\StatsItemCollection', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Stats\IndividualesFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetIndividuales(\Stats\IndividualesFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/GetIndividuales',
        $argument,
        ['\Stats\Individuales', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Stats\StatsBoxFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetBoxScore(\Stats\StatsBoxFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/GetBoxScore',
        $argument,
        ['\Stats\StatsBoxPartidoArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Stats\StatsBoxFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetBoxScorePartido(\Stats\StatsBoxFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/GetBoxScorePartido',
        $argument,
        ['\Stats\StatsBoxPartido', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Stats\StatsBoxLicenciaFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetBoxScoreLicencia(\Stats\StatsBoxLicenciaFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/GetBoxScoreLicencia',
        $argument,
        ['\Stats\StatsBoxLicencia', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Stats\StatsBoxEquipoFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetBoxScoreEquipo(\Stats\StatsBoxEquipoFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/GetBoxScoreEquipo',
        $argument,
        ['\Stats\StatsBoxEquipo', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Stats\PrecedentesFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetPrecedentes(\Stats\PrecedentesFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/GetPrecedentes',
        $argument,
        ['\Stats\Precedentes', 'decode'],
        $metadata, $options);
    }

    /**
     * GetPbp devuelve el play by play de un partido
     * @param \Stats\StatsGetPbpFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetPbp(\Stats\StatsGetPbpFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/GetPbp',
        $argument,
        ['\Stats\StatsPbp', 'decode'],
        $metadata, $options);
    }

    /**
     * ListPbp devuelve los play by play de varios partidos seleccionados por los filtros
     * @param \Stats\StatsPbpFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListPbp(\Stats\StatsPbpFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/ListPbp',
        $argument,
        ['\Stats\StatsPbpArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Stats\StatsTypeFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindStatsType(\Stats\StatsTypeFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/FindStatsType',
        $argument,
        ['\Stats\StatsType', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Stats\StatsTypeFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListStatsTypes(\Stats\StatsTypeFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/ListStatsTypes',
        $argument,
        ['\Stats\StatsTypeArray', 'decode'],
        $metadata, $options);
    }

    /**
     * Start Accion endpoints
     * @param \Stats\StatsAccionMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateStatsAccion(\Stats\StatsAccionMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/CreateStatsAccion',
        $argument,
        ['\Stats\StatsAccionMessage', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Stats\StatsAccionMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateStatsAccion(\Stats\StatsAccionMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/UpdateStatsAccion',
        $argument,
        ['\Stats\StatsAccionMessage', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Stats\StatsAccionFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindStatsAccionByID(\Stats\StatsAccionFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/FindStatsAccionByID',
        $argument,
        ['\Stats\StatsAccionMessage', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Stats\StatsAccionFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListStatsAcciones(\Stats\StatsAccionFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/ListStatsAcciones',
        $argument,
        ['\Stats\StatsAccionArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteStatsAccion(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/DeleteStatsAccion',
        $argument,
        ['\Stats\StatsAccionMessage', 'decode'],
        $metadata, $options);
    }

    /**
     * Start AccionTipo endpoints
     * @param \Stats\StatsAccionTipoMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateStatsAccionTipo(\Stats\StatsAccionTipoMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/CreateStatsAccionTipo',
        $argument,
        ['\Stats\StatsAccionTipoMessage', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Stats\StatsAccionTipoMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateStatsAccionTipo(\Stats\StatsAccionTipoMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/UpdateStatsAccionTipo',
        $argument,
        ['\Stats\StatsAccionTipoMessage', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Stats\StatsAccionTipoFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindStatsAccionTipoByID(\Stats\StatsAccionTipoFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/FindStatsAccionTipoByID',
        $argument,
        ['\Stats\StatsAccionTipoMessage', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Stats\StatsAccionTipoFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListStatsAccionTipos(\Stats\StatsAccionTipoFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/ListStatsAccionTipos',
        $argument,
        ['\Stats\StatsAccionTipoArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteStatsAccionTipo(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/DeleteStatsAccionTipo',
        $argument,
        ['\Stats\StatsAccionTipoMessage', 'decode'],
        $metadata, $options);
    }

    /**
     * Start BoxScore endpoints
     * @param \Stats\StatsBoxScoreMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateStatsBoxScore(\Stats\StatsBoxScoreMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/CreateStatsBoxScore',
        $argument,
        ['\Stats\StatsBoxScoreMessage', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Stats\StatsBoxScoreMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateStatsBoxScore(\Stats\StatsBoxScoreMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/UpdateStatsBoxScore',
        $argument,
        ['\Stats\StatsBoxScoreMessage', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Stats\StatsBoxScoreFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindStatsBoxScoreByID(\Stats\StatsBoxScoreFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/FindStatsBoxScoreByID',
        $argument,
        ['\Stats\StatsBoxScoreMessage', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Stats\StatsBoxScoreFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListStatsBoxScores(\Stats\StatsBoxScoreFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/ListStatsBoxScores',
        $argument,
        ['\Stats\StatsBoxScoreArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteStatsBoxScore(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/DeleteStatsBoxScore',
        $argument,
        ['\Stats\StatsBoxScoreMessage', 'decode'],
        $metadata, $options);
    }

    /**
     * Start BoxScoreTipo endpoints
     * @param \Stats\StatsBoxScoreTipoMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateStatsBoxScoreTipo(\Stats\StatsBoxScoreTipoMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/CreateStatsBoxScoreTipo',
        $argument,
        ['\Stats\StatsBoxScoreTipoMessage', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Stats\StatsBoxScoreTipoMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateStatsBoxScoreTipo(\Stats\StatsBoxScoreTipoMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/UpdateStatsBoxScoreTipo',
        $argument,
        ['\Stats\StatsBoxScoreTipoMessage', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Stats\StatsBoxScoreTipoFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindStatsBoxScoreTipoByID(\Stats\StatsBoxScoreTipoFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/FindStatsBoxScoreTipoByID',
        $argument,
        ['\Stats\StatsBoxScoreTipoMessage', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Stats\StatsBoxScoreTipoFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListStatsBoxScoreTipos(\Stats\StatsBoxScoreTipoFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/ListStatsBoxScoreTipos',
        $argument,
        ['\Stats\StatsBoxScoreTipoArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteStatsBoxScoreTipo(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/DeleteStatsBoxScoreTipo',
        $argument,
        ['\Stats\StatsBoxScoreTipoMessage', 'decode'],
        $metadata, $options);
    }

    /**
     * Start Configuracion endpoints
     * @param \Stats\StatsConfiguracionMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateStatsConfiguracion(\Stats\StatsConfiguracionMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/CreateStatsConfiguracion',
        $argument,
        ['\Stats\StatsConfiguracionMessage', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Stats\StatsConfiguracionMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateStatsConfiguracion(\Stats\StatsConfiguracionMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/UpdateStatsConfiguracion',
        $argument,
        ['\Stats\StatsConfiguracionMessage', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Stats\StatsConfiguracionFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindStatsConfiguracionByID(\Stats\StatsConfiguracionFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/FindStatsConfiguracionByID',
        $argument,
        ['\Stats\StatsConfiguracionMessage', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Stats\StatsConfiguracionFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListStatsConfiguraciones(\Stats\StatsConfiguracionFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/ListStatsConfiguraciones',
        $argument,
        ['\Stats\StatsConfiguracionArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteStatsConfiguracion(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/DeleteStatsConfiguracion',
        $argument,
        ['\Stats\StatsConfiguracionMessage', 'decode'],
        $metadata, $options);
    }

    /**
     * Start ConfiguracionPartidoTipo endpoints
     * @param \Stats\StatsConfiguracionPartidoTipoMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateStatsConfiguracionPartidoTipo(\Stats\StatsConfiguracionPartidoTipoMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/CreateStatsConfiguracionPartidoTipo',
        $argument,
        ['\Stats\StatsConfiguracionPartidoTipoMessage', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Stats\StatsConfiguracionPartidoTipoMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateStatsConfiguracionPartidoTipo(\Stats\StatsConfiguracionPartidoTipoMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/UpdateStatsConfiguracionPartidoTipo',
        $argument,
        ['\Stats\StatsConfiguracionPartidoTipoMessage', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Stats\StatsConfiguracionPartidoTipoFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindStatsConfiguracionPartidoTipoByID(\Stats\StatsConfiguracionPartidoTipoFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/FindStatsConfiguracionPartidoTipoByID',
        $argument,
        ['\Stats\StatsConfiguracionPartidoTipoMessage', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Stats\StatsConfiguracionPartidoTipoFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListStatsConfiguracionPartidoTipos(\Stats\StatsConfiguracionPartidoTipoFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/ListStatsConfiguracionPartidoTipos',
        $argument,
        ['\Stats\StatsConfiguracionPartidoTipoArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteStatsConfiguracionPartidoTipo(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/DeleteStatsConfiguracionPartidoTipo',
        $argument,
        ['\Stats\StatsConfiguracionPartidoTipoMessage', 'decode'],
        $metadata, $options);
    }

    /**
     * Start ValorJornada endpoints
     * @param \Stats\ValorJornada $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateValorJornada(\Stats\ValorJornada $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/CreateValorJornada',
        $argument,
        ['\Stats\ValorJornada', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Stats\ValorJornada $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateValorJornada(\Stats\ValorJornada $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/UpdateValorJornada',
        $argument,
        ['\Stats\ValorJornada', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindValorJornadaByID(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/FindValorJornadaByID',
        $argument,
        ['\Stats\ValorJornada', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Stats\ValorJornadaFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListValorJornadas(\Stats\ValorJornadaFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/ListValorJornadas',
        $argument,
        ['\Stats\ValorJornadaArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteValorJornada(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/stats.StatsService/DeleteValorJornada',
        $argument,
        ['\Stats\ValorJornada', 'decode'],
        $metadata, $options);
    }

}
