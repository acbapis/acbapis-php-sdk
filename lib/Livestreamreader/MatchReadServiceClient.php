<?php
// GENERATED CODE -- DO NOT EDIT!

namespace Livestreamreader;

/**
 */
class MatchReadServiceClient extends \Grpc\BaseStub {

    /**
     * @param string $hostname hostname
     * @param array $opts channel options
     * @param \Grpc\Channel $channel (optional) re-use channel object
     */
    public function __construct($hostname, $opts, $channel = null) {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * @param \Livestreamreader\ReadRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateReadRequest(\Livestreamreader\ReadRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/livestreamreader.MatchReadService/CreateReadRequest',
        $argument,
        ['\Common\UuidMessage', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\UuidMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function StartReading(\Common\UuidMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/livestreamreader.MatchReadService/StartReading',
        $argument,
        ['\Common\Timestamp', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\UuidMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function StopReading(\Common\UuidMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/livestreamreader.MatchReadService/StopReading',
        $argument,
        ['\Common\EmptyMessage', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Livestreamreader\ReadRequestFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListReadRequests(\Livestreamreader\ReadRequestFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/livestreamreader.MatchReadService/ListReadRequests',
        $argument,
        ['\Livestreamreader\ReadRequestArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\UuidMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetReadRequestByID(\Common\UuidMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/livestreamreader.MatchReadService/GetReadRequestByID',
        $argument,
        ['\Livestreamreader\MatchReadRequest', 'decode'],
        $metadata, $options);
    }

    /**
     * DeleteReading stops the recorder process.
     * When the recorder stops, it is deleted from database.
     * @param \Common\UuidMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteReading(\Common\UuidMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/livestreamreader.MatchReadService/DeleteReading',
        $argument,
        ['\Common\EmptyMessage', 'decode'],
        $metadata, $options);
    }

    /**
     * RecorderStatus returns the status of the livestream recorder, whether it is
     * currently recording a match or not
     * @param \Common\EmptyMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function RecorderStatus(\Common\EmptyMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/livestreamreader.MatchReadService/RecorderStatus',
        $argument,
        ['\Livestreamreader\Status', 'decode'],
        $metadata, $options);
    }

}
