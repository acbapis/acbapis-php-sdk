<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: livestreamreader/livestreamreader.proto

namespace Livestreamreader;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>livestreamreader.ReadRequestFilter</code>
 */
class ReadRequestFilter extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>int64 offset = 1;</code>
     */
    private $offset = 0;
    /**
     * Generated from protobuf field <code>int64 limit = 2;</code>
     */
    private $limit = 0;
    /**
     * Generated from protobuf field <code>string status = 3;</code>
     */
    private $status = '';
    /**
     * Generated from protobuf field <code>int64 start = 4;</code>
     */
    private $start = 0;
    /**
     * Generated from protobuf field <code>int64 matchId = 5;</code>
     */
    private $matchId = 0;
    /**
     * Generated from protobuf field <code>.livestreamreader.ReadRequestSorting sorting = 6;</code>
     */
    private $sorting = null;
    /**
     * Generated from protobuf field <code>string recorderId = 8;</code>
     */
    private $recorderId = '';

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type int|string $offset
     *     @type int|string $limit
     *     @type string $status
     *     @type int|string $start
     *     @type int|string $matchId
     *     @type \Livestreamreader\ReadRequestSorting $sorting
     *     @type string $recorderId
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Livestreamreader\Livestreamreader::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>int64 offset = 1;</code>
     * @return int|string
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     * Generated from protobuf field <code>int64 offset = 1;</code>
     * @param int|string $var
     * @return $this
     */
    public function setOffset($var)
    {
        GPBUtil::checkInt64($var);
        $this->offset = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 limit = 2;</code>
     * @return int|string
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * Generated from protobuf field <code>int64 limit = 2;</code>
     * @param int|string $var
     * @return $this
     */
    public function setLimit($var)
    {
        GPBUtil::checkInt64($var);
        $this->limit = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string status = 3;</code>
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Generated from protobuf field <code>string status = 3;</code>
     * @param string $var
     * @return $this
     */
    public function setStatus($var)
    {
        GPBUtil::checkString($var, True);
        $this->status = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 start = 4;</code>
     * @return int|string
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Generated from protobuf field <code>int64 start = 4;</code>
     * @param int|string $var
     * @return $this
     */
    public function setStart($var)
    {
        GPBUtil::checkInt64($var);
        $this->start = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 matchId = 5;</code>
     * @return int|string
     */
    public function getMatchId()
    {
        return $this->matchId;
    }

    /**
     * Generated from protobuf field <code>int64 matchId = 5;</code>
     * @param int|string $var
     * @return $this
     */
    public function setMatchId($var)
    {
        GPBUtil::checkInt64($var);
        $this->matchId = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>.livestreamreader.ReadRequestSorting sorting = 6;</code>
     * @return \Livestreamreader\ReadRequestSorting
     */
    public function getSorting()
    {
        return $this->sorting;
    }

    /**
     * Generated from protobuf field <code>.livestreamreader.ReadRequestSorting sorting = 6;</code>
     * @param \Livestreamreader\ReadRequestSorting $var
     * @return $this
     */
    public function setSorting($var)
    {
        GPBUtil::checkMessage($var, \Livestreamreader\ReadRequestSorting::class);
        $this->sorting = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string recorderId = 8;</code>
     * @return string
     */
    public function getRecorderId()
    {
        return $this->recorderId;
    }

    /**
     * Generated from protobuf field <code>string recorderId = 8;</code>
     * @param string $var
     * @return $this
     */
    public function setRecorderId($var)
    {
        GPBUtil::checkString($var, True);
        $this->recorderId = $var;

        return $this;
    }

}

