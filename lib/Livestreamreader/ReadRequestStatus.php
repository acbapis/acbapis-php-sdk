<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: livestreamreader/livestreamreader.proto

namespace Livestreamreader;

use UnexpectedValueException;

/**
 * Protobuf type <code>livestreamreader.ReadRequestStatus</code>
 */
class ReadRequestStatus
{
    /**
     * Generated from protobuf enum <code>UNKNOWN = 0;</code>
     */
    const UNKNOWN = 0;
    /**
     * Generated from protobuf enum <code>WAITING = 1;</code>
     */
    const WAITING = 1;
    /**
     * Generated from protobuf enum <code>RUNNING = 2;</code>
     */
    const RUNNING = 2;
    /**
     * Generated from protobuf enum <code>STOPPED = 3;</code>
     */
    const STOPPED = 3;
    /**
     * Generated from protobuf enum <code>FINISHED = 4;</code>
     */
    const FINISHED = 4;

    private static $valueToName = [
        self::UNKNOWN => 'UNKNOWN',
        self::WAITING => 'WAITING',
        self::RUNNING => 'RUNNING',
        self::STOPPED => 'STOPPED',
        self::FINISHED => 'FINISHED',
    ];

    public static function name($value)
    {
        if (!isset(self::$valueToName[$value])) {
            throw new UnexpectedValueException(sprintf(
                    'Enum %s has no name defined for value %s', __CLASS__, $value));
        }
        return self::$valueToName[$value];
    }


    public static function value($name)
    {
        $const = __CLASS__ . '::' . strtoupper($name);
        if (!defined($const)) {
            throw new UnexpectedValueException(sprintf(
                    'Enum %s has no value defined for name %s', __CLASS__, $name));
        }
        return constant($const);
    }
}

