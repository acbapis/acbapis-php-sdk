<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: openapi/openapi.proto

namespace Openapi;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>openapi.Item</code>
 */
class Item extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>string key = 1;</code>
     */
    private $key = '';
    /**
     * Generated from protobuf field <code>string val = 2;</code>
     */
    private $val = '';

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type string $key
     *     @type string $val
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Openapi\Openapi::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>string key = 1;</code>
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * Generated from protobuf field <code>string key = 1;</code>
     * @param string $var
     * @return $this
     */
    public function setKey($var)
    {
        GPBUtil::checkString($var, True);
        $this->key = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string val = 2;</code>
     * @return string
     */
    public function getVal()
    {
        return $this->val;
    }

    /**
     * Generated from protobuf field <code>string val = 2;</code>
     * @param string $var
     * @return $this
     */
    public function setVal($var)
    {
        GPBUtil::checkString($var, True);
        $this->val = $var;

        return $this;
    }

}

