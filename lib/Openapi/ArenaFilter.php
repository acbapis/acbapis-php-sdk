<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: openapi/openapi.proto

namespace Openapi;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>openapi.ArenaFilter</code>
 */
class ArenaFilter extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>int64 offset = 1;</code>
     */
    private $offset = 0;
    /**
     * Generated from protobuf field <code>int64 limit = 2;</code>
     */
    private $limit = 0;
    /**
     * Generated from protobuf field <code>string terms = 3;</code>
     */
    private $terms = '';
    /**
     * Arenas open during filter
     *
     * Generated from protobuf field <code>int64 time = 4;</code>
     */
    private $time = 0;
    /**
     * 1 for active arenas, 2 for inactive
     *
     * Generated from protobuf field <code>int32 active = 5;</code>
     */
    private $active = 0;
    /**
     * Required fields separated by comma
     *
     * Generated from protobuf field <code>string fields = 6;</code>
     */
    private $fields = '';
    /**
     * Generated from protobuf field <code>.openapi.ArenaSorting sorting = 7;</code>
     */
    private $sorting = null;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type int|string $offset
     *     @type int|string $limit
     *     @type string $terms
     *     @type int|string $time
     *           Arenas open during filter
     *     @type int $active
     *           1 for active arenas, 2 for inactive
     *     @type string $fields
     *           Required fields separated by comma
     *     @type \Openapi\ArenaSorting $sorting
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Openapi\Openapi::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>int64 offset = 1;</code>
     * @return int|string
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     * Generated from protobuf field <code>int64 offset = 1;</code>
     * @param int|string $var
     * @return $this
     */
    public function setOffset($var)
    {
        GPBUtil::checkInt64($var);
        $this->offset = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 limit = 2;</code>
     * @return int|string
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * Generated from protobuf field <code>int64 limit = 2;</code>
     * @param int|string $var
     * @return $this
     */
    public function setLimit($var)
    {
        GPBUtil::checkInt64($var);
        $this->limit = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string terms = 3;</code>
     * @return string
     */
    public function getTerms()
    {
        return $this->terms;
    }

    /**
     * Generated from protobuf field <code>string terms = 3;</code>
     * @param string $var
     * @return $this
     */
    public function setTerms($var)
    {
        GPBUtil::checkString($var, True);
        $this->terms = $var;

        return $this;
    }

    /**
     * Arenas open during filter
     *
     * Generated from protobuf field <code>int64 time = 4;</code>
     * @return int|string
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Arenas open during filter
     *
     * Generated from protobuf field <code>int64 time = 4;</code>
     * @param int|string $var
     * @return $this
     */
    public function setTime($var)
    {
        GPBUtil::checkInt64($var);
        $this->time = $var;

        return $this;
    }

    /**
     * 1 for active arenas, 2 for inactive
     *
     * Generated from protobuf field <code>int32 active = 5;</code>
     * @return int
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * 1 for active arenas, 2 for inactive
     *
     * Generated from protobuf field <code>int32 active = 5;</code>
     * @param int $var
     * @return $this
     */
    public function setActive($var)
    {
        GPBUtil::checkInt32($var);
        $this->active = $var;

        return $this;
    }

    /**
     * Required fields separated by comma
     *
     * Generated from protobuf field <code>string fields = 6;</code>
     * @return string
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * Required fields separated by comma
     *
     * Generated from protobuf field <code>string fields = 6;</code>
     * @param string $var
     * @return $this
     */
    public function setFields($var)
    {
        GPBUtil::checkString($var, True);
        $this->fields = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>.openapi.ArenaSorting sorting = 7;</code>
     * @return \Openapi\ArenaSorting
     */
    public function getSorting()
    {
        return $this->sorting;
    }

    /**
     * Generated from protobuf field <code>.openapi.ArenaSorting sorting = 7;</code>
     * @param \Openapi\ArenaSorting $var
     * @return $this
     */
    public function setSorting($var)
    {
        GPBUtil::checkMessage($var, \Openapi\ArenaSorting::class);
        $this->sorting = $var;

        return $this;
    }

}

