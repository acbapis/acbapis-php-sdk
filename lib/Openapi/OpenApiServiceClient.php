<?php
// GENERATED CODE -- DO NOT EDIT!

namespace Openapi;

/**
 */
class OpenApiServiceClient extends \Grpc\BaseStub {

    /**
     * @param string $hostname hostname
     * @param array $opts channel options
     * @param \Grpc\Channel $channel (optional) re-use channel object
     */
    public function __construct($hostname, $opts, $channel = null) {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * @param \Openapi\DenominationFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListDenominations(\Openapi\DenominationFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/ListDenominations',
        $argument,
        ['\Openapi\DenominationArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Openapi\CompetitionFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListCompetitions(\Openapi\CompetitionFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/ListCompetitions',
        $argument,
        ['\Openapi\CompetitionArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Openapi\FindByIdFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindCompetitionByID(\Openapi\FindByIdFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/FindCompetitionByID',
        $argument,
        ['\Openapi\Competition', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Openapi\EditionFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListEditions(\Openapi\EditionFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/ListEditions',
        $argument,
        ['\Openapi\EditionArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Openapi\FindByIdFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindEditionByID(\Openapi\FindByIdFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/FindEditionByID',
        $argument,
        ['\Openapi\Edition', 'decode'],
        $metadata, $options);
    }

    /**
     * FindEditionByCompetition returns the current or last edition of a competition
     * @param \Openapi\FindByIdFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindEditionByCompetition(\Openapi\FindByIdFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/FindEditionByCompetition',
        $argument,
        ['\Openapi\Edition', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Openapi\ClubFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListClubs(\Openapi\ClubFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/ListClubs',
        $argument,
        ['\Openapi\ClubArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Openapi\FindByIdFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindClubByID(\Openapi\FindByIdFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/FindClubByID',
        $argument,
        ['\Openapi\Club', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Openapi\TeamFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListTeams(\Openapi\TeamFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/ListTeams',
        $argument,
        ['\Openapi\TeamArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Openapi\FindTeamByIdFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindTeamByID(\Openapi\FindTeamByIdFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/FindTeamByID',
        $argument,
        ['\Openapi\Team', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Openapi\TeamFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListSimpleTeams(\Openapi\TeamFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/ListSimpleTeams',
        $argument,
        ['\Openapi\SimpleTeamArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Openapi\FindTeamByIdFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindSimpleTeamByID(\Openapi\FindTeamByIdFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/FindSimpleTeamByID',
        $argument,
        ['\Openapi\SimpleTeam', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetTeamBackground(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/GetTeamBackground',
        $argument,
        ['\Openapi\Background', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Openapi\TeamStatsFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetTeamStats(\Openapi\TeamStatsFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/GetTeamStats',
        $argument,
        ['\Openapi\StatsLine', 'decode'],
        $metadata, $options);
    }

    /**
     * ListParticipations returns teams that participate in an Edition. Current
     * Edition is used if none is specified
     * @param \Openapi\ParticipationFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListParticipations(\Openapi\ParticipationFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/ListParticipations',
        $argument,
        ['\Openapi\ParticipationArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Openapi\ArenaFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListArenas(\Openapi\ArenaFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/ListArenas',
        $argument,
        ['\Openapi\ArenaArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Openapi\FindByIdFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindArenaByID(\Openapi\FindByIdFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/FindArenaByID',
        $argument,
        ['\Openapi\Arena', 'decode'],
        $metadata, $options);
    }

    /**
     * *
     * GetSchedule returns match days grouped by competition. An Edition must be selected
     * by using one of the following filter combinations:
     * - editionId
     * - competitionId, editionNum
     * - competitionStr, editionNum
     * - comped
     * @param \Openapi\ScheduleFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetSchedule(\Openapi\ScheduleFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/GetSchedule',
        $argument,
        ['\Openapi\Schedule', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Openapi\ScheduleFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function StreamSchedule(\Openapi\ScheduleFilter $argument,
      $metadata = [], $options = []) {
        return $this->_serverStreamRequest('/openapi.OpenApiService/StreamSchedule',
        $argument,
        ['\Openapi\ScheduleWeek', 'decode'],
        $metadata, $options);
    }

    /**
     * *
     * GetMatchSchedule returns match days grouped by competition and with match information
     * An Edition must be selected by using one of the following filter combinations:
     * - editionId
     * - competitionId, editionNum
     * - competitionStr, editionNum
     * - comped
     * GetMatchSchedule returns a maximum of 3 weeks
     * @param \Openapi\ScheduleFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetMatchSchedule(\Openapi\ScheduleFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/GetMatchSchedule',
        $argument,
        ['\Openapi\MatchSchedule', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Openapi\ScheduleFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function StreamMatchSchedule(\Openapi\ScheduleFilter $argument,
      $metadata = [], $options = []) {
        return $this->_serverStreamRequest('/openapi.OpenApiService/StreamMatchSchedule',
        $argument,
        ['\Openapi\MatchScheduleWeek', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Openapi\SimpleScheduleFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetSimpleMatchSchedule(\Openapi\SimpleScheduleFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/GetSimpleMatchSchedule',
        $argument,
        ['\Openapi\SimpleMatchSchedule', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Openapi\MatchFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListMatches(\Openapi\MatchFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/ListMatches',
        $argument,
        ['\Openapi\MatchArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Openapi\MatchFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function StreamMatches(\Openapi\MatchFilter $argument,
      $metadata = [], $options = []) {
        return $this->_serverStreamRequest('/openapi.OpenApiService/StreamMatches',
        $argument,
        ['\Openapi\Match', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Openapi\FindMatchByIdFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindMatchByID(\Openapi\FindMatchByIdFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/FindMatchByID',
        $argument,
        ['\Openapi\MatchDetail', 'decode'],
        $metadata, $options);
    }

    /**
     * StreamMatch allows the client to receive real-time updates of a running match
     * @param \Openapi\FindMatchByIdFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function StreamMatch(\Openapi\FindMatchByIdFilter $argument,
      $metadata = [], $options = []) {
        return $this->_serverStreamRequest('/openapi.OpenApiService/StreamMatch',
        $argument,
        ['\Openapi\MatchDetail', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Openapi\PrecedentFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetPrecedents(\Openapi\PrecedentFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/GetPrecedents',
        $argument,
        ['\Openapi\Precedents', 'decode'],
        $metadata, $options);
    }

    /**
     * ListWeeks returns a maximum of 3 Week objects
     * @param \Openapi\WeekFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListWeeks(\Openapi\WeekFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/ListWeeks',
        $argument,
        ['\Openapi\WeekArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Openapi\WeekFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function StreamWeeks(\Openapi\WeekFilter $argument,
      $metadata = [], $options = []) {
        return $this->_serverStreamRequest('/openapi.OpenApiService/StreamWeeks',
        $argument,
        ['\Openapi\Week', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Openapi\FindByIdFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindWeekByID(\Openapi\FindByIdFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/FindWeekByID',
        $argument,
        ['\Openapi\Week', 'decode'],
        $metadata, $options);
    }

    /**
     * FindWeek returns a Week filtering by competition, edition and week number
     * @param \Openapi\FindWeekFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindWeek(\Openapi\FindWeekFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/FindWeek',
        $argument,
        ['\Openapi\Week', 'decode'],
        $metadata, $options);
    }

    /**
     * FindWeekByEdition returns the current or next week of an edition
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindWeekByEdition(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/FindWeekByEdition',
        $argument,
        ['\Openapi\Week', 'decode'],
        $metadata, $options);
    }

    /**
     * ListStandings returns team standings grouped by Week. Current Week is used
     * if no filters are specified
     * @param \Openapi\StandingFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListStandings(\Openapi\StandingFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/ListStandings',
        $argument,
        ['\Openapi\StandingArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Openapi\StandingFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function StreamStandings(\Openapi\StandingFilter $argument,
      $metadata = [], $options = []) {
        return $this->_serverStreamRequest('/openapi.OpenApiService/StreamStandings',
        $argument,
        ['\Openapi\Standing', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Openapi\PersonFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListPersons(\Openapi\PersonFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/ListPersons',
        $argument,
        ['\Openapi\PersonArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Openapi\SearchPersonsFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function SearchPersons(\Openapi\SearchPersonsFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/SearchPersons',
        $argument,
        ['\Openapi\SearchPersonsResults', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Openapi\FindByIdFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindPersonByID(\Openapi\FindByIdFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/FindPersonByID',
        $argument,
        ['\Openapi\Person', 'decode'],
        $metadata, $options);
    }

    /**
     * ListPlayers returns player licenses with their associated person data
     * @param \Openapi\LicenseFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListPlayers(\Openapi\LicenseFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/ListPlayers',
        $argument,
        ['\Openapi\LicenseArray', 'decode'],
        $metadata, $options);
    }

    /**
     * GetPlayerProfile returns a simpler version of License with the box score and social network information
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetPlayerProfile(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/GetPlayerProfile',
        $argument,
        ['\Openapi\PlayerProfile', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Openapi\PlayerStatsFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetPlayerStats(\Openapi\PlayerStatsFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/GetPlayerStats',
        $argument,
        ['\Openapi\StatsLine', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Openapi\PlayerProfileFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListPlayerProfiles(\Openapi\PlayerProfileFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/ListPlayerProfiles',
        $argument,
        ['\Openapi\PlayerProfileArray', 'decode'],
        $metadata, $options);
    }

    /**
     * ListCoaches returns coach licenses with their associated person data
     * @param \Openapi\LicenseFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListCoaches(\Openapi\LicenseFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/ListCoaches',
        $argument,
        ['\Openapi\LicenseArray', 'decode'],
        $metadata, $options);
    }

    /**
     * GetCoachProfile returns a simpler version of License with social network information
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetCoachProfile(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/GetCoachProfile',
        $argument,
        ['\Openapi\CoachProfile', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Openapi\CoachProfileFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListCoachProfiles(\Openapi\CoachProfileFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/ListCoachProfiles',
        $argument,
        ['\Openapi\CoachProfileArray', 'decode'],
        $metadata, $options);
    }

    /**
     * ListReferees returns referee licenses with their associated person data
     * @param \Openapi\LicenseFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListReferees(\Openapi\LicenseFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/ListReferees',
        $argument,
        ['\Openapi\LicenseArray', 'decode'],
        $metadata, $options);
    }

    /**
     * GetRefereeProfile returns a simpler version of License with social network information
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetRefereeProfile(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/GetRefereeProfile',
        $argument,
        ['\Openapi\RefereeProfile', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Openapi\RefereeProfileFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListRefereeProfiles(\Openapi\RefereeProfileFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/ListRefereeProfiles',
        $argument,
        ['\Openapi\RefereeProfileArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Openapi\LicenseFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListLicenses(\Openapi\LicenseFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/ListLicenses',
        $argument,
        ['\Openapi\LicenseArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Openapi\FindByIdFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindLicenseByID(\Openapi\FindByIdFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/FindLicenseByID',
        $argument,
        ['\Openapi\License', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Openapi\LicenseHistoriesFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetLicenseHistories(\Openapi\LicenseHistoriesFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/GetLicenseHistories',
        $argument,
        ['\Openapi\LicenseHistories', 'decode'],
        $metadata, $options);
    }

    /**
     * GetLicenseTeams returns all teams a License has been in
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetLicenseTeams(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/GetLicenseTeams',
        $argument,
        ['\Openapi\SimpleTeamArray', 'decode'],
        $metadata, $options);
    }

    /**
     * FindRostersByTeam returns the roster of a team in a edition. It uses
     * the current edition if none is specified
     * @param \Openapi\LicenseTeamFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindRosterByTeam(\Openapi\LicenseTeamFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/FindRosterByTeam',
        $argument,
        ['\Openapi\LicenseTeamArray', 'decode'],
        $metadata, $options);
    }

    /**
     * FindRostersByMatch returns the rosters of the teams in a match.
     * MatchId filter is obligatory
     * @param \Openapi\TeamMatchFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindRostersByMatch(\Openapi\TeamMatchFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/FindRostersByMatch',
        $argument,
        ['\Openapi\MatchRosters', 'decode'],
        $metadata, $options);
    }

    /**
     * ListStatusChanges returns status changes grouped by team
     * @param \Openapi\StatusChangeFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListStatusChanges(\Openapi\StatusChangeFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/ListStatusChanges',
        $argument,
        ['\Openapi\StatusChangeArray', 'decode'],
        $metadata, $options);
    }

    /**
     * ListStatusChangesByTeam returns status changes for a team
     * @param \Openapi\StatusChangeFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListStatusChangesByTeam(\Openapi\StatusChangeFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/ListStatusChangesByTeam',
        $argument,
        ['\Openapi\StatusChangeTeam', 'decode'],
        $metadata, $options);
    }

    /**
     * ListNews returns news with optionally their full article
     * @param \Openapi\NewsFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListNews(\Openapi\NewsFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/ListNews',
        $argument,
        ['\Openapi\NewsArray', 'decode'],
        $metadata, $options);
    }

    /**
     * ListNewsGrouped returns all news grouped by featured and promoted
     * @param \Openapi\NewsListGroupedFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListNewsGrouped(\Openapi\NewsListGroupedFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/ListNewsGrouped',
        $argument,
        ['\Openapi\NewsListGrouped', 'decode'],
        $metadata, $options);
    }

    /**
     * GetNewsContent returns the article text
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetNewsContent(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/GetNewsContent',
        $argument,
        ['\Openapi\NewsContent', 'decode'],
        $metadata, $options);
    }

    /**
     * * STATS METHODS *
     *
     * GetStatsCollection utiliza el filtro para selecciona la colección estadística que retornará. Es imprescindible indicar el tipo
     * puesto que no existe uno por defecto a diferencia de GetLeaders, GetTop5, GetAlltime, GetRecords que devolverán los tipos
     * estándar predeterminados en caso de obviar el tipo.
     * Si no se indicar valores para la competición, edición y fase se tomarán por defecto los de la que se encuentre en curso.
     * @param \Openapi\StatsItemCollectionFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetStatsCollection(\Openapi\StatsItemCollectionFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/GetStatsCollection',
        $argument,
        ['\Openapi\StatsItemCollection', 'decode'],
        $metadata, $options);
    }

    /**
     * GetTop5 construye el Top5 de puntos, rebotes, asistencias y valoración en una jornada, ordenados del 1 al 5.
     * los más : MÁXIMOS POR PARTIDO
     * @param \Openapi\Top5Filter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetTop5(\Openapi\Top5Filter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/GetTop5',
        $argument,
        ['\Openapi\StatsItemCollectionArray', 'decode'],
        $metadata, $options);
    }

    /**
     * GetLeaders devuelve los líderes en una edición. Permite filtrar por fase de la edición.
     * lideres : MÁXIMOS POR PROMEDIO/SUMA EN UN CONCEPTO ESTADÍSTICO EN UNA EDICIÓN
     * @param \Openapi\LeadersFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetLeaders(\Openapi\LeadersFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/GetLeaders',
        $argument,
        ['\Openapi\StatsItemCollection', 'decode'],
        $metadata, $options);
    }

    /**
     * GetAllTime devuelve los registros históricos por categoría atendiendo al filtro proporcionado.
     * historicos : MÁXIMOS DE SUMA EN UN CONCEPTO ESTADÍSTICO DE TODAS LAS EDICIONES DE UNA COMPETICIÓN
     * @param \Openapi\AllTimeFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetAllTime(\Openapi\AllTimeFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/GetAllTime',
        $argument,
        ['\Openapi\StatsItemCollection', 'decode'],
        $metadata, $options);
    }

    /**
     * GetRecords devuelve los máximos topes agrupados atendiendo al filtro proporcionado
     * records: MÁXIMO DE TOPES POR COMPETICIÓN EN UN PARTIDO
     * @param \Openapi\RecordsFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetRecords(\Openapi\RecordsFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/GetRecords',
        $argument,
        ['\Openapi\StatsItemCollection', 'decode'],
        $metadata, $options);
    }

    /**
     * GetHighs devuelve los topes máximos por competición edición y fase (según filtros)
     * topes: MÁXIMOS POR EDICIÓN Y FASE AGRUPADOS POR LICENCIA
     * @param \Openapi\HighsFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetHighs(\Openapi\HighsFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/GetHighs',
        $argument,
        ['\Openapi\StatsItemCollection', 'decode'],
        $metadata, $options);
    }

    /**
     * GetBoxScore returns box scores from matches
     * @param \Openapi\BoxScoreFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetBoxScore(\Openapi\BoxScoreFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/GetBoxScore',
        $argument,
        ['\Openapi\BoxScoreMatchArray', 'decode'],
        $metadata, $options);
    }

    /**
     * GetMatchBoxScore returns the box scores for the matches a team participated in
     * @param \Openapi\BoxScoreFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetMatchBoxScore(\Openapi\BoxScoreFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/GetMatchBoxScore',
        $argument,
        ['\Openapi\BoxScoreMatch', 'decode'],
        $metadata, $options);
    }

    /**
     * GetRanking
     * @param \Openapi\RankingFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetRanking(\Openapi\RankingFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/GetRanking',
        $argument,
        ['\Openapi\Ranking', 'decode'],
        $metadata, $options);
    }

    /**
     * GetTeamsRankings
     * @param \Openapi\TeamsRankingsFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetTeamsRankings(\Openapi\TeamsRankingsFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/GetTeamsRankings',
        $argument,
        ['\Openapi\TeamsRankings', 'decode'],
        $metadata, $options);
    }

    /**
     * GetTeamsRankingList
     * @param \Openapi\TeamsRankingListFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetTeamsRankingList(\Openapi\TeamsRankingListFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/GetTeamsRankingList',
        $argument,
        ['\Openapi\TeamsRankingList', 'decode'],
        $metadata, $options);
    }

    /**
     * GetClubRanking
     * @param \Openapi\ClubRankingFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetClubRanking(\Openapi\ClubRankingFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/GetClubRanking',
        $argument,
        ['\Openapi\ClubRanking', 'decode'],
        $metadata, $options);
    }

    /**
     * GetTeamSeason
     * @param \Openapi\TeamSeasonFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetTeamSeason(\Openapi\TeamSeasonFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/GetTeamSeason',
        $argument,
        ['\Openapi\TeamSeason', 'decode'],
        $metadata, $options);
    }

    /**
     * GetCoachSeasons
     * @param \Openapi\CoachSeasonsFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetCoachSeasons(\Openapi\CoachSeasonsFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/GetCoachSeasons',
        $argument,
        ['\Openapi\CoachSeasons', 'decode'],
        $metadata, $options);
    }

    /**
     * GetCoachHighs
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetCoachHighs(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/GetCoachHighs',
        $argument,
        ['\Openapi\CoachHighs', 'decode'],
        $metadata, $options);
    }

    /**
     * GetCoachHistory
     * @param \Openapi\CoachHistoryFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetCoachHistory(\Openapi\CoachHistoryFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/GetCoachHistory',
        $argument,
        ['\Openapi\CoachHistory', 'decode'],
        $metadata, $options);
    }

    /**
     * GetPlayerMatches
     * @param \Openapi\PlayerMatchesFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetPlayerMatches(\Openapi\PlayerMatchesFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/GetPlayerMatches',
        $argument,
        ['\Openapi\PlayerMatches', 'decode'],
        $metadata, $options);
    }

    /**
     * GetPlayerSeasons
     * @param \Openapi\PlayerSeasonsFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetPlayerSeasons(\Openapi\PlayerSeasonsFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/GetPlayerSeasons',
        $argument,
        ['\Openapi\PlayerSeasons', 'decode'],
        $metadata, $options);
    }

    /**
     * GetPlayerHighs
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetPlayerHighs(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/GetPlayerHighs',
        $argument,
        ['\Openapi\PlayerHighs', 'decode'],
        $metadata, $options);
    }

    /**
     * GetPlayersRankingList
     * @param \Openapi\PlayersRankingListFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetPlayersRankingList(\Openapi\PlayersRankingListFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/GetPlayersRankingList',
        $argument,
        ['\Openapi\PlayersRankingList', 'decode'],
        $metadata, $options);
    }

    /**
     * * END STATS METHODS *
     *
     * GetPbp returns the play by play of a match
     * @param \Openapi\GetPbpFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetPbp(\Openapi\GetPbpFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/GetPbp',
        $argument,
        ['\Openapi\Pbp', 'decode'],
        $metadata, $options);
    }

    /**
     * ListPbp returns the play by play of a list of matches
     * @param \Openapi\PbpFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListPbp(\Openapi\PbpFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/ListPbp',
        $argument,
        ['\Openapi\PbpArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Openapi\CuadroHonorFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetCuadroHonor(\Openapi\CuadroHonorFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/GetCuadroHonor',
        $argument,
        ['\Openapi\CuadroHonor', 'decode'],
        $metadata, $options);
    }

    /**
     * *
     * GetAppHome returns returns the home configuration for a client app.
     * Defines contents to be shown and their order
     * @param \Common\EmptyMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetAppHome(\Common\EmptyMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/GetAppHome',
        $argument,
        ['\Openapi\HomeComposition', 'decode'],
        $metadata, $options);
    }

    /**
     * *
     * GetAppMenu returns the configuration for the menu of a client app.
     * Accepts the 'tipo' parameter. 1 for lateral menu and 2 for bottom menu
     * @param \Openapi\MenuFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetAppMenu(\Openapi\MenuFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/GetAppMenu',
        $argument,
        ['\Openapi\MenuComposition', 'decode'],
        $metadata, $options);
    }

    /**
     * *
     * GetSettings returns a message with the relevant data values that are active at the present moment.
     * It indicates the competition, season, round, week and whether we are in round time or out of it.
     * Equivalences in spanish:
     * - season      = temporada
     * - competition = competicion
     * - edition     = edicion
     * - round       = fase
     * - week        = jornada
     * @param \Common\EmptyMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetSettings(\Common\EmptyMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapi.OpenApiService/GetSettings',
        $argument,
        ['\Openapi\Settings', 'decode'],
        $metadata, $options);
    }

}
