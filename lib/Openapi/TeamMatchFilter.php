<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: openapi/openapi.proto

namespace Openapi;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>openapi.TeamMatchFilter</code>
 */
class TeamMatchFilter extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>int64 matchId = 1;</code>
     */
    private $matchId = 0;
    /**
     * Generated from protobuf field <code>int64 teamId = 2;</code>
     */
    private $teamId = 0;
    /**
     * Optional, use "visitor" or "home" for local and visiting team respectively
     *
     * Generated from protobuf field <code>string team = 3;</code>
     */
    private $team = '';
    /**
     * Required fields separated by comma
     *
     * Generated from protobuf field <code>string fields = 4;</code>
     */
    private $fields = '';

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type int|string $matchId
     *     @type int|string $teamId
     *     @type string $team
     *           Optional, use "visitor" or "home" for local and visiting team respectively
     *     @type string $fields
     *           Required fields separated by comma
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Openapi\Openapi::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>int64 matchId = 1;</code>
     * @return int|string
     */
    public function getMatchId()
    {
        return $this->matchId;
    }

    /**
     * Generated from protobuf field <code>int64 matchId = 1;</code>
     * @param int|string $var
     * @return $this
     */
    public function setMatchId($var)
    {
        GPBUtil::checkInt64($var);
        $this->matchId = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 teamId = 2;</code>
     * @return int|string
     */
    public function getTeamId()
    {
        return $this->teamId;
    }

    /**
     * Generated from protobuf field <code>int64 teamId = 2;</code>
     * @param int|string $var
     * @return $this
     */
    public function setTeamId($var)
    {
        GPBUtil::checkInt64($var);
        $this->teamId = $var;

        return $this;
    }

    /**
     * Optional, use "visitor" or "home" for local and visiting team respectively
     *
     * Generated from protobuf field <code>string team = 3;</code>
     * @return string
     */
    public function getTeam()
    {
        return $this->team;
    }

    /**
     * Optional, use "visitor" or "home" for local and visiting team respectively
     *
     * Generated from protobuf field <code>string team = 3;</code>
     * @param string $var
     * @return $this
     */
    public function setTeam($var)
    {
        GPBUtil::checkString($var, True);
        $this->team = $var;

        return $this;
    }

    /**
     * Required fields separated by comma
     *
     * Generated from protobuf field <code>string fields = 4;</code>
     * @return string
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * Required fields separated by comma
     *
     * Generated from protobuf field <code>string fields = 4;</code>
     * @param string $var
     * @return $this
     */
    public function setFields($var)
    {
        GPBUtil::checkString($var, True);
        $this->fields = $var;

        return $this;
    }

}

