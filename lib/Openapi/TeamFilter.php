<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: openapi/openapi.proto

namespace Openapi;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>openapi.TeamFilter</code>
 */
class TeamFilter extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>int64 clubId = 1;</code>
     */
    private $clubId = 0;
    /**
     * Generated from protobuf field <code>string competitionStr = 2;</code>
     */
    private $competitionStr = '';
    /**
     * Generated from protobuf field <code>int32 editionNum = 3;</code>
     */
    private $editionNum = 0;
    /**
     * Generated from protobuf field <code>int64 competitionId = 4;</code>
     */
    private $competitionId = 0;
    /**
     * Generated from protobuf field <code>int64 editionId = 5;</code>
     */
    private $editionId = 0;
    /**
     * Generated from protobuf field <code>string comped = 6;</code>
     */
    private $comped = '';
    /**
     * Generated from protobuf field <code>int64 offset = 7;</code>
     */
    private $offset = 0;
    /**
     * Max value and defaults to 25
     *
     * Generated from protobuf field <code>int64 limit = 8;</code>
     */
    private $limit = 0;
    /**
     * Generated from protobuf field <code>int64 start = 9;</code>
     */
    private $start = 0;
    /**
     * Generated from protobuf field <code>int64 end = 10;</code>
     */
    private $end = 0;
    /**
     * Required fields separated by comma
     *
     * Generated from protobuf field <code>string fields = 11;</code>
     */
    private $fields = '';
    /**
     * Generated from protobuf field <code>.openapi.TeamSorting sorting = 12;</code>
     */
    private $sorting = null;
    /**
     * Generated from protobuf field <code>repeated int64 editionIds = 13;</code>
     */
    private $editionIds;
    /**
     * Generated from protobuf field <code>repeated int64 ids = 14;</code>
     */
    private $ids;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type int|string $clubId
     *     @type string $competitionStr
     *     @type int $editionNum
     *     @type int|string $competitionId
     *     @type int|string $editionId
     *     @type string $comped
     *     @type int|string $offset
     *     @type int|string $limit
     *           Max value and defaults to 25
     *     @type int|string $start
     *     @type int|string $end
     *     @type string $fields
     *           Required fields separated by comma
     *     @type \Openapi\TeamSorting $sorting
     *     @type int[]|string[]|\Google\Protobuf\Internal\RepeatedField $editionIds
     *     @type int[]|string[]|\Google\Protobuf\Internal\RepeatedField $ids
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Openapi\Openapi::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>int64 clubId = 1;</code>
     * @return int|string
     */
    public function getClubId()
    {
        return $this->clubId;
    }

    /**
     * Generated from protobuf field <code>int64 clubId = 1;</code>
     * @param int|string $var
     * @return $this
     */
    public function setClubId($var)
    {
        GPBUtil::checkInt64($var);
        $this->clubId = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string competitionStr = 2;</code>
     * @return string
     */
    public function getCompetitionStr()
    {
        return $this->competitionStr;
    }

    /**
     * Generated from protobuf field <code>string competitionStr = 2;</code>
     * @param string $var
     * @return $this
     */
    public function setCompetitionStr($var)
    {
        GPBUtil::checkString($var, True);
        $this->competitionStr = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int32 editionNum = 3;</code>
     * @return int
     */
    public function getEditionNum()
    {
        return $this->editionNum;
    }

    /**
     * Generated from protobuf field <code>int32 editionNum = 3;</code>
     * @param int $var
     * @return $this
     */
    public function setEditionNum($var)
    {
        GPBUtil::checkInt32($var);
        $this->editionNum = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 competitionId = 4;</code>
     * @return int|string
     */
    public function getCompetitionId()
    {
        return $this->competitionId;
    }

    /**
     * Generated from protobuf field <code>int64 competitionId = 4;</code>
     * @param int|string $var
     * @return $this
     */
    public function setCompetitionId($var)
    {
        GPBUtil::checkInt64($var);
        $this->competitionId = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 editionId = 5;</code>
     * @return int|string
     */
    public function getEditionId()
    {
        return $this->editionId;
    }

    /**
     * Generated from protobuf field <code>int64 editionId = 5;</code>
     * @param int|string $var
     * @return $this
     */
    public function setEditionId($var)
    {
        GPBUtil::checkInt64($var);
        $this->editionId = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string comped = 6;</code>
     * @return string
     */
    public function getComped()
    {
        return $this->comped;
    }

    /**
     * Generated from protobuf field <code>string comped = 6;</code>
     * @param string $var
     * @return $this
     */
    public function setComped($var)
    {
        GPBUtil::checkString($var, True);
        $this->comped = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 offset = 7;</code>
     * @return int|string
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     * Generated from protobuf field <code>int64 offset = 7;</code>
     * @param int|string $var
     * @return $this
     */
    public function setOffset($var)
    {
        GPBUtil::checkInt64($var);
        $this->offset = $var;

        return $this;
    }

    /**
     * Max value and defaults to 25
     *
     * Generated from protobuf field <code>int64 limit = 8;</code>
     * @return int|string
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * Max value and defaults to 25
     *
     * Generated from protobuf field <code>int64 limit = 8;</code>
     * @param int|string $var
     * @return $this
     */
    public function setLimit($var)
    {
        GPBUtil::checkInt64($var);
        $this->limit = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 start = 9;</code>
     * @return int|string
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Generated from protobuf field <code>int64 start = 9;</code>
     * @param int|string $var
     * @return $this
     */
    public function setStart($var)
    {
        GPBUtil::checkInt64($var);
        $this->start = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 end = 10;</code>
     * @return int|string
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * Generated from protobuf field <code>int64 end = 10;</code>
     * @param int|string $var
     * @return $this
     */
    public function setEnd($var)
    {
        GPBUtil::checkInt64($var);
        $this->end = $var;

        return $this;
    }

    /**
     * Required fields separated by comma
     *
     * Generated from protobuf field <code>string fields = 11;</code>
     * @return string
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * Required fields separated by comma
     *
     * Generated from protobuf field <code>string fields = 11;</code>
     * @param string $var
     * @return $this
     */
    public function setFields($var)
    {
        GPBUtil::checkString($var, True);
        $this->fields = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>.openapi.TeamSorting sorting = 12;</code>
     * @return \Openapi\TeamSorting
     */
    public function getSorting()
    {
        return $this->sorting;
    }

    /**
     * Generated from protobuf field <code>.openapi.TeamSorting sorting = 12;</code>
     * @param \Openapi\TeamSorting $var
     * @return $this
     */
    public function setSorting($var)
    {
        GPBUtil::checkMessage($var, \Openapi\TeamSorting::class);
        $this->sorting = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>repeated int64 editionIds = 13;</code>
     * @return \Google\Protobuf\Internal\RepeatedField
     */
    public function getEditionIds()
    {
        return $this->editionIds;
    }

    /**
     * Generated from protobuf field <code>repeated int64 editionIds = 13;</code>
     * @param int[]|string[]|\Google\Protobuf\Internal\RepeatedField $var
     * @return $this
     */
    public function setEditionIds($var)
    {
        $arr = GPBUtil::checkRepeatedField($var, \Google\Protobuf\Internal\GPBType::INT64);
        $this->editionIds = $arr;

        return $this;
    }

    /**
     * Generated from protobuf field <code>repeated int64 ids = 14;</code>
     * @return \Google\Protobuf\Internal\RepeatedField
     */
    public function getIds()
    {
        return $this->ids;
    }

    /**
     * Generated from protobuf field <code>repeated int64 ids = 14;</code>
     * @param int[]|string[]|\Google\Protobuf\Internal\RepeatedField $var
     * @return $this
     */
    public function setIds($var)
    {
        $arr = GPBUtil::checkRepeatedField($var, \Google\Protobuf\Internal\GPBType::INT64);
        $this->ids = $arr;

        return $this;
    }

}

