<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: openapi/openapi.proto

namespace Openapi;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>openapi.CoachProfile</code>
 */
class CoachProfile extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>int64 id = 1;</code>
     */
    private $id = 0;
    /**
     * Generated from protobuf field <code>string name = 2;</code>
     */
    private $name = '';
    /**
     * Generated from protobuf field <code>string abbrev = 3;</code>
     */
    private $abbrev = '';
    /**
     * Generated from protobuf field <code>string placeOfBirth = 4;</code>
     */
    private $placeOfBirth = '';
    /**
     * Generated from protobuf field <code>string dateOfBirth = 5;</code>
     */
    private $dateOfBirth = '';
    /**
     * Generated from protobuf field <code>int32 age = 6;</code>
     */
    private $age = 0;
    /**
     * Generated from protobuf field <code>string photo = 7;</code>
     */
    private $photo = '';
    /**
     * Generated from protobuf field <code>string nationality = 8;</code>
     */
    private $nationality = '';
    /**
     * Generated from protobuf field <code>string license = 9;</code>
     */
    private $license = '';
    /**
     * Generated from protobuf field <code>int32 ACByears = 11;</code>
     */
    private $ACByears = 0;
    /**
     * Generated from protobuf field <code>repeated .openapi.SocialNetwork socialNetworks = 12;</code>
     */
    private $socialNetworks;
    /**
     * Generated from protobuf field <code>string name15 = 13;</code>
     */
    private $name15 = '';
    /**
     * Generated from protobuf field <code>string pronunciationUrl = 14;</code>
     */
    private $pronunciationUrl = '';
    /**
     * Generated from protobuf field <code>repeated .openapi.Image images = 15;</code>
     */
    private $images;
    /**
     * Generated from protobuf field <code>string shirtNumber = 16;</code>
     */
    private $shirtNumber = '';
    /**
     * Generated from protobuf field <code>.openapi.TeamLicense teamLicense = 17;</code>
     */
    private $teamLicense = null;
    /**
     * Generated from protobuf field <code>.openapi.Background background = 18;</code>
     */
    private $background = null;
    /**
     * Generated from protobuf field <code>string countryOfBirth = 19;</code>
     */
    private $countryOfBirth = '';

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type int|string $id
     *     @type string $name
     *     @type string $abbrev
     *     @type string $placeOfBirth
     *     @type string $dateOfBirth
     *     @type int $age
     *     @type string $photo
     *     @type string $nationality
     *     @type string $license
     *     @type int $ACByears
     *     @type \Openapi\SocialNetwork[]|\Google\Protobuf\Internal\RepeatedField $socialNetworks
     *     @type string $name15
     *     @type string $pronunciationUrl
     *     @type \Openapi\Image[]|\Google\Protobuf\Internal\RepeatedField $images
     *     @type string $shirtNumber
     *     @type \Openapi\TeamLicense $teamLicense
     *     @type \Openapi\Background $background
     *     @type string $countryOfBirth
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Openapi\Openapi::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>int64 id = 1;</code>
     * @return int|string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Generated from protobuf field <code>int64 id = 1;</code>
     * @param int|string $var
     * @return $this
     */
    public function setId($var)
    {
        GPBUtil::checkInt64($var);
        $this->id = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string name = 2;</code>
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Generated from protobuf field <code>string name = 2;</code>
     * @param string $var
     * @return $this
     */
    public function setName($var)
    {
        GPBUtil::checkString($var, True);
        $this->name = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string abbrev = 3;</code>
     * @return string
     */
    public function getAbbrev()
    {
        return $this->abbrev;
    }

    /**
     * Generated from protobuf field <code>string abbrev = 3;</code>
     * @param string $var
     * @return $this
     */
    public function setAbbrev($var)
    {
        GPBUtil::checkString($var, True);
        $this->abbrev = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string placeOfBirth = 4;</code>
     * @return string
     */
    public function getPlaceOfBirth()
    {
        return $this->placeOfBirth;
    }

    /**
     * Generated from protobuf field <code>string placeOfBirth = 4;</code>
     * @param string $var
     * @return $this
     */
    public function setPlaceOfBirth($var)
    {
        GPBUtil::checkString($var, True);
        $this->placeOfBirth = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string dateOfBirth = 5;</code>
     * @return string
     */
    public function getDateOfBirth()
    {
        return $this->dateOfBirth;
    }

    /**
     * Generated from protobuf field <code>string dateOfBirth = 5;</code>
     * @param string $var
     * @return $this
     */
    public function setDateOfBirth($var)
    {
        GPBUtil::checkString($var, True);
        $this->dateOfBirth = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int32 age = 6;</code>
     * @return int
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * Generated from protobuf field <code>int32 age = 6;</code>
     * @param int $var
     * @return $this
     */
    public function setAge($var)
    {
        GPBUtil::checkInt32($var);
        $this->age = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string photo = 7;</code>
     * @return string
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * Generated from protobuf field <code>string photo = 7;</code>
     * @param string $var
     * @return $this
     */
    public function setPhoto($var)
    {
        GPBUtil::checkString($var, True);
        $this->photo = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string nationality = 8;</code>
     * @return string
     */
    public function getNationality()
    {
        return $this->nationality;
    }

    /**
     * Generated from protobuf field <code>string nationality = 8;</code>
     * @param string $var
     * @return $this
     */
    public function setNationality($var)
    {
        GPBUtil::checkString($var, True);
        $this->nationality = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string license = 9;</code>
     * @return string
     */
    public function getLicense()
    {
        return $this->license;
    }

    /**
     * Generated from protobuf field <code>string license = 9;</code>
     * @param string $var
     * @return $this
     */
    public function setLicense($var)
    {
        GPBUtil::checkString($var, True);
        $this->license = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int32 ACByears = 11;</code>
     * @return int
     */
    public function getACByears()
    {
        return $this->ACByears;
    }

    /**
     * Generated from protobuf field <code>int32 ACByears = 11;</code>
     * @param int $var
     * @return $this
     */
    public function setACByears($var)
    {
        GPBUtil::checkInt32($var);
        $this->ACByears = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>repeated .openapi.SocialNetwork socialNetworks = 12;</code>
     * @return \Google\Protobuf\Internal\RepeatedField
     */
    public function getSocialNetworks()
    {
        return $this->socialNetworks;
    }

    /**
     * Generated from protobuf field <code>repeated .openapi.SocialNetwork socialNetworks = 12;</code>
     * @param \Openapi\SocialNetwork[]|\Google\Protobuf\Internal\RepeatedField $var
     * @return $this
     */
    public function setSocialNetworks($var)
    {
        $arr = GPBUtil::checkRepeatedField($var, \Google\Protobuf\Internal\GPBType::MESSAGE, \Openapi\SocialNetwork::class);
        $this->socialNetworks = $arr;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string name15 = 13;</code>
     * @return string
     */
    public function getName15()
    {
        return $this->name15;
    }

    /**
     * Generated from protobuf field <code>string name15 = 13;</code>
     * @param string $var
     * @return $this
     */
    public function setName15($var)
    {
        GPBUtil::checkString($var, True);
        $this->name15 = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string pronunciationUrl = 14;</code>
     * @return string
     */
    public function getPronunciationUrl()
    {
        return $this->pronunciationUrl;
    }

    /**
     * Generated from protobuf field <code>string pronunciationUrl = 14;</code>
     * @param string $var
     * @return $this
     */
    public function setPronunciationUrl($var)
    {
        GPBUtil::checkString($var, True);
        $this->pronunciationUrl = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>repeated .openapi.Image images = 15;</code>
     * @return \Google\Protobuf\Internal\RepeatedField
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Generated from protobuf field <code>repeated .openapi.Image images = 15;</code>
     * @param \Openapi\Image[]|\Google\Protobuf\Internal\RepeatedField $var
     * @return $this
     */
    public function setImages($var)
    {
        $arr = GPBUtil::checkRepeatedField($var, \Google\Protobuf\Internal\GPBType::MESSAGE, \Openapi\Image::class);
        $this->images = $arr;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string shirtNumber = 16;</code>
     * @return string
     */
    public function getShirtNumber()
    {
        return $this->shirtNumber;
    }

    /**
     * Generated from protobuf field <code>string shirtNumber = 16;</code>
     * @param string $var
     * @return $this
     */
    public function setShirtNumber($var)
    {
        GPBUtil::checkString($var, True);
        $this->shirtNumber = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>.openapi.TeamLicense teamLicense = 17;</code>
     * @return \Openapi\TeamLicense
     */
    public function getTeamLicense()
    {
        return $this->teamLicense;
    }

    /**
     * Generated from protobuf field <code>.openapi.TeamLicense teamLicense = 17;</code>
     * @param \Openapi\TeamLicense $var
     * @return $this
     */
    public function setTeamLicense($var)
    {
        GPBUtil::checkMessage($var, \Openapi\TeamLicense::class);
        $this->teamLicense = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>.openapi.Background background = 18;</code>
     * @return \Openapi\Background
     */
    public function getBackground()
    {
        return $this->background;
    }

    /**
     * Generated from protobuf field <code>.openapi.Background background = 18;</code>
     * @param \Openapi\Background $var
     * @return $this
     */
    public function setBackground($var)
    {
        GPBUtil::checkMessage($var, \Openapi\Background::class);
        $this->background = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string countryOfBirth = 19;</code>
     * @return string
     */
    public function getCountryOfBirth()
    {
        return $this->countryOfBirth;
    }

    /**
     * Generated from protobuf field <code>string countryOfBirth = 19;</code>
     * @param string $var
     * @return $this
     */
    public function setCountryOfBirth($var)
    {
        GPBUtil::checkString($var, True);
        $this->countryOfBirth = $var;

        return $this;
    }

}

