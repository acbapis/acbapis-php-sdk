<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: openapi/openapi.proto

namespace Openapi;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>openapi.PlayerStatsFilter</code>
 */
class PlayerStatsFilter extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>int64 licenseId = 1;</code>
     */
    private $licenseId = 0;
    /**
     * Generated from protobuf field <code>int64 competitionId = 2;</code>
     */
    private $competitionId = 0;
    /**
     * Generated from protobuf field <code>string competitionStr = 3;</code>
     */
    private $competitionStr = '';
    /**
     * Generated from protobuf field <code>string competitionAbbrev = 4;</code>
     */
    private $competitionAbbrev = '';
    /**
     * Generated from protobuf field <code>int64 editionId = 5;</code>
     */
    private $editionId = 0;
    /**
     * Generated from protobuf field <code>int32 editionNum = 6;</code>
     */
    private $editionNum = 0;
    /**
     * Generated from protobuf field <code>string editionStr = 7;</code>
     */
    private $editionStr = '';
    /**
     * Generated from protobuf field <code>int64 teamId = 8;</code>
     */
    private $teamId = 0;
    /**
     * Generated from protobuf field <code>string teamStr = 9;</code>
     */
    private $teamStr = '';
    /**
     * Generated from protobuf field <code>string teamAbbrev = 10;</code>
     */
    private $teamAbbrev = '';
    /**
     * Generated from protobuf field <code>bool averages = 11;</code>
     */
    private $averages = false;
    /**
     * Generated from protobuf field <code>int64 roundId = 12;</code>
     */
    private $roundId = 0;
    /**
     * Generated from protobuf field <code>int64 seasonId = 13;</code>
     */
    private $seasonId = 0;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type int|string $licenseId
     *     @type int|string $competitionId
     *     @type string $competitionStr
     *     @type string $competitionAbbrev
     *     @type int|string $editionId
     *     @type int $editionNum
     *     @type string $editionStr
     *     @type int|string $teamId
     *     @type string $teamStr
     *     @type string $teamAbbrev
     *     @type bool $averages
     *     @type int|string $roundId
     *     @type int|string $seasonId
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Openapi\Openapi::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>int64 licenseId = 1;</code>
     * @return int|string
     */
    public function getLicenseId()
    {
        return $this->licenseId;
    }

    /**
     * Generated from protobuf field <code>int64 licenseId = 1;</code>
     * @param int|string $var
     * @return $this
     */
    public function setLicenseId($var)
    {
        GPBUtil::checkInt64($var);
        $this->licenseId = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 competitionId = 2;</code>
     * @return int|string
     */
    public function getCompetitionId()
    {
        return $this->competitionId;
    }

    /**
     * Generated from protobuf field <code>int64 competitionId = 2;</code>
     * @param int|string $var
     * @return $this
     */
    public function setCompetitionId($var)
    {
        GPBUtil::checkInt64($var);
        $this->competitionId = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string competitionStr = 3;</code>
     * @return string
     */
    public function getCompetitionStr()
    {
        return $this->competitionStr;
    }

    /**
     * Generated from protobuf field <code>string competitionStr = 3;</code>
     * @param string $var
     * @return $this
     */
    public function setCompetitionStr($var)
    {
        GPBUtil::checkString($var, True);
        $this->competitionStr = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string competitionAbbrev = 4;</code>
     * @return string
     */
    public function getCompetitionAbbrev()
    {
        return $this->competitionAbbrev;
    }

    /**
     * Generated from protobuf field <code>string competitionAbbrev = 4;</code>
     * @param string $var
     * @return $this
     */
    public function setCompetitionAbbrev($var)
    {
        GPBUtil::checkString($var, True);
        $this->competitionAbbrev = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 editionId = 5;</code>
     * @return int|string
     */
    public function getEditionId()
    {
        return $this->editionId;
    }

    /**
     * Generated from protobuf field <code>int64 editionId = 5;</code>
     * @param int|string $var
     * @return $this
     */
    public function setEditionId($var)
    {
        GPBUtil::checkInt64($var);
        $this->editionId = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int32 editionNum = 6;</code>
     * @return int
     */
    public function getEditionNum()
    {
        return $this->editionNum;
    }

    /**
     * Generated from protobuf field <code>int32 editionNum = 6;</code>
     * @param int $var
     * @return $this
     */
    public function setEditionNum($var)
    {
        GPBUtil::checkInt32($var);
        $this->editionNum = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string editionStr = 7;</code>
     * @return string
     */
    public function getEditionStr()
    {
        return $this->editionStr;
    }

    /**
     * Generated from protobuf field <code>string editionStr = 7;</code>
     * @param string $var
     * @return $this
     */
    public function setEditionStr($var)
    {
        GPBUtil::checkString($var, True);
        $this->editionStr = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 teamId = 8;</code>
     * @return int|string
     */
    public function getTeamId()
    {
        return $this->teamId;
    }

    /**
     * Generated from protobuf field <code>int64 teamId = 8;</code>
     * @param int|string $var
     * @return $this
     */
    public function setTeamId($var)
    {
        GPBUtil::checkInt64($var);
        $this->teamId = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string teamStr = 9;</code>
     * @return string
     */
    public function getTeamStr()
    {
        return $this->teamStr;
    }

    /**
     * Generated from protobuf field <code>string teamStr = 9;</code>
     * @param string $var
     * @return $this
     */
    public function setTeamStr($var)
    {
        GPBUtil::checkString($var, True);
        $this->teamStr = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string teamAbbrev = 10;</code>
     * @return string
     */
    public function getTeamAbbrev()
    {
        return $this->teamAbbrev;
    }

    /**
     * Generated from protobuf field <code>string teamAbbrev = 10;</code>
     * @param string $var
     * @return $this
     */
    public function setTeamAbbrev($var)
    {
        GPBUtil::checkString($var, True);
        $this->teamAbbrev = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>bool averages = 11;</code>
     * @return bool
     */
    public function getAverages()
    {
        return $this->averages;
    }

    /**
     * Generated from protobuf field <code>bool averages = 11;</code>
     * @param bool $var
     * @return $this
     */
    public function setAverages($var)
    {
        GPBUtil::checkBool($var);
        $this->averages = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 roundId = 12;</code>
     * @return int|string
     */
    public function getRoundId()
    {
        return $this->roundId;
    }

    /**
     * Generated from protobuf field <code>int64 roundId = 12;</code>
     * @param int|string $var
     * @return $this
     */
    public function setRoundId($var)
    {
        GPBUtil::checkInt64($var);
        $this->roundId = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 seasonId = 13;</code>
     * @return int|string
     */
    public function getSeasonId()
    {
        return $this->seasonId;
    }

    /**
     * Generated from protobuf field <code>int64 seasonId = 13;</code>
     * @param int|string $var
     * @return $this
     */
    public function setSeasonId($var)
    {
        GPBUtil::checkInt64($var);
        $this->seasonId = $var;

        return $this;
    }

}

