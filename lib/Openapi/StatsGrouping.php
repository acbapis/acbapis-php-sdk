<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: openapi/openapi.proto

namespace Openapi;

use UnexpectedValueException;

/**
 * Protobuf type <code>openapi.StatsGrouping</code>
 */
class StatsGrouping
{
    /**
     * Generated from protobuf enum <code>PLAYER = 0;</code>
     */
    const PLAYER = 0;
    /**
     * Generated from protobuf enum <code>TEAM = 1;</code>
     */
    const TEAM = 1;

    private static $valueToName = [
        self::PLAYER => 'PLAYER',
        self::TEAM => 'TEAM',
    ];

    public static function name($value)
    {
        if (!isset(self::$valueToName[$value])) {
            throw new UnexpectedValueException(sprintf(
                    'Enum %s has no name defined for value %s', __CLASS__, $value));
        }
        return self::$valueToName[$value];
    }


    public static function value($name)
    {
        $const = __CLASS__ . '::' . strtoupper($name);
        if (!defined($const)) {
            throw new UnexpectedValueException(sprintf(
                    'Enum %s has no value defined for name %s', __CLASS__, $name));
        }
        return constant($const);
    }
}

