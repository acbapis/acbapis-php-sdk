<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: openapi/openapi.proto

namespace Openapi;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>openapi.Person</code>
 */
class Person extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>int64 id = 1;</code>
     */
    private $id = 0;
    /**
     * Generated from protobuf field <code>int64 dateOfBirth = 2;</code>
     */
    private $dateOfBirth = 0;
    /**
     * Generated from protobuf field <code>int64 dateOfPassing = 3;</code>
     */
    private $dateOfPassing = 0;
    /**
     * Generated from protobuf field <code>string name = 4;</code>
     */
    private $name = '';
    /**
     * Generated from protobuf field <code>string name2 = 5;</code>
     */
    private $name2 = '';
    /**
     * Generated from protobuf field <code>string surname = 6;</code>
     */
    private $surname = '';
    /**
     * Generated from protobuf field <code>string surname2 = 7;</code>
     */
    private $surname2 = '';
    /**
     * Generated from protobuf field <code>string placeOfBirth = 8;</code>
     */
    private $placeOfBirth = '';
    /**
     * 3-char country code
     *
     * Generated from protobuf field <code>string countryOfBirth = 9;</code>
     */
    private $countryOfBirth = '';
    /**
     * Generated from protobuf field <code>int32 height = 10;</code>
     */
    private $height = 0;
    /**
     * Generated from protobuf field <code>int32 weight = 11;</code>
     */
    private $weight = 0;
    /**
     * Generated from protobuf field <code>repeated .openapi.Image images = 12;</code>
     */
    private $images;
    /**
     * Generated from protobuf field <code>string pronunciationUrl = 13;</code>
     */
    private $pronunciationUrl = '';

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type int|string $id
     *     @type int|string $dateOfBirth
     *     @type int|string $dateOfPassing
     *     @type string $name
     *     @type string $name2
     *     @type string $surname
     *     @type string $surname2
     *     @type string $placeOfBirth
     *     @type string $countryOfBirth
     *           3-char country code
     *     @type int $height
     *     @type int $weight
     *     @type \Openapi\Image[]|\Google\Protobuf\Internal\RepeatedField $images
     *     @type string $pronunciationUrl
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Openapi\Openapi::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>int64 id = 1;</code>
     * @return int|string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Generated from protobuf field <code>int64 id = 1;</code>
     * @param int|string $var
     * @return $this
     */
    public function setId($var)
    {
        GPBUtil::checkInt64($var);
        $this->id = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 dateOfBirth = 2;</code>
     * @return int|string
     */
    public function getDateOfBirth()
    {
        return $this->dateOfBirth;
    }

    /**
     * Generated from protobuf field <code>int64 dateOfBirth = 2;</code>
     * @param int|string $var
     * @return $this
     */
    public function setDateOfBirth($var)
    {
        GPBUtil::checkInt64($var);
        $this->dateOfBirth = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 dateOfPassing = 3;</code>
     * @return int|string
     */
    public function getDateOfPassing()
    {
        return $this->dateOfPassing;
    }

    /**
     * Generated from protobuf field <code>int64 dateOfPassing = 3;</code>
     * @param int|string $var
     * @return $this
     */
    public function setDateOfPassing($var)
    {
        GPBUtil::checkInt64($var);
        $this->dateOfPassing = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string name = 4;</code>
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Generated from protobuf field <code>string name = 4;</code>
     * @param string $var
     * @return $this
     */
    public function setName($var)
    {
        GPBUtil::checkString($var, True);
        $this->name = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string name2 = 5;</code>
     * @return string
     */
    public function getName2()
    {
        return $this->name2;
    }

    /**
     * Generated from protobuf field <code>string name2 = 5;</code>
     * @param string $var
     * @return $this
     */
    public function setName2($var)
    {
        GPBUtil::checkString($var, True);
        $this->name2 = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string surname = 6;</code>
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Generated from protobuf field <code>string surname = 6;</code>
     * @param string $var
     * @return $this
     */
    public function setSurname($var)
    {
        GPBUtil::checkString($var, True);
        $this->surname = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string surname2 = 7;</code>
     * @return string
     */
    public function getSurname2()
    {
        return $this->surname2;
    }

    /**
     * Generated from protobuf field <code>string surname2 = 7;</code>
     * @param string $var
     * @return $this
     */
    public function setSurname2($var)
    {
        GPBUtil::checkString($var, True);
        $this->surname2 = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string placeOfBirth = 8;</code>
     * @return string
     */
    public function getPlaceOfBirth()
    {
        return $this->placeOfBirth;
    }

    /**
     * Generated from protobuf field <code>string placeOfBirth = 8;</code>
     * @param string $var
     * @return $this
     */
    public function setPlaceOfBirth($var)
    {
        GPBUtil::checkString($var, True);
        $this->placeOfBirth = $var;

        return $this;
    }

    /**
     * 3-char country code
     *
     * Generated from protobuf field <code>string countryOfBirth = 9;</code>
     * @return string
     */
    public function getCountryOfBirth()
    {
        return $this->countryOfBirth;
    }

    /**
     * 3-char country code
     *
     * Generated from protobuf field <code>string countryOfBirth = 9;</code>
     * @param string $var
     * @return $this
     */
    public function setCountryOfBirth($var)
    {
        GPBUtil::checkString($var, True);
        $this->countryOfBirth = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int32 height = 10;</code>
     * @return int
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Generated from protobuf field <code>int32 height = 10;</code>
     * @param int $var
     * @return $this
     */
    public function setHeight($var)
    {
        GPBUtil::checkInt32($var);
        $this->height = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int32 weight = 11;</code>
     * @return int
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Generated from protobuf field <code>int32 weight = 11;</code>
     * @param int $var
     * @return $this
     */
    public function setWeight($var)
    {
        GPBUtil::checkInt32($var);
        $this->weight = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>repeated .openapi.Image images = 12;</code>
     * @return \Google\Protobuf\Internal\RepeatedField
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Generated from protobuf field <code>repeated .openapi.Image images = 12;</code>
     * @param \Openapi\Image[]|\Google\Protobuf\Internal\RepeatedField $var
     * @return $this
     */
    public function setImages($var)
    {
        $arr = GPBUtil::checkRepeatedField($var, \Google\Protobuf\Internal\GPBType::MESSAGE, \Openapi\Image::class);
        $this->images = $arr;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string pronunciationUrl = 13;</code>
     * @return string
     */
    public function getPronunciationUrl()
    {
        return $this->pronunciationUrl;
    }

    /**
     * Generated from protobuf field <code>string pronunciationUrl = 13;</code>
     * @param string $var
     * @return $this
     */
    public function setPronunciationUrl($var)
    {
        GPBUtil::checkString($var, True);
        $this->pronunciationUrl = $var;

        return $this;
    }

}

