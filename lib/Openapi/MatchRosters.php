<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: openapi/openapi.proto

namespace Openapi;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>openapi.MatchRosters</code>
 */
class MatchRosters extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>int64 localTeamId = 1;</code>
     */
    private $localTeamId = 0;
    /**
     * Generated from protobuf field <code>string localTeamName = 2;</code>
     */
    private $localTeamName = '';
    /**
     * Generated from protobuf field <code>string localTeamAbbrev = 3;</code>
     */
    private $localTeamAbbrev = '';
    /**
     * Generated from protobuf field <code>.openapi.LicenseTeamArray localTeam = 4;</code>
     */
    private $localTeam = null;
    /**
     * Generated from protobuf field <code>int64 visitingTeamId = 5;</code>
     */
    private $visitingTeamId = 0;
    /**
     * Generated from protobuf field <code>string visitingTeamName = 6;</code>
     */
    private $visitingTeamName = '';
    /**
     * Generated from protobuf field <code>string visitingTeamAbbrev = 7;</code>
     */
    private $visitingTeamAbbrev = '';
    /**
     * Generated from protobuf field <code>.openapi.LicenseTeamArray visitingTeam = 8;</code>
     */
    private $visitingTeam = null;
    /**
     * Generated from protobuf field <code>string localTeamName15 = 9;</code>
     */
    private $localTeamName15 = '';
    /**
     * Generated from protobuf field <code>string visitingTeamName15 = 10;</code>
     */
    private $visitingTeamName15 = '';

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type int|string $localTeamId
     *     @type string $localTeamName
     *     @type string $localTeamAbbrev
     *     @type \Openapi\LicenseTeamArray $localTeam
     *     @type int|string $visitingTeamId
     *     @type string $visitingTeamName
     *     @type string $visitingTeamAbbrev
     *     @type \Openapi\LicenseTeamArray $visitingTeam
     *     @type string $localTeamName15
     *     @type string $visitingTeamName15
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Openapi\Openapi::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>int64 localTeamId = 1;</code>
     * @return int|string
     */
    public function getLocalTeamId()
    {
        return $this->localTeamId;
    }

    /**
     * Generated from protobuf field <code>int64 localTeamId = 1;</code>
     * @param int|string $var
     * @return $this
     */
    public function setLocalTeamId($var)
    {
        GPBUtil::checkInt64($var);
        $this->localTeamId = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string localTeamName = 2;</code>
     * @return string
     */
    public function getLocalTeamName()
    {
        return $this->localTeamName;
    }

    /**
     * Generated from protobuf field <code>string localTeamName = 2;</code>
     * @param string $var
     * @return $this
     */
    public function setLocalTeamName($var)
    {
        GPBUtil::checkString($var, True);
        $this->localTeamName = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string localTeamAbbrev = 3;</code>
     * @return string
     */
    public function getLocalTeamAbbrev()
    {
        return $this->localTeamAbbrev;
    }

    /**
     * Generated from protobuf field <code>string localTeamAbbrev = 3;</code>
     * @param string $var
     * @return $this
     */
    public function setLocalTeamAbbrev($var)
    {
        GPBUtil::checkString($var, True);
        $this->localTeamAbbrev = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>.openapi.LicenseTeamArray localTeam = 4;</code>
     * @return \Openapi\LicenseTeamArray
     */
    public function getLocalTeam()
    {
        return $this->localTeam;
    }

    /**
     * Generated from protobuf field <code>.openapi.LicenseTeamArray localTeam = 4;</code>
     * @param \Openapi\LicenseTeamArray $var
     * @return $this
     */
    public function setLocalTeam($var)
    {
        GPBUtil::checkMessage($var, \Openapi\LicenseTeamArray::class);
        $this->localTeam = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 visitingTeamId = 5;</code>
     * @return int|string
     */
    public function getVisitingTeamId()
    {
        return $this->visitingTeamId;
    }

    /**
     * Generated from protobuf field <code>int64 visitingTeamId = 5;</code>
     * @param int|string $var
     * @return $this
     */
    public function setVisitingTeamId($var)
    {
        GPBUtil::checkInt64($var);
        $this->visitingTeamId = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string visitingTeamName = 6;</code>
     * @return string
     */
    public function getVisitingTeamName()
    {
        return $this->visitingTeamName;
    }

    /**
     * Generated from protobuf field <code>string visitingTeamName = 6;</code>
     * @param string $var
     * @return $this
     */
    public function setVisitingTeamName($var)
    {
        GPBUtil::checkString($var, True);
        $this->visitingTeamName = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string visitingTeamAbbrev = 7;</code>
     * @return string
     */
    public function getVisitingTeamAbbrev()
    {
        return $this->visitingTeamAbbrev;
    }

    /**
     * Generated from protobuf field <code>string visitingTeamAbbrev = 7;</code>
     * @param string $var
     * @return $this
     */
    public function setVisitingTeamAbbrev($var)
    {
        GPBUtil::checkString($var, True);
        $this->visitingTeamAbbrev = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>.openapi.LicenseTeamArray visitingTeam = 8;</code>
     * @return \Openapi\LicenseTeamArray
     */
    public function getVisitingTeam()
    {
        return $this->visitingTeam;
    }

    /**
     * Generated from protobuf field <code>.openapi.LicenseTeamArray visitingTeam = 8;</code>
     * @param \Openapi\LicenseTeamArray $var
     * @return $this
     */
    public function setVisitingTeam($var)
    {
        GPBUtil::checkMessage($var, \Openapi\LicenseTeamArray::class);
        $this->visitingTeam = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string localTeamName15 = 9;</code>
     * @return string
     */
    public function getLocalTeamName15()
    {
        return $this->localTeamName15;
    }

    /**
     * Generated from protobuf field <code>string localTeamName15 = 9;</code>
     * @param string $var
     * @return $this
     */
    public function setLocalTeamName15($var)
    {
        GPBUtil::checkString($var, True);
        $this->localTeamName15 = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string visitingTeamName15 = 10;</code>
     * @return string
     */
    public function getVisitingTeamName15()
    {
        return $this->visitingTeamName15;
    }

    /**
     * Generated from protobuf field <code>string visitingTeamName15 = 10;</code>
     * @param string $var
     * @return $this
     */
    public function setVisitingTeamName15($var)
    {
        GPBUtil::checkString($var, True);
        $this->visitingTeamName15 = $var;

        return $this;
    }

}

