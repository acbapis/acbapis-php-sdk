<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: openapi/openapi.proto

namespace Openapi;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>openapi.LicenseTeam</code>
 */
class LicenseTeam extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>int64 id = 1;</code>
     */
    private $id = 0;
    /**
     * Generated from protobuf field <code>int64 startDate = 2;</code>
     */
    private $startDate = 0;
    /**
     * Generated from protobuf field <code>int64 endDate = 3;</code>
     */
    private $endDate = 0;
    /**
     * Generated from protobuf field <code>int64 licenseId = 4;</code>
     */
    private $licenseId = 0;
    /**
     * Used when obtaining licenses, not needed in POST or PUT
     *
     * Generated from protobuf field <code>.openapi.License license = 5;</code>
     */
    private $license = null;
    /**
     * Generated from protobuf field <code>int64 teamId = 6;</code>
     */
    private $teamId = 0;
    /**
     * Generated from protobuf field <code>string rosterStatusType = 7;</code>
     */
    private $rosterStatusType = '';
    /**
     * Generated from protobuf field <code>bool active = 8;</code>
     */
    private $active = false;
    /**
     * Generated from protobuf field <code>string rosterState = 9;</code>
     */
    private $rosterState = '';
    /**
     * Generated from protobuf field <code>bool captain = 10;</code>
     */
    private $captain = false;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type int|string $id
     *     @type int|string $startDate
     *     @type int|string $endDate
     *     @type int|string $licenseId
     *     @type \Openapi\License $license
     *           Used when obtaining licenses, not needed in POST or PUT
     *     @type int|string $teamId
     *     @type string $rosterStatusType
     *     @type bool $active
     *     @type string $rosterState
     *     @type bool $captain
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Openapi\Openapi::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>int64 id = 1;</code>
     * @return int|string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Generated from protobuf field <code>int64 id = 1;</code>
     * @param int|string $var
     * @return $this
     */
    public function setId($var)
    {
        GPBUtil::checkInt64($var);
        $this->id = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 startDate = 2;</code>
     * @return int|string
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Generated from protobuf field <code>int64 startDate = 2;</code>
     * @param int|string $var
     * @return $this
     */
    public function setStartDate($var)
    {
        GPBUtil::checkInt64($var);
        $this->startDate = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 endDate = 3;</code>
     * @return int|string
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Generated from protobuf field <code>int64 endDate = 3;</code>
     * @param int|string $var
     * @return $this
     */
    public function setEndDate($var)
    {
        GPBUtil::checkInt64($var);
        $this->endDate = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 licenseId = 4;</code>
     * @return int|string
     */
    public function getLicenseId()
    {
        return $this->licenseId;
    }

    /**
     * Generated from protobuf field <code>int64 licenseId = 4;</code>
     * @param int|string $var
     * @return $this
     */
    public function setLicenseId($var)
    {
        GPBUtil::checkInt64($var);
        $this->licenseId = $var;

        return $this;
    }

    /**
     * Used when obtaining licenses, not needed in POST or PUT
     *
     * Generated from protobuf field <code>.openapi.License license = 5;</code>
     * @return \Openapi\License
     */
    public function getLicense()
    {
        return $this->license;
    }

    /**
     * Used when obtaining licenses, not needed in POST or PUT
     *
     * Generated from protobuf field <code>.openapi.License license = 5;</code>
     * @param \Openapi\License $var
     * @return $this
     */
    public function setLicense($var)
    {
        GPBUtil::checkMessage($var, \Openapi\License::class);
        $this->license = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 teamId = 6;</code>
     * @return int|string
     */
    public function getTeamId()
    {
        return $this->teamId;
    }

    /**
     * Generated from protobuf field <code>int64 teamId = 6;</code>
     * @param int|string $var
     * @return $this
     */
    public function setTeamId($var)
    {
        GPBUtil::checkInt64($var);
        $this->teamId = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string rosterStatusType = 7;</code>
     * @return string
     */
    public function getRosterStatusType()
    {
        return $this->rosterStatusType;
    }

    /**
     * Generated from protobuf field <code>string rosterStatusType = 7;</code>
     * @param string $var
     * @return $this
     */
    public function setRosterStatusType($var)
    {
        GPBUtil::checkString($var, True);
        $this->rosterStatusType = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>bool active = 8;</code>
     * @return bool
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Generated from protobuf field <code>bool active = 8;</code>
     * @param bool $var
     * @return $this
     */
    public function setActive($var)
    {
        GPBUtil::checkBool($var);
        $this->active = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string rosterState = 9;</code>
     * @return string
     */
    public function getRosterState()
    {
        return $this->rosterState;
    }

    /**
     * Generated from protobuf field <code>string rosterState = 9;</code>
     * @param string $var
     * @return $this
     */
    public function setRosterState($var)
    {
        GPBUtil::checkString($var, True);
        $this->rosterState = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>bool captain = 10;</code>
     * @return bool
     */
    public function getCaptain()
    {
        return $this->captain;
    }

    /**
     * Generated from protobuf field <code>bool captain = 10;</code>
     * @param bool $var
     * @return $this
     */
    public function setCaptain($var)
    {
        GPBUtil::checkBool($var);
        $this->captain = $var;

        return $this;
    }

}

