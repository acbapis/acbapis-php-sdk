<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: annotation/annotation.proto

namespace Annotation;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>annotation.DenominacionFilter</code>
 */
class DenominacionFilter extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>int64 offset = 1;</code>
     */
    private $offset = 0;
    /**
     * Generated from protobuf field <code>int64 limit = 2;</code>
     */
    private $limit = 0;
    /**
     * Generated from protobuf field <code>int64 id = 3;</code>
     */
    private $id = 0;
    /**
     * Tipo de entidad
     *
     * Generated from protobuf field <code>string tabla = 4;</code>
     */
    private $tabla = '';
    /**
     * Generated from protobuf field <code>int64 entidadId = 5;</code>
     */
    private $entidadId = 0;
    /**
     * Return rows with entidad_id IN [entidadIds]
     *
     * Generated from protobuf field <code>repeated int64 entidadIds = 6;</code>
     */
    private $entidadIds;
    /**
     * Generated from protobuf field <code>int64 time = 7;</code>
     */
    private $time = 0;
    /**
     * Se aplica con LIKE %{{denominacion}}%
     *
     * Generated from protobuf field <code>string denominacion = 8;</code>
     */
    private $denominacion = '';
    /**
     * Generated from protobuf field <code>int64 tipoId = 9;</code>
     */
    private $tipoId = 0;
    /**
     * Generated from protobuf field <code>repeated int64 tipoIds = 10;</code>
     */
    private $tipoIds;
    /**
     * Activo filter. 1 for active (true), 2 for inactive (false)
     *
     * Generated from protobuf field <code>int32 activo = 11;</code>
     */
    private $activo = 0;
    /**
     * Public filter. 1 for public denominaciones
     *
     * Generated from protobuf field <code>int32 public = 12;</code>
     */
    private $public = 0;
    /**
     * Wanted fields separated by comma
     *
     * Generated from protobuf field <code>string fields = 13;</code>
     */
    private $fields = '';
    /**
     * Generated from protobuf field <code>.annotation.DenominacionSorting sorting = 14;</code>
     */
    private $sorting = null;
    /**
     * Generated from protobuf field <code>string denominacionEq = 15;</code>
     */
    private $denominacionEq = '';
    /**
     * If true, response won't return total number of items
     *
     * Generated from protobuf field <code>bool omitCount = 16;</code>
     */
    private $omitCount = false;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type int|string $offset
     *     @type int|string $limit
     *     @type int|string $id
     *     @type string $tabla
     *           Tipo de entidad
     *     @type int|string $entidadId
     *     @type int[]|string[]|\Google\Protobuf\Internal\RepeatedField $entidadIds
     *           Return rows with entidad_id IN [entidadIds]
     *     @type int|string $time
     *     @type string $denominacion
     *           Se aplica con LIKE %{{denominacion}}%
     *     @type int|string $tipoId
     *     @type int[]|string[]|\Google\Protobuf\Internal\RepeatedField $tipoIds
     *     @type int $activo
     *           Activo filter. 1 for active (true), 2 for inactive (false)
     *     @type int $public
     *           Public filter. 1 for public denominaciones
     *     @type string $fields
     *           Wanted fields separated by comma
     *     @type \Annotation\DenominacionSorting $sorting
     *     @type string $denominacionEq
     *     @type bool $omitCount
     *           If true, response won't return total number of items
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Annotation\Annotation::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>int64 offset = 1;</code>
     * @return int|string
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     * Generated from protobuf field <code>int64 offset = 1;</code>
     * @param int|string $var
     * @return $this
     */
    public function setOffset($var)
    {
        GPBUtil::checkInt64($var);
        $this->offset = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 limit = 2;</code>
     * @return int|string
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * Generated from protobuf field <code>int64 limit = 2;</code>
     * @param int|string $var
     * @return $this
     */
    public function setLimit($var)
    {
        GPBUtil::checkInt64($var);
        $this->limit = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 id = 3;</code>
     * @return int|string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Generated from protobuf field <code>int64 id = 3;</code>
     * @param int|string $var
     * @return $this
     */
    public function setId($var)
    {
        GPBUtil::checkInt64($var);
        $this->id = $var;

        return $this;
    }

    /**
     * Tipo de entidad
     *
     * Generated from protobuf field <code>string tabla = 4;</code>
     * @return string
     */
    public function getTabla()
    {
        return $this->tabla;
    }

    /**
     * Tipo de entidad
     *
     * Generated from protobuf field <code>string tabla = 4;</code>
     * @param string $var
     * @return $this
     */
    public function setTabla($var)
    {
        GPBUtil::checkString($var, True);
        $this->tabla = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 entidadId = 5;</code>
     * @return int|string
     */
    public function getEntidadId()
    {
        return $this->entidadId;
    }

    /**
     * Generated from protobuf field <code>int64 entidadId = 5;</code>
     * @param int|string $var
     * @return $this
     */
    public function setEntidadId($var)
    {
        GPBUtil::checkInt64($var);
        $this->entidadId = $var;

        return $this;
    }

    /**
     * Return rows with entidad_id IN [entidadIds]
     *
     * Generated from protobuf field <code>repeated int64 entidadIds = 6;</code>
     * @return \Google\Protobuf\Internal\RepeatedField
     */
    public function getEntidadIds()
    {
        return $this->entidadIds;
    }

    /**
     * Return rows with entidad_id IN [entidadIds]
     *
     * Generated from protobuf field <code>repeated int64 entidadIds = 6;</code>
     * @param int[]|string[]|\Google\Protobuf\Internal\RepeatedField $var
     * @return $this
     */
    public function setEntidadIds($var)
    {
        $arr = GPBUtil::checkRepeatedField($var, \Google\Protobuf\Internal\GPBType::INT64);
        $this->entidadIds = $arr;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 time = 7;</code>
     * @return int|string
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Generated from protobuf field <code>int64 time = 7;</code>
     * @param int|string $var
     * @return $this
     */
    public function setTime($var)
    {
        GPBUtil::checkInt64($var);
        $this->time = $var;

        return $this;
    }

    /**
     * Se aplica con LIKE %{{denominacion}}%
     *
     * Generated from protobuf field <code>string denominacion = 8;</code>
     * @return string
     */
    public function getDenominacion()
    {
        return $this->denominacion;
    }

    /**
     * Se aplica con LIKE %{{denominacion}}%
     *
     * Generated from protobuf field <code>string denominacion = 8;</code>
     * @param string $var
     * @return $this
     */
    public function setDenominacion($var)
    {
        GPBUtil::checkString($var, True);
        $this->denominacion = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 tipoId = 9;</code>
     * @return int|string
     */
    public function getTipoId()
    {
        return $this->tipoId;
    }

    /**
     * Generated from protobuf field <code>int64 tipoId = 9;</code>
     * @param int|string $var
     * @return $this
     */
    public function setTipoId($var)
    {
        GPBUtil::checkInt64($var);
        $this->tipoId = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>repeated int64 tipoIds = 10;</code>
     * @return \Google\Protobuf\Internal\RepeatedField
     */
    public function getTipoIds()
    {
        return $this->tipoIds;
    }

    /**
     * Generated from protobuf field <code>repeated int64 tipoIds = 10;</code>
     * @param int[]|string[]|\Google\Protobuf\Internal\RepeatedField $var
     * @return $this
     */
    public function setTipoIds($var)
    {
        $arr = GPBUtil::checkRepeatedField($var, \Google\Protobuf\Internal\GPBType::INT64);
        $this->tipoIds = $arr;

        return $this;
    }

    /**
     * Activo filter. 1 for active (true), 2 for inactive (false)
     *
     * Generated from protobuf field <code>int32 activo = 11;</code>
     * @return int
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Activo filter. 1 for active (true), 2 for inactive (false)
     *
     * Generated from protobuf field <code>int32 activo = 11;</code>
     * @param int $var
     * @return $this
     */
    public function setActivo($var)
    {
        GPBUtil::checkInt32($var);
        $this->activo = $var;

        return $this;
    }

    /**
     * Public filter. 1 for public denominaciones
     *
     * Generated from protobuf field <code>int32 public = 12;</code>
     * @return int
     */
    public function getPublic()
    {
        return $this->public;
    }

    /**
     * Public filter. 1 for public denominaciones
     *
     * Generated from protobuf field <code>int32 public = 12;</code>
     * @param int $var
     * @return $this
     */
    public function setPublic($var)
    {
        GPBUtil::checkInt32($var);
        $this->public = $var;

        return $this;
    }

    /**
     * Wanted fields separated by comma
     *
     * Generated from protobuf field <code>string fields = 13;</code>
     * @return string
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * Wanted fields separated by comma
     *
     * Generated from protobuf field <code>string fields = 13;</code>
     * @param string $var
     * @return $this
     */
    public function setFields($var)
    {
        GPBUtil::checkString($var, True);
        $this->fields = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>.annotation.DenominacionSorting sorting = 14;</code>
     * @return \Annotation\DenominacionSorting
     */
    public function getSorting()
    {
        return $this->sorting;
    }

    /**
     * Generated from protobuf field <code>.annotation.DenominacionSorting sorting = 14;</code>
     * @param \Annotation\DenominacionSorting $var
     * @return $this
     */
    public function setSorting($var)
    {
        GPBUtil::checkMessage($var, \Annotation\DenominacionSorting::class);
        $this->sorting = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string denominacionEq = 15;</code>
     * @return string
     */
    public function getDenominacionEq()
    {
        return $this->denominacionEq;
    }

    /**
     * Generated from protobuf field <code>string denominacionEq = 15;</code>
     * @param string $var
     * @return $this
     */
    public function setDenominacionEq($var)
    {
        GPBUtil::checkString($var, True);
        $this->denominacionEq = $var;

        return $this;
    }

    /**
     * If true, response won't return total number of items
     *
     * Generated from protobuf field <code>bool omitCount = 16;</code>
     * @return bool
     */
    public function getOmitCount()
    {
        return $this->omitCount;
    }

    /**
     * If true, response won't return total number of items
     *
     * Generated from protobuf field <code>bool omitCount = 16;</code>
     * @param bool $var
     * @return $this
     */
    public function setOmitCount($var)
    {
        GPBUtil::checkBool($var);
        $this->omitCount = $var;

        return $this;
    }

}

