<?php
// GENERATED CODE -- DO NOT EDIT!

namespace Annotation;

/**
 */
class AnnotationServiceClient extends \Grpc\BaseStub {

    /**
     * @param string $hostname hostname
     * @param array $opts channel options
     * @param \Grpc\Channel $channel (optional) re-use channel object
     */
    public function __construct($hostname, $opts, $channel = null) {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * @param \Annotation\PersonaHistoriesFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetPersonaHistories(\Annotation\PersonaHistoriesFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/annotation.AnnotationService/GetPersonaHistories',
        $argument,
        ['\Annotation\PersonaHistories', 'decode'],
        $metadata, $options);
    }

    /**
     * ListEquivalencias obtiene una lista de las equivalencias contenidas en la base de datos que cumplen
     * con las restricciones impuestas por EquivalenciaFilter
     * @param \Annotation\EquivalenciaFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListEquivalencias(\Annotation\EquivalenciaFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/annotation.AnnotationService/ListEquivalencias',
        $argument,
        ['\Annotation\EquivalenciaArray', 'decode'],
        $metadata, $options);
    }

    /**
     * GetEquivalenciaCompact obtienen la equivalencia correspondiente al id proporcionado
     * @param \Annotation\EquivalenciaFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetEquivalenciaCompact(\Annotation\EquivalenciaFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/annotation.AnnotationService/GetEquivalenciaCompact',
        $argument,
        ['\Annotation\EquivalenciaCompact', 'decode'],
        $metadata, $options);
    }

    /**
     * ListEquivalenciasCompact obtiene una lista de las equivalencias contenidas en la base de datos que cumplen
     * con las restricciones impuestas por EquivalenciaFilter
     * @param \Annotation\EquivalenciaFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListEquivalenciasCompact(\Annotation\EquivalenciaFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/annotation.AnnotationService/ListEquivalenciasCompact',
        $argument,
        ['\Annotation\EquivalenciaCompactArray', 'decode'],
        $metadata, $options);
    }

    /**
     * Start Annotation endpoints
     * @param \Annotation\Annotation $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateAnnotation(\Annotation\Annotation $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/annotation.AnnotationService/CreateAnnotation',
        $argument,
        ['\Annotation\Annotation', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Annotation\Annotation $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateAnnotation(\Annotation\Annotation $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/annotation.AnnotationService/UpdateAnnotation',
        $argument,
        ['\Annotation\Annotation', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Annotation\AnnotationFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindAnnotationByID(\Annotation\AnnotationFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/annotation.AnnotationService/FindAnnotationByID',
        $argument,
        ['\Annotation\Annotation', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Annotation\AnnotationFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListAnnotations(\Annotation\AnnotationFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/annotation.AnnotationService/ListAnnotations',
        $argument,
        ['\Annotation\AnnotationArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteAnnotation(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/annotation.AnnotationService/DeleteAnnotation',
        $argument,
        ['\Annotation\Annotation', 'decode'],
        $metadata, $options);
    }

    /**
     * Start Denominacion endpoints
     * @param \Annotation\Denominacion $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateDenominacion(\Annotation\Denominacion $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/annotation.AnnotationService/CreateDenominacion',
        $argument,
        ['\Annotation\Denominacion', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Annotation\Denominacion $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateDenominacion(\Annotation\Denominacion $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/annotation.AnnotationService/UpdateDenominacion',
        $argument,
        ['\Annotation\Denominacion', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Annotation\DenominacionFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindDenominacionByID(\Annotation\DenominacionFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/annotation.AnnotationService/FindDenominacionByID',
        $argument,
        ['\Annotation\Denominacion', 'decode'],
        $metadata, $options);
    }

    /**
     * FindEntidadDenominacion devuelve las denominaciones de tipo 1, 6 y 9000 para una entidad
     * en un momento concreto. El filtro time se establece por defecto al momento actual
     * @param \Annotation\EntidadDenominacionFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindEntidadDenominacion(\Annotation\EntidadDenominacionFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/annotation.AnnotationService/FindEntidadDenominacion',
        $argument,
        ['\Annotation\EntidadDenominacion', 'decode'],
        $metadata, $options);
    }

    /**
     * ListEntidadDenominaciones devuelve las denominaciones de tipo 1, 6 y 9000 para entidades
     * definidas por los filtros. El filtro time se establece por defecto al momento actual
     * @param \Annotation\EntidadDenominacionFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListEntidadDenominaciones(\Annotation\EntidadDenominacionFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/annotation.AnnotationService/ListEntidadDenominaciones',
        $argument,
        ['\Annotation\EntidadDenominacionArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Annotation\DenominacionFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListDenominaciones(\Annotation\DenominacionFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/annotation.AnnotationService/ListDenominaciones',
        $argument,
        ['\Annotation\DenominacionArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteDenominacion(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/annotation.AnnotationService/DeleteDenominacion',
        $argument,
        ['\Annotation\Denominacion', 'decode'],
        $metadata, $options);
    }

    /**
     * Start AnnotationType endpoints
     * @param \Annotation\AnnotationType $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateAnnotationType(\Annotation\AnnotationType $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/annotation.AnnotationService/CreateAnnotationType',
        $argument,
        ['\Annotation\AnnotationType', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Annotation\AnnotationType $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateAnnotationType(\Annotation\AnnotationType $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/annotation.AnnotationService/UpdateAnnotationType',
        $argument,
        ['\Annotation\AnnotationType', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindAnnotationTypeByID(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/annotation.AnnotationService/FindAnnotationTypeByID',
        $argument,
        ['\Annotation\AnnotationType', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Annotation\AnnotationTypeFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListAnnotationTypes(\Annotation\AnnotationTypeFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/annotation.AnnotationService/ListAnnotationTypes',
        $argument,
        ['\Annotation\AnnotationTypeArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteAnnotationType(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/annotation.AnnotationService/DeleteAnnotationType',
        $argument,
        ['\Annotation\AnnotationType', 'decode'],
        $metadata, $options);
    }

    /**
     * Start HistoryRecord endpoints
     * @param \Annotation\HistoryRecord $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateHistoryRecord(\Annotation\HistoryRecord $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/annotation.AnnotationService/CreateHistoryRecord',
        $argument,
        ['\Annotation\HistoryRecord', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Annotation\HistoryRecord $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateHistoryRecord(\Annotation\HistoryRecord $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/annotation.AnnotationService/UpdateHistoryRecord',
        $argument,
        ['\Annotation\HistoryRecord', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Annotation\HistoryRecordFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindHistoryRecordByID(\Annotation\HistoryRecordFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/annotation.AnnotationService/FindHistoryRecordByID',
        $argument,
        ['\Annotation\HistoryRecord', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Annotation\HistoryRecordFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListHistoryRecords(\Annotation\HistoryRecordFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/annotation.AnnotationService/ListHistoryRecords',
        $argument,
        ['\Annotation\HistoryRecordArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteHistoryRecord(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/annotation.AnnotationService/DeleteHistoryRecord',
        $argument,
        ['\Annotation\HistoryRecord', 'decode'],
        $metadata, $options);
    }

    /**
     * Start HistoryType endpoints
     * @param \Annotation\HistoryType $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateHistoryType(\Annotation\HistoryType $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/annotation.AnnotationService/CreateHistoryType',
        $argument,
        ['\Annotation\HistoryType', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Annotation\HistoryType $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateHistoryType(\Annotation\HistoryType $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/annotation.AnnotationService/UpdateHistoryType',
        $argument,
        ['\Annotation\HistoryType', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindHistoryTypeByID(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/annotation.AnnotationService/FindHistoryTypeByID',
        $argument,
        ['\Annotation\HistoryType', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Annotation\HistoryTypeFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListHistoryTypes(\Annotation\HistoryTypeFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/annotation.AnnotationService/ListHistoryTypes',
        $argument,
        ['\Annotation\HistoryTypeArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteHistoryType(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/annotation.AnnotationService/DeleteHistoryType',
        $argument,
        ['\Annotation\HistoryType', 'decode'],
        $metadata, $options);
    }

    /**
     * Start TituloType endpoints
     * @param \Annotation\TituloType $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateTituloType(\Annotation\TituloType $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/annotation.AnnotationService/CreateTituloType',
        $argument,
        ['\Annotation\TituloType', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Annotation\TituloType $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateTituloType(\Annotation\TituloType $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/annotation.AnnotationService/UpdateTituloType',
        $argument,
        ['\Annotation\TituloType', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindTituloTypeByID(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/annotation.AnnotationService/FindTituloTypeByID',
        $argument,
        ['\Annotation\TituloType', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Annotation\TituloTypeFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListTituloTypes(\Annotation\TituloTypeFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/annotation.AnnotationService/ListTituloTypes',
        $argument,
        ['\Annotation\TituloTypeArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteTituloType(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/annotation.AnnotationService/DeleteTituloType',
        $argument,
        ['\Annotation\TituloType', 'decode'],
        $metadata, $options);
    }

}
