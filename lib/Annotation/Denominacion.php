<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: annotation/annotation.proto

namespace Annotation;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Denominacion Messages
 *
 * Generated from protobuf message <code>annotation.Denominacion</code>
 */
class Denominacion extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>int64 id = 1;</code>
     */
    private $id = 0;
    /**
     * Generated from protobuf field <code>int64 fechaAlta = 2;</code>
     */
    private $fechaAlta = 0;
    /**
     * Generated from protobuf field <code>int64 fechaBaja = 3;</code>
     */
    private $fechaBaja = 0;
    /**
     * Generated from protobuf field <code>string tabla = 4;</code>
     */
    private $tabla = '';
    /**
     * Generated from protobuf field <code>int64 entidadId = 5;</code>
     */
    private $entidadId = 0;
    /**
     * Generated from protobuf field <code>int64 tipoId = 6;</code>
     */
    private $tipoId = 0;
    /**
     * Generated from protobuf field <code>string denominacion = 7;</code>
     */
    private $denominacion = '';
    /**
     * Generated from protobuf field <code>int32 orden = 8;</code>
     */
    private $orden = 0;
    /**
     * Generated from protobuf field <code>bool activo = 9;</code>
     */
    private $activo = false;
    /**
     * Generated from protobuf field <code>int32 publicable = 10;</code>
     */
    private $publicable = 0;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type int|string $id
     *     @type int|string $fechaAlta
     *     @type int|string $fechaBaja
     *     @type string $tabla
     *     @type int|string $entidadId
     *     @type int|string $tipoId
     *     @type string $denominacion
     *     @type int $orden
     *     @type bool $activo
     *     @type int $publicable
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Annotation\Annotation::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>int64 id = 1;</code>
     * @return int|string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Generated from protobuf field <code>int64 id = 1;</code>
     * @param int|string $var
     * @return $this
     */
    public function setId($var)
    {
        GPBUtil::checkInt64($var);
        $this->id = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 fechaAlta = 2;</code>
     * @return int|string
     */
    public function getFechaAlta()
    {
        return $this->fechaAlta;
    }

    /**
     * Generated from protobuf field <code>int64 fechaAlta = 2;</code>
     * @param int|string $var
     * @return $this
     */
    public function setFechaAlta($var)
    {
        GPBUtil::checkInt64($var);
        $this->fechaAlta = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 fechaBaja = 3;</code>
     * @return int|string
     */
    public function getFechaBaja()
    {
        return $this->fechaBaja;
    }

    /**
     * Generated from protobuf field <code>int64 fechaBaja = 3;</code>
     * @param int|string $var
     * @return $this
     */
    public function setFechaBaja($var)
    {
        GPBUtil::checkInt64($var);
        $this->fechaBaja = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string tabla = 4;</code>
     * @return string
     */
    public function getTabla()
    {
        return $this->tabla;
    }

    /**
     * Generated from protobuf field <code>string tabla = 4;</code>
     * @param string $var
     * @return $this
     */
    public function setTabla($var)
    {
        GPBUtil::checkString($var, True);
        $this->tabla = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 entidadId = 5;</code>
     * @return int|string
     */
    public function getEntidadId()
    {
        return $this->entidadId;
    }

    /**
     * Generated from protobuf field <code>int64 entidadId = 5;</code>
     * @param int|string $var
     * @return $this
     */
    public function setEntidadId($var)
    {
        GPBUtil::checkInt64($var);
        $this->entidadId = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 tipoId = 6;</code>
     * @return int|string
     */
    public function getTipoId()
    {
        return $this->tipoId;
    }

    /**
     * Generated from protobuf field <code>int64 tipoId = 6;</code>
     * @param int|string $var
     * @return $this
     */
    public function setTipoId($var)
    {
        GPBUtil::checkInt64($var);
        $this->tipoId = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string denominacion = 7;</code>
     * @return string
     */
    public function getDenominacion()
    {
        return $this->denominacion;
    }

    /**
     * Generated from protobuf field <code>string denominacion = 7;</code>
     * @param string $var
     * @return $this
     */
    public function setDenominacion($var)
    {
        GPBUtil::checkString($var, True);
        $this->denominacion = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int32 orden = 8;</code>
     * @return int
     */
    public function getOrden()
    {
        return $this->orden;
    }

    /**
     * Generated from protobuf field <code>int32 orden = 8;</code>
     * @param int $var
     * @return $this
     */
    public function setOrden($var)
    {
        GPBUtil::checkInt32($var);
        $this->orden = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>bool activo = 9;</code>
     * @return bool
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Generated from protobuf field <code>bool activo = 9;</code>
     * @param bool $var
     * @return $this
     */
    public function setActivo($var)
    {
        GPBUtil::checkBool($var);
        $this->activo = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int32 publicable = 10;</code>
     * @return int
     */
    public function getPublicable()
    {
        return $this->publicable;
    }

    /**
     * Generated from protobuf field <code>int32 publicable = 10;</code>
     * @param int $var
     * @return $this
     */
    public function setPublicable($var)
    {
        GPBUtil::checkInt32($var);
        $this->publicable = $var;

        return $this;
    }

}

