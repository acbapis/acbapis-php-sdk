<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: arbitraje/arbapi.proto

namespace Arbitraje;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>arbitraje.AllocationTypeFilter</code>
 */
class AllocationTypeFilter extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>int64 offset = 1;</code>
     */
    private $offset = 0;
    /**
     * Generated from protobuf field <code>int64 limit = 2;</code>
     */
    private $limit = 0;
    /**
     * Generated from protobuf field <code>string terms = 3;</code>
     */
    private $terms = '';
    /**
     * Generated from protobuf field <code>.arbitraje.AllocationTypeSorting sorting = 4;</code>
     */
    private $sorting = null;
    /**
     * If true, returns all records
     *
     * Generated from protobuf field <code>bool unfiltered = 30;</code>
     */
    private $unfiltered = false;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type int|string $offset
     *     @type int|string $limit
     *     @type string $terms
     *     @type \Arbitraje\AllocationTypeSorting $sorting
     *     @type bool $unfiltered
     *           If true, returns all records
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Arbitraje\Arbapi::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>int64 offset = 1;</code>
     * @return int|string
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     * Generated from protobuf field <code>int64 offset = 1;</code>
     * @param int|string $var
     * @return $this
     */
    public function setOffset($var)
    {
        GPBUtil::checkInt64($var);
        $this->offset = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 limit = 2;</code>
     * @return int|string
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * Generated from protobuf field <code>int64 limit = 2;</code>
     * @param int|string $var
     * @return $this
     */
    public function setLimit($var)
    {
        GPBUtil::checkInt64($var);
        $this->limit = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string terms = 3;</code>
     * @return string
     */
    public function getTerms()
    {
        return $this->terms;
    }

    /**
     * Generated from protobuf field <code>string terms = 3;</code>
     * @param string $var
     * @return $this
     */
    public function setTerms($var)
    {
        GPBUtil::checkString($var, True);
        $this->terms = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>.arbitraje.AllocationTypeSorting sorting = 4;</code>
     * @return \Arbitraje\AllocationTypeSorting
     */
    public function getSorting()
    {
        return $this->sorting;
    }

    /**
     * Generated from protobuf field <code>.arbitraje.AllocationTypeSorting sorting = 4;</code>
     * @param \Arbitraje\AllocationTypeSorting $var
     * @return $this
     */
    public function setSorting($var)
    {
        GPBUtil::checkMessage($var, \Arbitraje\AllocationTypeSorting::class);
        $this->sorting = $var;

        return $this;
    }

    /**
     * If true, returns all records
     *
     * Generated from protobuf field <code>bool unfiltered = 30;</code>
     * @return bool
     */
    public function getUnfiltered()
    {
        return $this->unfiltered;
    }

    /**
     * If true, returns all records
     *
     * Generated from protobuf field <code>bool unfiltered = 30;</code>
     * @param bool $var
     * @return $this
     */
    public function setUnfiltered($var)
    {
        GPBUtil::checkBool($var);
        $this->unfiltered = $var;

        return $this;
    }

}

