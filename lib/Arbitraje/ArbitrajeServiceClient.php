<?php
// GENERATED CODE -- DO NOT EDIT!

namespace Arbitraje;

/**
 */
class ArbitrajeServiceClient extends \Grpc\BaseStub {

    /**
     * @param string $hostname hostname
     * @param array $opts channel options
     * @param \Grpc\Channel $channel (optional) re-use channel object
     */
    public function __construct($hostname, $opts, $channel = null) {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * * Start Allocation endpoints *
     * @param \Arbitraje\Allocation $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateAllocation(\Arbitraje\Allocation $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/arbitraje.ArbitrajeService/CreateAllocation',
        $argument,
        ['\Arbitraje\Allocation', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Arbitraje\Allocation $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateAllocation(\Arbitraje\Allocation $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/arbitraje.ArbitrajeService/UpdateAllocation',
        $argument,
        ['\Arbitraje\Allocation', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindAllocationByID(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/arbitraje.ArbitrajeService/FindAllocationByID',
        $argument,
        ['\Arbitraje\Allocation', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Arbitraje\AllocationFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListAllocations(\Arbitraje\AllocationFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/arbitraje.ArbitrajeService/ListAllocations',
        $argument,
        ['\Arbitraje\AllocationArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteAllocation(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/arbitraje.ArbitrajeService/DeleteAllocation',
        $argument,
        ['\Arbitraje\Allocation', 'decode'],
        $metadata, $options);
    }

    /**
     * * Start AllocationType endpoints *
     * @param \Arbitraje\AllocationType $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateAllocationType(\Arbitraje\AllocationType $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/arbitraje.ArbitrajeService/CreateAllocationType',
        $argument,
        ['\Arbitraje\AllocationType', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Arbitraje\AllocationType $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateAllocationType(\Arbitraje\AllocationType $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/arbitraje.ArbitrajeService/UpdateAllocationType',
        $argument,
        ['\Arbitraje\AllocationType', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindAllocationTypeByID(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/arbitraje.ArbitrajeService/FindAllocationTypeByID',
        $argument,
        ['\Arbitraje\AllocationType', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Arbitraje\AllocationTypeFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListAllocationTypes(\Arbitraje\AllocationTypeFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/arbitraje.ArbitrajeService/ListAllocationTypes',
        $argument,
        ['\Arbitraje\AllocationTypeArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteAllocationType(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/arbitraje.ArbitrajeService/DeleteAllocationType',
        $argument,
        ['\Arbitraje\AllocationType', 'decode'],
        $metadata, $options);
    }

    /**
     * * Start ShirtType endpoints *
     * @param \Arbitraje\ShirtType $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateShirtType(\Arbitraje\ShirtType $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/arbitraje.ArbitrajeService/CreateShirtType',
        $argument,
        ['\Arbitraje\ShirtType', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Arbitraje\ShirtType $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateShirtType(\Arbitraje\ShirtType $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/arbitraje.ArbitrajeService/UpdateShirtType',
        $argument,
        ['\Arbitraje\ShirtType', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindShirtTypeByID(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/arbitraje.ArbitrajeService/FindShirtTypeByID',
        $argument,
        ['\Arbitraje\ShirtType', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Arbitraje\ShirtTypeFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListShirtTypes(\Arbitraje\ShirtTypeFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/arbitraje.ArbitrajeService/ListShirtTypes',
        $argument,
        ['\Arbitraje\ShirtTypeArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteShirtType(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/arbitraje.ArbitrajeService/DeleteShirtType',
        $argument,
        ['\Arbitraje\ShirtType', 'decode'],
        $metadata, $options);
    }

    /**
     * * Start Nomination endpoints *
     * @param \Arbitraje\Nomination $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateNomination(\Arbitraje\Nomination $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/arbitraje.ArbitrajeService/CreateNomination',
        $argument,
        ['\Arbitraje\Nomination', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Arbitraje\Nomination $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateNomination(\Arbitraje\Nomination $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/arbitraje.ArbitrajeService/UpdateNomination',
        $argument,
        ['\Arbitraje\Nomination', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindNominationByID(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/arbitraje.ArbitrajeService/FindNominationByID',
        $argument,
        ['\Arbitraje\Nomination', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Arbitraje\NominationFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListNominations(\Arbitraje\NominationFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/arbitraje.ArbitrajeService/ListNominations',
        $argument,
        ['\Arbitraje\NominationArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteNomination(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/arbitraje.ArbitrajeService/DeleteNomination',
        $argument,
        ['\Arbitraje\Nomination', 'decode'],
        $metadata, $options);
    }

    /**
     * * Start NominationType endpoints *
     * @param \Arbitraje\NominationType $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateNominationType(\Arbitraje\NominationType $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/arbitraje.ArbitrajeService/CreateNominationType',
        $argument,
        ['\Arbitraje\NominationType', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Arbitraje\NominationType $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateNominationType(\Arbitraje\NominationType $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/arbitraje.ArbitrajeService/UpdateNominationType',
        $argument,
        ['\Arbitraje\NominationType', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindNominationTypeByID(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/arbitraje.ArbitrajeService/FindNominationTypeByID',
        $argument,
        ['\Arbitraje\NominationType', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Arbitraje\NominationTypeFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListNominationTypes(\Arbitraje\NominationTypeFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/arbitraje.ArbitrajeService/ListNominationTypes',
        $argument,
        ['\Arbitraje\NominationTypeArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteNominationType(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/arbitraje.ArbitrajeService/DeleteNominationType',
        $argument,
        ['\Arbitraje\NominationType', 'decode'],
        $metadata, $options);
    }

}
