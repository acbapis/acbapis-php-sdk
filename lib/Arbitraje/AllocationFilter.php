<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: arbitraje/arbapi.proto

namespace Arbitraje;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>arbitraje.AllocationFilter</code>
 */
class AllocationFilter extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>int64 offset = 1;</code>
     */
    private $offset = 0;
    /**
     * Generated from protobuf field <code>int64 limit = 2;</code>
     */
    private $limit = 0;
    /**
     * Generated from protobuf field <code>int64 gameId = 3;</code>
     */
    private $gameId = 0;
    /**
     * Generated from protobuf field <code>int64 elementId = 4;</code>
     */
    private $elementId = 0;
    /**
     * Generated from protobuf field <code>int64 typeId = 5;</code>
     */
    private $typeId = 0;
    /**
     * Active filter. 1 for active, 2 for inactive
     *
     * Generated from protobuf field <code>int32 active = 7;</code>
     */
    private $active = 0;
    /**
     * Generated from protobuf field <code>.arbitraje.AllocationSorting sorting = 8;</code>
     */
    private $sorting = null;
    /**
     * If true, returns all records
     *
     * Generated from protobuf field <code>bool unfiltered = 30;</code>
     */
    private $unfiltered = false;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type int|string $offset
     *     @type int|string $limit
     *     @type int|string $gameId
     *     @type int|string $elementId
     *     @type int|string $typeId
     *     @type int $active
     *           Active filter. 1 for active, 2 for inactive
     *     @type \Arbitraje\AllocationSorting $sorting
     *     @type bool $unfiltered
     *           If true, returns all records
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Arbitraje\Arbapi::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>int64 offset = 1;</code>
     * @return int|string
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     * Generated from protobuf field <code>int64 offset = 1;</code>
     * @param int|string $var
     * @return $this
     */
    public function setOffset($var)
    {
        GPBUtil::checkInt64($var);
        $this->offset = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 limit = 2;</code>
     * @return int|string
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * Generated from protobuf field <code>int64 limit = 2;</code>
     * @param int|string $var
     * @return $this
     */
    public function setLimit($var)
    {
        GPBUtil::checkInt64($var);
        $this->limit = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 gameId = 3;</code>
     * @return int|string
     */
    public function getGameId()
    {
        return $this->gameId;
    }

    /**
     * Generated from protobuf field <code>int64 gameId = 3;</code>
     * @param int|string $var
     * @return $this
     */
    public function setGameId($var)
    {
        GPBUtil::checkInt64($var);
        $this->gameId = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 elementId = 4;</code>
     * @return int|string
     */
    public function getElementId()
    {
        return $this->elementId;
    }

    /**
     * Generated from protobuf field <code>int64 elementId = 4;</code>
     * @param int|string $var
     * @return $this
     */
    public function setElementId($var)
    {
        GPBUtil::checkInt64($var);
        $this->elementId = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 typeId = 5;</code>
     * @return int|string
     */
    public function getTypeId()
    {
        return $this->typeId;
    }

    /**
     * Generated from protobuf field <code>int64 typeId = 5;</code>
     * @param int|string $var
     * @return $this
     */
    public function setTypeId($var)
    {
        GPBUtil::checkInt64($var);
        $this->typeId = $var;

        return $this;
    }

    /**
     * Active filter. 1 for active, 2 for inactive
     *
     * Generated from protobuf field <code>int32 active = 7;</code>
     * @return int
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Active filter. 1 for active, 2 for inactive
     *
     * Generated from protobuf field <code>int32 active = 7;</code>
     * @param int $var
     * @return $this
     */
    public function setActive($var)
    {
        GPBUtil::checkInt32($var);
        $this->active = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>.arbitraje.AllocationSorting sorting = 8;</code>
     * @return \Arbitraje\AllocationSorting
     */
    public function getSorting()
    {
        return $this->sorting;
    }

    /**
     * Generated from protobuf field <code>.arbitraje.AllocationSorting sorting = 8;</code>
     * @param \Arbitraje\AllocationSorting $var
     * @return $this
     */
    public function setSorting($var)
    {
        GPBUtil::checkMessage($var, \Arbitraje\AllocationSorting::class);
        $this->sorting = $var;

        return $this;
    }

    /**
     * If true, returns all records
     *
     * Generated from protobuf field <code>bool unfiltered = 30;</code>
     * @return bool
     */
    public function getUnfiltered()
    {
        return $this->unfiltered;
    }

    /**
     * If true, returns all records
     *
     * Generated from protobuf field <code>bool unfiltered = 30;</code>
     * @param bool $var
     * @return $this
     */
    public function setUnfiltered($var)
    {
        GPBUtil::checkBool($var);
        $this->unfiltered = $var;

        return $this;
    }

}

