<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: ans/ans.proto

namespace Ans;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>ans.Subscription</code>
 */
class Subscription extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>string id = 1;</code>
     */
    private $id = '';
    /**
     * Generated from protobuf field <code>string accountId = 2;</code>
     */
    private $accountId = '';
    /**
     * Generated from protobuf field <code>.ans.Account account = 3;</code>
     */
    private $account = null;
    /**
     * Generated from protobuf field <code>string serviceId = 4;</code>
     */
    private $serviceId = '';
    /**
     * Generated from protobuf field <code>.ans.Service service = 5;</code>
     */
    private $service = null;
    /**
     * Generated from protobuf field <code>string formatId = 6;</code>
     */
    private $formatId = '';
    /**
     * Generated from protobuf field <code>.ans.Format format = 7;</code>
     */
    private $format = null;
    /**
     * Generated from protobuf field <code>string endpoint = 8;</code>
     */
    private $endpoint = '';
    /**
     * Generated from protobuf field <code>.ans.SubscriptionKind kind = 9;</code>
     */
    private $kind = 0;
    /**
     * Generated from protobuf field <code>bool authRequired = 10;</code>
     */
    private $authRequired = false;
    /**
     * Generated from protobuf field <code>.ans.Credentials credentials = 11;</code>
     */
    private $credentials = null;
    /**
     * Generated from protobuf field <code>repeated .ans.Filter preFilter = 12;</code>
     */
    private $preFilter;
    /**
     * Generated from protobuf field <code>string ftpDestination = 13;</code>
     */
    private $ftpDestination = '';
    /**
     * Generated from protobuf field <code>repeated string emailTo = 14;</code>
     */
    private $emailTo;
    /**
     * Generated from protobuf field <code>string emailSubject = 15;</code>
     */
    private $emailSubject = '';
    /**
     * Generated from protobuf field <code>bool active = 16;</code>
     */
    private $active = false;
    /**
     * Generated from protobuf field <code>int64 created = 17;</code>
     */
    private $created = 0;
    /**
     * Generated from protobuf field <code>int64 updated = 18;</code>
     */
    private $updated = 0;
    /**
     * Generated from protobuf field <code>int64 deleted = 19;</code>
     */
    private $deleted = 0;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type string $id
     *     @type string $accountId
     *     @type \Ans\Account $account
     *     @type string $serviceId
     *     @type \Ans\Service $service
     *     @type string $formatId
     *     @type \Ans\Format $format
     *     @type string $endpoint
     *     @type int $kind
     *     @type bool $authRequired
     *     @type \Ans\Credentials $credentials
     *     @type \Ans\Filter[]|\Google\Protobuf\Internal\RepeatedField $preFilter
     *     @type string $ftpDestination
     *     @type string[]|\Google\Protobuf\Internal\RepeatedField $emailTo
     *     @type string $emailSubject
     *     @type bool $active
     *     @type int|string $created
     *     @type int|string $updated
     *     @type int|string $deleted
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Ans\Ans::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>string id = 1;</code>
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Generated from protobuf field <code>string id = 1;</code>
     * @param string $var
     * @return $this
     */
    public function setId($var)
    {
        GPBUtil::checkString($var, True);
        $this->id = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string accountId = 2;</code>
     * @return string
     */
    public function getAccountId()
    {
        return $this->accountId;
    }

    /**
     * Generated from protobuf field <code>string accountId = 2;</code>
     * @param string $var
     * @return $this
     */
    public function setAccountId($var)
    {
        GPBUtil::checkString($var, True);
        $this->accountId = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>.ans.Account account = 3;</code>
     * @return \Ans\Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Generated from protobuf field <code>.ans.Account account = 3;</code>
     * @param \Ans\Account $var
     * @return $this
     */
    public function setAccount($var)
    {
        GPBUtil::checkMessage($var, \Ans\Account::class);
        $this->account = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string serviceId = 4;</code>
     * @return string
     */
    public function getServiceId()
    {
        return $this->serviceId;
    }

    /**
     * Generated from protobuf field <code>string serviceId = 4;</code>
     * @param string $var
     * @return $this
     */
    public function setServiceId($var)
    {
        GPBUtil::checkString($var, True);
        $this->serviceId = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>.ans.Service service = 5;</code>
     * @return \Ans\Service
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * Generated from protobuf field <code>.ans.Service service = 5;</code>
     * @param \Ans\Service $var
     * @return $this
     */
    public function setService($var)
    {
        GPBUtil::checkMessage($var, \Ans\Service::class);
        $this->service = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string formatId = 6;</code>
     * @return string
     */
    public function getFormatId()
    {
        return $this->formatId;
    }

    /**
     * Generated from protobuf field <code>string formatId = 6;</code>
     * @param string $var
     * @return $this
     */
    public function setFormatId($var)
    {
        GPBUtil::checkString($var, True);
        $this->formatId = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>.ans.Format format = 7;</code>
     * @return \Ans\Format
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * Generated from protobuf field <code>.ans.Format format = 7;</code>
     * @param \Ans\Format $var
     * @return $this
     */
    public function setFormat($var)
    {
        GPBUtil::checkMessage($var, \Ans\Format::class);
        $this->format = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string endpoint = 8;</code>
     * @return string
     */
    public function getEndpoint()
    {
        return $this->endpoint;
    }

    /**
     * Generated from protobuf field <code>string endpoint = 8;</code>
     * @param string $var
     * @return $this
     */
    public function setEndpoint($var)
    {
        GPBUtil::checkString($var, True);
        $this->endpoint = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>.ans.SubscriptionKind kind = 9;</code>
     * @return int
     */
    public function getKind()
    {
        return $this->kind;
    }

    /**
     * Generated from protobuf field <code>.ans.SubscriptionKind kind = 9;</code>
     * @param int $var
     * @return $this
     */
    public function setKind($var)
    {
        GPBUtil::checkEnum($var, \Ans\SubscriptionKind::class);
        $this->kind = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>bool authRequired = 10;</code>
     * @return bool
     */
    public function getAuthRequired()
    {
        return $this->authRequired;
    }

    /**
     * Generated from protobuf field <code>bool authRequired = 10;</code>
     * @param bool $var
     * @return $this
     */
    public function setAuthRequired($var)
    {
        GPBUtil::checkBool($var);
        $this->authRequired = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>.ans.Credentials credentials = 11;</code>
     * @return \Ans\Credentials
     */
    public function getCredentials()
    {
        return $this->credentials;
    }

    /**
     * Generated from protobuf field <code>.ans.Credentials credentials = 11;</code>
     * @param \Ans\Credentials $var
     * @return $this
     */
    public function setCredentials($var)
    {
        GPBUtil::checkMessage($var, \Ans\Credentials::class);
        $this->credentials = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>repeated .ans.Filter preFilter = 12;</code>
     * @return \Google\Protobuf\Internal\RepeatedField
     */
    public function getPreFilter()
    {
        return $this->preFilter;
    }

    /**
     * Generated from protobuf field <code>repeated .ans.Filter preFilter = 12;</code>
     * @param \Ans\Filter[]|\Google\Protobuf\Internal\RepeatedField $var
     * @return $this
     */
    public function setPreFilter($var)
    {
        $arr = GPBUtil::checkRepeatedField($var, \Google\Protobuf\Internal\GPBType::MESSAGE, \Ans\Filter::class);
        $this->preFilter = $arr;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string ftpDestination = 13;</code>
     * @return string
     */
    public function getFtpDestination()
    {
        return $this->ftpDestination;
    }

    /**
     * Generated from protobuf field <code>string ftpDestination = 13;</code>
     * @param string $var
     * @return $this
     */
    public function setFtpDestination($var)
    {
        GPBUtil::checkString($var, True);
        $this->ftpDestination = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>repeated string emailTo = 14;</code>
     * @return \Google\Protobuf\Internal\RepeatedField
     */
    public function getEmailTo()
    {
        return $this->emailTo;
    }

    /**
     * Generated from protobuf field <code>repeated string emailTo = 14;</code>
     * @param string[]|\Google\Protobuf\Internal\RepeatedField $var
     * @return $this
     */
    public function setEmailTo($var)
    {
        $arr = GPBUtil::checkRepeatedField($var, \Google\Protobuf\Internal\GPBType::STRING);
        $this->emailTo = $arr;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string emailSubject = 15;</code>
     * @return string
     */
    public function getEmailSubject()
    {
        return $this->emailSubject;
    }

    /**
     * Generated from protobuf field <code>string emailSubject = 15;</code>
     * @param string $var
     * @return $this
     */
    public function setEmailSubject($var)
    {
        GPBUtil::checkString($var, True);
        $this->emailSubject = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>bool active = 16;</code>
     * @return bool
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Generated from protobuf field <code>bool active = 16;</code>
     * @param bool $var
     * @return $this
     */
    public function setActive($var)
    {
        GPBUtil::checkBool($var);
        $this->active = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 created = 17;</code>
     * @return int|string
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Generated from protobuf field <code>int64 created = 17;</code>
     * @param int|string $var
     * @return $this
     */
    public function setCreated($var)
    {
        GPBUtil::checkInt64($var);
        $this->created = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 updated = 18;</code>
     * @return int|string
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Generated from protobuf field <code>int64 updated = 18;</code>
     * @param int|string $var
     * @return $this
     */
    public function setUpdated($var)
    {
        GPBUtil::checkInt64($var);
        $this->updated = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 deleted = 19;</code>
     * @return int|string
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Generated from protobuf field <code>int64 deleted = 19;</code>
     * @param int|string $var
     * @return $this
     */
    public function setDeleted($var)
    {
        GPBUtil::checkInt64($var);
        $this->deleted = $var;

        return $this;
    }

}

