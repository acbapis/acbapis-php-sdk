<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: ans/ans.proto

namespace Ans;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Sorting fields, every field accepts 'DESC' or 'ASC'
 *
 * Generated from protobuf message <code>ans.FormatSorting</code>
 */
class FormatSorting extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>string id = 1;</code>
     */
    private $id = '';
    /**
     * Generated from protobuf field <code>string serviceId = 2;</code>
     */
    private $serviceId = '';
    /**
     * Generated from protobuf field <code>string accountId = 3;</code>
     */
    private $accountId = '';
    /**
     * Generated from protobuf field <code>string formatType = 4;</code>
     */
    private $formatType = '';
    /**
     * Generated from protobuf field <code>string tmpl = 5;</code>
     */
    private $tmpl = '';
    /**
     * Generated from protobuf field <code>string tmplVersion = 6;</code>
     */
    private $tmplVersion = '';
    /**
     * Generated from protobuf field <code>string active = 7;</code>
     */
    private $active = '';
    /**
     * Generated from protobuf field <code>string created = 8;</code>
     */
    private $created = '';
    /**
     * Generated from protobuf field <code>string updated = 9;</code>
     */
    private $updated = '';
    /**
     * Generated from protobuf field <code>string deleted = 10;</code>
     */
    private $deleted = '';

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type string $id
     *     @type string $serviceId
     *     @type string $accountId
     *     @type string $formatType
     *     @type string $tmpl
     *     @type string $tmplVersion
     *     @type string $active
     *     @type string $created
     *     @type string $updated
     *     @type string $deleted
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Ans\Ans::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>string id = 1;</code>
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Generated from protobuf field <code>string id = 1;</code>
     * @param string $var
     * @return $this
     */
    public function setId($var)
    {
        GPBUtil::checkString($var, True);
        $this->id = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string serviceId = 2;</code>
     * @return string
     */
    public function getServiceId()
    {
        return $this->serviceId;
    }

    /**
     * Generated from protobuf field <code>string serviceId = 2;</code>
     * @param string $var
     * @return $this
     */
    public function setServiceId($var)
    {
        GPBUtil::checkString($var, True);
        $this->serviceId = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string accountId = 3;</code>
     * @return string
     */
    public function getAccountId()
    {
        return $this->accountId;
    }

    /**
     * Generated from protobuf field <code>string accountId = 3;</code>
     * @param string $var
     * @return $this
     */
    public function setAccountId($var)
    {
        GPBUtil::checkString($var, True);
        $this->accountId = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string formatType = 4;</code>
     * @return string
     */
    public function getFormatType()
    {
        return $this->formatType;
    }

    /**
     * Generated from protobuf field <code>string formatType = 4;</code>
     * @param string $var
     * @return $this
     */
    public function setFormatType($var)
    {
        GPBUtil::checkString($var, True);
        $this->formatType = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string tmpl = 5;</code>
     * @return string
     */
    public function getTmpl()
    {
        return $this->tmpl;
    }

    /**
     * Generated from protobuf field <code>string tmpl = 5;</code>
     * @param string $var
     * @return $this
     */
    public function setTmpl($var)
    {
        GPBUtil::checkString($var, True);
        $this->tmpl = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string tmplVersion = 6;</code>
     * @return string
     */
    public function getTmplVersion()
    {
        return $this->tmplVersion;
    }

    /**
     * Generated from protobuf field <code>string tmplVersion = 6;</code>
     * @param string $var
     * @return $this
     */
    public function setTmplVersion($var)
    {
        GPBUtil::checkString($var, True);
        $this->tmplVersion = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string active = 7;</code>
     * @return string
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Generated from protobuf field <code>string active = 7;</code>
     * @param string $var
     * @return $this
     */
    public function setActive($var)
    {
        GPBUtil::checkString($var, True);
        $this->active = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string created = 8;</code>
     * @return string
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Generated from protobuf field <code>string created = 8;</code>
     * @param string $var
     * @return $this
     */
    public function setCreated($var)
    {
        GPBUtil::checkString($var, True);
        $this->created = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string updated = 9;</code>
     * @return string
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Generated from protobuf field <code>string updated = 9;</code>
     * @param string $var
     * @return $this
     */
    public function setUpdated($var)
    {
        GPBUtil::checkString($var, True);
        $this->updated = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string deleted = 10;</code>
     * @return string
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Generated from protobuf field <code>string deleted = 10;</code>
     * @param string $var
     * @return $this
     */
    public function setDeleted($var)
    {
        GPBUtil::checkString($var, True);
        $this->deleted = $var;

        return $this;
    }

}

