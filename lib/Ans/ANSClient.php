<?php
// GENERATED CODE -- DO NOT EDIT!

namespace Ans;

/**
 */
class ANSClient extends \Grpc\BaseStub {

    /**
     * @param string $hostname hostname
     * @param array $opts channel options
     * @param \Grpc\Channel $channel (optional) re-use channel object
     */
    public function __construct($hostname, $opts, $channel = null) {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * Start Format endpoints
     * @param \Ans\Format $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateFormat(\Ans\Format $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ans.ANS/CreateFormat',
        $argument,
        ['\Ans\Format', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Ans\Format $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateFormat(\Ans\Format $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ans.ANS/UpdateFormat',
        $argument,
        ['\Ans\Format', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\UuidMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindFormatByID(\Common\UuidMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ans.ANS/FindFormatByID',
        $argument,
        ['\Ans\Format', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Ans\FormatFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListFormats(\Ans\FormatFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ans.ANS/ListFormats',
        $argument,
        ['\Ans\FormatArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\UuidMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteFormat(\Common\UuidMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ans.ANS/DeleteFormat',
        $argument,
        ['\Ans\Format', 'decode'],
        $metadata, $options);
    }

    /**
     * Start Service endpoints
     * @param \Ans\Service $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateService(\Ans\Service $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ans.ANS/CreateService',
        $argument,
        ['\Ans\Service', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Ans\Service $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateService(\Ans\Service $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ans.ANS/UpdateService',
        $argument,
        ['\Ans\Service', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\UuidMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindServiceByID(\Common\UuidMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ans.ANS/FindServiceByID',
        $argument,
        ['\Ans\Service', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Ans\ServiceFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListServices(\Ans\ServiceFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ans.ANS/ListServices',
        $argument,
        ['\Ans\ServiceArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\UuidMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteService(\Common\UuidMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ans.ANS/DeleteService',
        $argument,
        ['\Ans\Service', 'decode'],
        $metadata, $options);
    }

    /**
     * Start Subscription endpoints
     * @param \Ans\Subscription $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateSubscription(\Ans\Subscription $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ans.ANS/CreateSubscription',
        $argument,
        ['\Ans\Subscription', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Ans\Subscription $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateSubscription(\Ans\Subscription $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ans.ANS/UpdateSubscription',
        $argument,
        ['\Ans\Subscription', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\UuidMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindSubscriptionByID(\Common\UuidMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ans.ANS/FindSubscriptionByID',
        $argument,
        ['\Ans\Subscription', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Ans\SubscriptionFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListSubscriptions(\Ans\SubscriptionFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ans.ANS/ListSubscriptions',
        $argument,
        ['\Ans\SubscriptionArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\UuidMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteSubscription(\Common\UuidMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ans.ANS/DeleteSubscription',
        $argument,
        ['\Ans\Subscription', 'decode'],
        $metadata, $options);
    }

}
