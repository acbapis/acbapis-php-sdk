<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: media/media.proto

namespace Media;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>media.Video</code>
 */
class Video extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>int64 id = 1;</code>
     */
    private $id = 0;
    /**
     * Generated from protobuf field <code>string title = 2;</code>
     */
    private $title = '';
    /**
     * Generated from protobuf field <code>string fileName = 3;</code>
     */
    private $fileName = '';
    /**
     * Generated from protobuf field <code>string category = 4;</code>
     */
    private $category = '';
    /**
     * Generated from protobuf field <code>int64 dateAdded = 5;</code>
     */
    private $dateAdded = 0;
    /**
     * Generated from protobuf field <code>string embedCode = 6;</code>
     */
    private $embedCode = '';
    /**
     * Generated from protobuf field <code>string url = 7;</code>
     */
    private $url = '';

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type int|string $id
     *     @type string $title
     *     @type string $fileName
     *     @type string $category
     *     @type int|string $dateAdded
     *     @type string $embedCode
     *     @type string $url
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Media\Media::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>int64 id = 1;</code>
     * @return int|string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Generated from protobuf field <code>int64 id = 1;</code>
     * @param int|string $var
     * @return $this
     */
    public function setId($var)
    {
        GPBUtil::checkInt64($var);
        $this->id = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string title = 2;</code>
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Generated from protobuf field <code>string title = 2;</code>
     * @param string $var
     * @return $this
     */
    public function setTitle($var)
    {
        GPBUtil::checkString($var, True);
        $this->title = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string fileName = 3;</code>
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * Generated from protobuf field <code>string fileName = 3;</code>
     * @param string $var
     * @return $this
     */
    public function setFileName($var)
    {
        GPBUtil::checkString($var, True);
        $this->fileName = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string category = 4;</code>
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Generated from protobuf field <code>string category = 4;</code>
     * @param string $var
     * @return $this
     */
    public function setCategory($var)
    {
        GPBUtil::checkString($var, True);
        $this->category = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 dateAdded = 5;</code>
     * @return int|string
     */
    public function getDateAdded()
    {
        return $this->dateAdded;
    }

    /**
     * Generated from protobuf field <code>int64 dateAdded = 5;</code>
     * @param int|string $var
     * @return $this
     */
    public function setDateAdded($var)
    {
        GPBUtil::checkInt64($var);
        $this->dateAdded = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string embedCode = 6;</code>
     * @return string
     */
    public function getEmbedCode()
    {
        return $this->embedCode;
    }

    /**
     * Generated from protobuf field <code>string embedCode = 6;</code>
     * @param string $var
     * @return $this
     */
    public function setEmbedCode($var)
    {
        GPBUtil::checkString($var, True);
        $this->embedCode = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string url = 7;</code>
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Generated from protobuf field <code>string url = 7;</code>
     * @param string $var
     * @return $this
     */
    public function setUrl($var)
    {
        GPBUtil::checkString($var, True);
        $this->url = $var;

        return $this;
    }

}

