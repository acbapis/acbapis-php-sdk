<?php
// GENERATED CODE -- DO NOT EDIT!

namespace Media;

/**
 */
class MediaServiceClient extends \Grpc\BaseStub {

    /**
     * @param string $hostname hostname
     * @param array $opts channel options
     * @param \Grpc\Channel $channel (optional) re-use channel object
     */
    public function __construct($hostname, $opts, $channel = null) {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * @param \Media\GetImageMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetImage(\Media\GetImageMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/media.MediaService/GetImage',
        $argument,
        ['\Media\Image', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Media\SearchImageMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function SearchImage(\Media\SearchImageMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/media.MediaService/SearchImage',
        $argument,
        ['\Media\ImageArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Media\GetVideoMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetVideo(\Media\GetVideoMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/media.MediaService/GetVideo',
        $argument,
        ['\Media\Video', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Media\StaticImageFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetStaticImage(\Media\StaticImageFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/media.MediaService/GetStaticImage',
        $argument,
        ['\Media\StaticImage', 'decode'],
        $metadata, $options);
    }

}
