<?php
// GENERATED CODE -- DO NOT EDIT!

namespace Settings;

/**
 */
class SettingServiceClient extends \Grpc\BaseStub {

    /**
     * @param string $hostname hostname
     * @param array $opts channel options
     * @param \Grpc\Channel $channel (optional) re-use channel object
     */
    public function __construct($hostname, $opts, $channel = null) {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * @param \Settings\Setting $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateSetting(\Settings\Setting $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/settings.SettingService/CreateSetting',
        $argument,
        ['\Settings\Setting', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Settings\Setting $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateSetting(\Settings\Setting $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/settings.SettingService/UpdateSetting',
        $argument,
        ['\Settings\Setting', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Settings\Setting $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetSetting(\Settings\Setting $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/settings.SettingService/GetSetting',
        $argument,
        ['\Settings\Setting', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Settings\Bucket $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListSettings(\Settings\Bucket $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/settings.SettingService/ListSettings',
        $argument,
        ['\Settings\SettingArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Settings\Setting $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteSetting(\Settings\Setting $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/settings.SettingService/DeleteSetting',
        $argument,
        ['\Settings\ErrorMsg', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Settings\Bucket $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteBucket(\Settings\Bucket $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/settings.SettingService/DeleteBucket',
        $argument,
        ['\Settings\ErrorMsg', 'decode'],
        $metadata, $options);
    }

}
