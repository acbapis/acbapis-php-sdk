<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: microauth/microauth.proto

namespace Microauth;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>microauth.RoleMember</code>
 */
class RoleMember extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>string role = 1;</code>
     */
    private $role = '';
    /**
     * Generated from protobuf field <code>string member = 2;</code>
     */
    private $member = '';
    /**
     * Generated from protobuf field <code>string role_id = 3;</code>
     */
    private $role_id = '';
    /**
     * Generated from protobuf field <code>string member_id = 4;</code>
     */
    private $member_id = '';

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type string $role
     *     @type string $member
     *     @type string $role_id
     *     @type string $member_id
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Microauth\Microauth::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>string role = 1;</code>
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Generated from protobuf field <code>string role = 1;</code>
     * @param string $var
     * @return $this
     */
    public function setRole($var)
    {
        GPBUtil::checkString($var, True);
        $this->role = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string member = 2;</code>
     * @return string
     */
    public function getMember()
    {
        return $this->member;
    }

    /**
     * Generated from protobuf field <code>string member = 2;</code>
     * @param string $var
     * @return $this
     */
    public function setMember($var)
    {
        GPBUtil::checkString($var, True);
        $this->member = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string role_id = 3;</code>
     * @return string
     */
    public function getRoleId()
    {
        return $this->role_id;
    }

    /**
     * Generated from protobuf field <code>string role_id = 3;</code>
     * @param string $var
     * @return $this
     */
    public function setRoleId($var)
    {
        GPBUtil::checkString($var, True);
        $this->role_id = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string member_id = 4;</code>
     * @return string
     */
    public function getMemberId()
    {
        return $this->member_id;
    }

    /**
     * Generated from protobuf field <code>string member_id = 4;</code>
     * @param string $var
     * @return $this
     */
    public function setMemberId($var)
    {
        GPBUtil::checkString($var, True);
        $this->member_id = $var;

        return $this;
    }

}

