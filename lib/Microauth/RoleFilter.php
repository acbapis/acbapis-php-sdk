<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: microauth/microauth.proto

namespace Microauth;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * RoleFilter allows to filter the output of the contents in a RoleArray.
 * realm = string, to get only the roles in realm
 * showDeleted = true, to show the roles deleted (default false)
 * deletedOnly = true, to show only the roles deleted (default false)
 * showAll = truc, to show all records, even deleted ones, mixed (default false)
 *
 * Generated from protobuf message <code>microauth.RoleFilter</code>
 */
class RoleFilter extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>string realm = 1;</code>
     */
    private $realm = '';
    /**
     * Generated from protobuf field <code>bool showDeleted = 2;</code>
     */
    private $showDeleted = false;
    /**
     * Generated from protobuf field <code>bool deletedOnly = 3;</code>
     */
    private $deletedOnly = false;
    /**
     * Generated from protobuf field <code>bool showAll = 4;</code>
     */
    private $showAll = false;
    /**
     * First element to return
     *
     * Generated from protobuf field <code>int32 offset = 5;</code>
     */
    private $offset = 0;
    /**
     * Number of elements to return
     *
     * Generated from protobuf field <code>int32 limit = 6;</code>
     */
    private $limit = 0;
    /**
     * Filter by roles that contains label
     *
     * Generated from protobuf field <code>string label = 7;</code>
     */
    private $label = '';
    /**
     * Filter by roles with label equal to filter
     *
     * Generated from protobuf field <code>string labelEq = 8;</code>
     */
    private $labelEq = '';

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type string $realm
     *     @type bool $showDeleted
     *     @type bool $deletedOnly
     *     @type bool $showAll
     *     @type int $offset
     *           First element to return
     *     @type int $limit
     *           Number of elements to return
     *     @type string $label
     *           Filter by roles that contains label
     *     @type string $labelEq
     *           Filter by roles with label equal to filter
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Microauth\Microauth::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>string realm = 1;</code>
     * @return string
     */
    public function getRealm()
    {
        return $this->realm;
    }

    /**
     * Generated from protobuf field <code>string realm = 1;</code>
     * @param string $var
     * @return $this
     */
    public function setRealm($var)
    {
        GPBUtil::checkString($var, True);
        $this->realm = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>bool showDeleted = 2;</code>
     * @return bool
     */
    public function getShowDeleted()
    {
        return $this->showDeleted;
    }

    /**
     * Generated from protobuf field <code>bool showDeleted = 2;</code>
     * @param bool $var
     * @return $this
     */
    public function setShowDeleted($var)
    {
        GPBUtil::checkBool($var);
        $this->showDeleted = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>bool deletedOnly = 3;</code>
     * @return bool
     */
    public function getDeletedOnly()
    {
        return $this->deletedOnly;
    }

    /**
     * Generated from protobuf field <code>bool deletedOnly = 3;</code>
     * @param bool $var
     * @return $this
     */
    public function setDeletedOnly($var)
    {
        GPBUtil::checkBool($var);
        $this->deletedOnly = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>bool showAll = 4;</code>
     * @return bool
     */
    public function getShowAll()
    {
        return $this->showAll;
    }

    /**
     * Generated from protobuf field <code>bool showAll = 4;</code>
     * @param bool $var
     * @return $this
     */
    public function setShowAll($var)
    {
        GPBUtil::checkBool($var);
        $this->showAll = $var;

        return $this;
    }

    /**
     * First element to return
     *
     * Generated from protobuf field <code>int32 offset = 5;</code>
     * @return int
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     * First element to return
     *
     * Generated from protobuf field <code>int32 offset = 5;</code>
     * @param int $var
     * @return $this
     */
    public function setOffset($var)
    {
        GPBUtil::checkInt32($var);
        $this->offset = $var;

        return $this;
    }

    /**
     * Number of elements to return
     *
     * Generated from protobuf field <code>int32 limit = 6;</code>
     * @return int
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * Number of elements to return
     *
     * Generated from protobuf field <code>int32 limit = 6;</code>
     * @param int $var
     * @return $this
     */
    public function setLimit($var)
    {
        GPBUtil::checkInt32($var);
        $this->limit = $var;

        return $this;
    }

    /**
     * Filter by roles that contains label
     *
     * Generated from protobuf field <code>string label = 7;</code>
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Filter by roles that contains label
     *
     * Generated from protobuf field <code>string label = 7;</code>
     * @param string $var
     * @return $this
     */
    public function setLabel($var)
    {
        GPBUtil::checkString($var, True);
        $this->label = $var;

        return $this;
    }

    /**
     * Filter by roles with label equal to filter
     *
     * Generated from protobuf field <code>string labelEq = 8;</code>
     * @return string
     */
    public function getLabelEq()
    {
        return $this->labelEq;
    }

    /**
     * Filter by roles with label equal to filter
     *
     * Generated from protobuf field <code>string labelEq = 8;</code>
     * @param string $var
     * @return $this
     */
    public function setLabelEq($var)
    {
        GPBUtil::checkString($var, True);
        $this->labelEq = $var;

        return $this;
    }

}

