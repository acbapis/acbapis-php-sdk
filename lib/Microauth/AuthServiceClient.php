<?php
// GENERATED CODE -- DO NOT EDIT!

namespace Microauth;

/**
 *
 * AuthService defines the methods exposed by this service
 */
class AuthServiceClient extends \Grpc\BaseStub {

    /**
     * @param string $hostname hostname
     * @param array $opts channel options
     * @param \Grpc\Channel $channel (optional) re-use channel object
     */
    public function __construct($hostname, $opts, $channel = null) {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * CreateAccount gets an Account with the data of the new Account that will
     * be created and returns the Account with the missing fields added to it.
     *
     * If the Account could not be created, an account with an empty ID should
     * be returned.
     * @param \Microauth\Account $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateAccount(\Microauth\Account $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/microauth.AuthService/CreateAccount',
        $argument,
        ['\Common\UuidMessage', 'decode'],
        $metadata, $options);
    }

    /**
     * Me returns the information of the user that sent the request
     * @param \Common\EmptyMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function Me(\Common\EmptyMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/microauth.AuthService/Me',
        $argument,
        ['\Microauth\Account', 'decode'],
        $metadata, $options);
    }

    /**
     * GetAccount returns the account that matches the provided Id.
     *
     * Id should be a valid UUID. If no account is found, an account with an
     * empty Id should be returned.
     * @param \Common\UuidMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetAccount(\Common\UuidMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/microauth.AuthService/GetAccount',
        $argument,
        ['\Microauth\Account', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Microauth\Email $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetAccountByEmail(\Microauth\Email $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/microauth.AuthService/GetAccountByEmail',
        $argument,
        ['\Microauth\Account', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Microauth\AccountFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListAccounts(\Microauth\AccountFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/microauth.AuthService/ListAccounts',
        $argument,
        ['\Microauth\AccountArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Microauth\SaveAccount $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateAccount(\Microauth\SaveAccount $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/microauth.AuthService/UpdateAccount',
        $argument,
        ['\Microauth\Account', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Microauth\Account $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateAccountPassword(\Microauth\Account $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/microauth.AuthService/UpdateAccountPassword',
        $argument,
        ['\Microauth\Account', 'decode'],
        $metadata, $options);
    }

    /**
     * ActivateAccount activates an account's login
     * @param \Common\UuidMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ActivateAccount(\Common\UuidMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/microauth.AuthService/ActivateAccount',
        $argument,
        ['\Microauth\Account', 'decode'],
        $metadata, $options);
    }

    /**
     * EnableAccount enables an account's login
     * @param \Common\UuidMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function EnableAccount(\Common\UuidMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/microauth.AuthService/EnableAccount',
        $argument,
        ['\Microauth\Account', 'decode'],
        $metadata, $options);
    }

    /**
     * DisableAccount disables an account's login
     * @param \Common\UuidMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DisableAccount(\Common\UuidMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/microauth.AuthService/DisableAccount',
        $argument,
        ['\Microauth\Account', 'decode'],
        $metadata, $options);
    }

    /**
     * DeleteAccount take the Id of the account to be deleted and remove it from
     * the database.
     *
     * It returns the Id that has been deleted. If an error occurs and the account
     * could not be deleted, an empty Id should be returned.
     * @param \Common\UuidMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteAccount(\Common\UuidMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/microauth.AuthService/DeleteAccount',
        $argument,
        ['\Common\UuidMessage', 'decode'],
        $metadata, $options);
    }

    /**
     * * Start Authentication endpoints *
     *
     * Login checks the credentials sent against database and returns a
     * JWT after registering a new JWT credential for the user's consumer
     * with Kong
     * @param \Microauth\Credentials $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function Login(\Microauth\Credentials $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/microauth.AuthService/Login',
        $argument,
        ['\Microauth\JWT', 'decode'],
        $metadata, $options);
    }

    /**
     * Logout soft deletes the JWT in the request and the associated RSA pair
     * from database and removes the consumer credentials from Kong
     * @param \Microauth\JWT $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function Logout(\Microauth\JWT $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/microauth.AuthService/Logout',
        $argument,
        ['\Microauth\AuthStatus', 'decode'],
        $metadata, $options);
    }

    /**
     * Renew creates a new JWT using the credentials of the received JWT
     * @param \Microauth\JWT $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function Renew(\Microauth\JWT $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/microauth.AuthService/Renew',
        $argument,
        ['\Microauth\JWT', 'decode'],
        $metadata, $options);
    }

    /**
     * Auth will check the credentials to verify the validity.
     * It returns an AuthStatus with the result.
     * @param \Microauth\Credentials $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function Auth(\Microauth\Credentials $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/microauth.AuthService/Auth',
        $argument,
        ['\Microauth\AuthStatus', 'decode'],
        $metadata, $options);
    }

    /**
     * * End Authentication endpoints *
     *
     * * Start RSAKey endpoints *
     *
     * CreateKey crea una pareja de claves RSA 2048b
     * @param \Common\EmptyMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateRSAKey(\Common\EmptyMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/microauth.AuthService/CreateRSAKey',
        $argument,
        ['\Microauth\Key', 'decode'],
        $metadata, $options);
    }

    /**
     * CreateAndBindKey recibe un id correspondiente a una cuenta,
     * genera una clave RSA y la vincula a la misma devolviento un
     * tipo Key con el identificador del Account y la pareja de claves.
     * @param \Common\UuidMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateAndBindRSAKey(\Common\UuidMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/microauth.AuthService/CreateAndBindRSAKey',
        $argument,
        ['\Microauth\Key', 'decode'],
        $metadata, $options);
    }

    /**
     * GetAccountKeys recibe las claves generadas para el Account
     * @param \Common\UuidMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetAccountKeys(\Common\UuidMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/microauth.AuthService/GetAccountKeys',
        $argument,
        ['\Microauth\KeyArray', 'decode'],
        $metadata, $options);
    }

    /**
     * * Start RBAC endpoints *
     *
     * Authzoriation methods
     *
     * IsAllowed method check if the role is authorized for the action on the resource.
     *
     * The param authReq is composed by the realm of application, the role requesting permission,
     * the resource accessed and the action required.
     * @param \Microauth\AuthzRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function IsAllowed(\Microauth\AuthzRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/microauth.AuthService/IsAllowed',
        $argument,
        ['\Microauth\AuthStatus', 'decode'],
        $metadata, $options);
    }

    /**
     * Role methods
     *
     * CreateRole adds role to the storage
     * @param \Microauth\Role $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateRole(\Microauth\Role $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/microauth.AuthService/CreateRole',
        $argument,
        ['\Microauth\Role', 'decode'],
        $metadata, $options);
    }

    /**
     * UpdateRole update the information of role
     * @param \Microauth\Role $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateRole(\Microauth\Role $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/microauth.AuthService/UpdateRole',
        $argument,
        ['\Microauth\Role', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Microauth\Urn $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteRole(\Microauth\Urn $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/microauth.AuthService/DeleteRole',
        $argument,
        ['\Microauth\AuthStatus', 'decode'],
        $metadata, $options);
    }

    /**
     * GetRole returns the role identified by roleId
     * @param \Microauth\Urn $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetRole(\Microauth\Urn $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/microauth.AuthService/GetRole',
        $argument,
        ['\Microauth\Role', 'decode'],
        $metadata, $options);
    }

    /**
     * ListRoles return an array with all roles registered in microauth
     * @param \Microauth\RoleFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListRoles(\Microauth\RoleFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/microauth.AuthService/ListRoles',
        $argument,
        ['\Microauth\RoleArray', 'decode'],
        $metadata, $options);
    }

    /**
     * ListRoleMembers returns an array containing all accunts belonging to roleId
     * @param \Microauth\Urn $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListRoleMembers(\Microauth\Urn $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/microauth.AuthService/ListRoleMembers',
        $argument,
        ['\Microauth\AccountArray', 'decode'],
        $metadata, $options);
    }

    /**
     * ListRolesForAccount returns an array containing the roles that account belongs to
     * @param \Microauth\Urn $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListRolesForAccount(\Microauth\Urn $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/microauth.AuthService/ListRolesForAccount',
        $argument,
        ['\Microauth\RoleArray', 'decode'],
        $metadata, $options);
    }

    /**
     * AddRoleMember adds the account to the role
     * @param \Microauth\RoleMember $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function AddRoleMember(\Microauth\RoleMember $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/microauth.AuthService/AddRoleMember',
        $argument,
        ['\Microauth\AuthStatus', 'decode'],
        $metadata, $options);
    }

    /**
     * AddMemberToRoles takes an account URN and add it to every role in roles
     * @param \Microauth\MemberRoles $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function AddMemberToRoles(\Microauth\MemberRoles $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/microauth.AuthService/AddMemberToRoles',
        $argument,
        ['\Microauth\RoleArray', 'decode'],
        $metadata, $options);
    }

    /**
     * RemoveRoleMember removes a member account from the role
     * @param \Microauth\RoleMember $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function RemoveRoleMember(\Microauth\RoleMember $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/microauth.AuthService/RemoveRoleMember',
        $argument,
        ['\Microauth\AuthStatus', 'decode'],
        $metadata, $options);
    }

    /**
     * RemoveMemberFromRoles removes a member account from the list of roles
     * @param \Microauth\MemberRoles $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function RemoveMemberFromRoles(\Microauth\MemberRoles $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/microauth.AuthService/RemoveMemberFromRoles',
        $argument,
        ['\Microauth\AuthStatus', 'decode'],
        $metadata, $options);
    }

    /**
     * Action methods
     *
     * @param \Microauth\Action $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateAction(\Microauth\Action $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/microauth.AuthService/CreateAction',
        $argument,
        ['\Microauth\Action', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Microauth\Action $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateAction(\Microauth\Action $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/microauth.AuthService/UpdateAction',
        $argument,
        ['\Microauth\Action', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Microauth\Urn $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteAction(\Microauth\Urn $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/microauth.AuthService/DeleteAction',
        $argument,
        ['\Microauth\AuthStatus', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Microauth\Urn $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetAction(\Microauth\Urn $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/microauth.AuthService/GetAction',
        $argument,
        ['\Microauth\Action', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Microauth\ActionFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListActions(\Microauth\ActionFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/microauth.AuthService/ListActions',
        $argument,
        ['\Microauth\ActionArray', 'decode'],
        $metadata, $options);
    }

    /**
     * Resource methods
     *
     * @param \Microauth\Resource $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateResource(\Microauth\Resource $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/microauth.AuthService/CreateResource',
        $argument,
        ['\Microauth\Resource', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Microauth\Resource $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateResource(\Microauth\Resource $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/microauth.AuthService/UpdateResource',
        $argument,
        ['\Microauth\Resource', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Microauth\Urn $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteResource(\Microauth\Urn $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/microauth.AuthService/DeleteResource',
        $argument,
        ['\Microauth\AuthStatus', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Microauth\Urn $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetResource(\Microauth\Urn $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/microauth.AuthService/GetResource',
        $argument,
        ['\Microauth\Resource', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Microauth\ResourceFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListResources(\Microauth\ResourceFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/microauth.AuthService/ListResources',
        $argument,
        ['\Microauth\ResourceArray', 'decode'],
        $metadata, $options);
    }

    /**
     * Effect methods
     *
     * @param \Microauth\Effect $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateEffect(\Microauth\Effect $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/microauth.AuthService/CreateEffect',
        $argument,
        ['\Microauth\Effect', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Microauth\Effect $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateEffect(\Microauth\Effect $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/microauth.AuthService/UpdateEffect',
        $argument,
        ['\Microauth\Effect', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Microauth\Urn $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteEffect(\Microauth\Urn $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/microauth.AuthService/DeleteEffect',
        $argument,
        ['\Microauth\AuthStatus', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Microauth\Urn $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetEffect(\Microauth\Urn $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/microauth.AuthService/GetEffect',
        $argument,
        ['\Microauth\Effect', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Microauth\EffectFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListEffects(\Microauth\EffectFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/microauth.AuthService/ListEffects',
        $argument,
        ['\Microauth\EffectArray', 'decode'],
        $metadata, $options);
    }

    /**
     * Policy methods
     *
     * @param \Microauth\Policy $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreatePolicy(\Microauth\Policy $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/microauth.AuthService/CreatePolicy',
        $argument,
        ['\Microauth\Policy', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Microauth\Policy $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdatePolicy(\Microauth\Policy $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/microauth.AuthService/UpdatePolicy',
        $argument,
        ['\Microauth\Policy', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Microauth\Urn $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeletePolicy(\Microauth\Urn $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/microauth.AuthService/DeletePolicy',
        $argument,
        ['\Microauth\AuthStatus', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Microauth\Urn $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetPolicy(\Microauth\Urn $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/microauth.AuthService/GetPolicy',
        $argument,
        ['\Microauth\Policy', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Microauth\PolicyFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListPolicies(\Microauth\PolicyFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/microauth.AuthService/ListPolicies',
        $argument,
        ['\Microauth\PolicyArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Microauth\Urn $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListRolesFromPolicy(\Microauth\Urn $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/microauth.AuthService/ListRolesFromPolicy',
        $argument,
        ['\Microauth\RoleArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Microauth\Urn $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListPoliciesFromRole(\Microauth\Urn $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/microauth.AuthService/ListPoliciesFromRole',
        $argument,
        ['\Microauth\PolicyArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Microauth\RolePolicy $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function BindRolePolicy(\Microauth\RolePolicy $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/microauth.AuthService/BindRolePolicy',
        $argument,
        ['\Microauth\AuthStatus', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Microauth\RolePolicy $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UnbindRolePolicy(\Microauth\RolePolicy $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/microauth.AuthService/UnbindRolePolicy',
        $argument,
        ['\Microauth\AuthStatus', 'decode'],
        $metadata, $options);
    }

}
