<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: microauth/microauth.proto

namespace Microauth;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * PolicyArray contains an array, policies, with a collection of Policies.
 * total will show the number of records retrieved.
 *
 * Generated from protobuf message <code>microauth.PolicyArray</code>
 */
class PolicyArray extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>repeated .microauth.Policy policies = 1;</code>
     */
    private $policies;
    /**
     * Generated from protobuf field <code>int32 total = 2;</code>
     */
    private $total = 0;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type \Microauth\Policy[]|\Google\Protobuf\Internal\RepeatedField $policies
     *     @type int $total
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Microauth\Microauth::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>repeated .microauth.Policy policies = 1;</code>
     * @return \Google\Protobuf\Internal\RepeatedField
     */
    public function getPolicies()
    {
        return $this->policies;
    }

    /**
     * Generated from protobuf field <code>repeated .microauth.Policy policies = 1;</code>
     * @param \Microauth\Policy[]|\Google\Protobuf\Internal\RepeatedField $var
     * @return $this
     */
    public function setPolicies($var)
    {
        $arr = GPBUtil::checkRepeatedField($var, \Google\Protobuf\Internal\GPBType::MESSAGE, \Microauth\Policy::class);
        $this->policies = $arr;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int32 total = 2;</code>
     * @return int
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Generated from protobuf field <code>int32 total = 2;</code>
     * @param int $var
     * @return $this
     */
    public function setTotal($var)
    {
        GPBUtil::checkInt32($var);
        $this->total = $var;

        return $this;
    }

}

