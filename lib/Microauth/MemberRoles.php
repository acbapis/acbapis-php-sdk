<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: microauth/microauth.proto

namespace Microauth;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>microauth.MemberRoles</code>
 */
class MemberRoles extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>repeated string roles = 1;</code>
     */
    private $roles;
    /**
     * Generated from protobuf field <code>string member = 2;</code>
     */
    private $member = '';
    /**
     * Generated from protobuf field <code>string member_id = 3;</code>
     */
    private $member_id = '';

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type string[]|\Google\Protobuf\Internal\RepeatedField $roles
     *     @type string $member
     *     @type string $member_id
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Microauth\Microauth::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>repeated string roles = 1;</code>
     * @return \Google\Protobuf\Internal\RepeatedField
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * Generated from protobuf field <code>repeated string roles = 1;</code>
     * @param string[]|\Google\Protobuf\Internal\RepeatedField $var
     * @return $this
     */
    public function setRoles($var)
    {
        $arr = GPBUtil::checkRepeatedField($var, \Google\Protobuf\Internal\GPBType::STRING);
        $this->roles = $arr;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string member = 2;</code>
     * @return string
     */
    public function getMember()
    {
        return $this->member;
    }

    /**
     * Generated from protobuf field <code>string member = 2;</code>
     * @param string $var
     * @return $this
     */
    public function setMember($var)
    {
        GPBUtil::checkString($var, True);
        $this->member = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string member_id = 3;</code>
     * @return string
     */
    public function getMemberId()
    {
        return $this->member_id;
    }

    /**
     * Generated from protobuf field <code>string member_id = 3;</code>
     * @param string $var
     * @return $this
     */
    public function setMemberId($var)
    {
        GPBUtil::checkString($var, True);
        $this->member_id = $var;

        return $this;
    }

}

