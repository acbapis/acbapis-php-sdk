<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: microauth/microauth.proto

namespace Microauth;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * PolicyFilter allows to filter the output of the contents in a PolicyArray.
 * realm = string, to get only the policies in realm
 * showDeleted = true, to show the policies deleted (default false)
 * deletedOnly = true, to show only the policies deleted (default false)
 * showAll = truc, to show all policies, even deleted ones, mixed (default false)
 *
 * Generated from protobuf message <code>microauth.PolicyFilter</code>
 */
class PolicyFilter extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>string realm = 1;</code>
     */
    private $realm = '';
    /**
     * Generated from protobuf field <code>bool showDeleted = 2;</code>
     */
    private $showDeleted = false;
    /**
     * Generated from protobuf field <code>bool deletedOnly = 3;</code>
     */
    private $deletedOnly = false;
    /**
     * Generated from protobuf field <code>bool showAll = 4;</code>
     */
    private $showAll = false;
    /**
     * Generated from protobuf field <code>string accountId = 5;</code>
     */
    private $accountId = '';
    /**
     * Generated from protobuf field <code>string actionId = 6;</code>
     */
    private $actionId = '';
    /**
     * Generated from protobuf field <code>string resourceId = 7;</code>
     */
    private $resourceId = '';

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type string $realm
     *     @type bool $showDeleted
     *     @type bool $deletedOnly
     *     @type bool $showAll
     *     @type string $accountId
     *     @type string $actionId
     *     @type string $resourceId
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Microauth\Microauth::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>string realm = 1;</code>
     * @return string
     */
    public function getRealm()
    {
        return $this->realm;
    }

    /**
     * Generated from protobuf field <code>string realm = 1;</code>
     * @param string $var
     * @return $this
     */
    public function setRealm($var)
    {
        GPBUtil::checkString($var, True);
        $this->realm = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>bool showDeleted = 2;</code>
     * @return bool
     */
    public function getShowDeleted()
    {
        return $this->showDeleted;
    }

    /**
     * Generated from protobuf field <code>bool showDeleted = 2;</code>
     * @param bool $var
     * @return $this
     */
    public function setShowDeleted($var)
    {
        GPBUtil::checkBool($var);
        $this->showDeleted = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>bool deletedOnly = 3;</code>
     * @return bool
     */
    public function getDeletedOnly()
    {
        return $this->deletedOnly;
    }

    /**
     * Generated from protobuf field <code>bool deletedOnly = 3;</code>
     * @param bool $var
     * @return $this
     */
    public function setDeletedOnly($var)
    {
        GPBUtil::checkBool($var);
        $this->deletedOnly = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>bool showAll = 4;</code>
     * @return bool
     */
    public function getShowAll()
    {
        return $this->showAll;
    }

    /**
     * Generated from protobuf field <code>bool showAll = 4;</code>
     * @param bool $var
     * @return $this
     */
    public function setShowAll($var)
    {
        GPBUtil::checkBool($var);
        $this->showAll = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string accountId = 5;</code>
     * @return string
     */
    public function getAccountId()
    {
        return $this->accountId;
    }

    /**
     * Generated from protobuf field <code>string accountId = 5;</code>
     * @param string $var
     * @return $this
     */
    public function setAccountId($var)
    {
        GPBUtil::checkString($var, True);
        $this->accountId = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string actionId = 6;</code>
     * @return string
     */
    public function getActionId()
    {
        return $this->actionId;
    }

    /**
     * Generated from protobuf field <code>string actionId = 6;</code>
     * @param string $var
     * @return $this
     */
    public function setActionId($var)
    {
        GPBUtil::checkString($var, True);
        $this->actionId = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string resourceId = 7;</code>
     * @return string
     */
    public function getResourceId()
    {
        return $this->resourceId;
    }

    /**
     * Generated from protobuf field <code>string resourceId = 7;</code>
     * @param string $var
     * @return $this
     */
    public function setResourceId($var)
    {
        GPBUtil::checkString($var, True);
        $this->resourceId = $var;

        return $this;
    }

}

