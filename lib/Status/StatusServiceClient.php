<?php
// GENERATED CODE -- DO NOT EDIT!

namespace Status;

/**
 */
class StatusServiceClient extends \Grpc\BaseStub {

    /**
     * @param string $hostname hostname
     * @param array $opts channel options
     * @param \Grpc\Channel $channel (optional) re-use channel object
     */
    public function __construct($hostname, $opts, $channel = null) {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * @param \Common\EmptyMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetServerTime(\Common\EmptyMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/status.StatusService/GetServerTime',
        $argument,
        ['\Status\ServerTimeMessage', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\EmptyMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetVersion(\Common\EmptyMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/status.StatusService/GetVersion',
        $argument,
        ['\Common\Version', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\EmptyMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetGlobalServiceStatus(\Common\EmptyMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/status.StatusService/GetGlobalServiceStatus',
        $argument,
        ['\Status\ServerStatusMessage', 'decode'],
        $metadata, $options);
    }

}
