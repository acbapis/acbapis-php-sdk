<?php
// GENERATED CODE -- DO NOT EDIT!

namespace Services;

/**
 * ACBLiveService offers methods to query data for live matches.
 * If client request a stream, a gRPC stream is opened and kept open
 * until the match ends.
 *
 * If client request data for a match, the current data is returned.
 */
class ACBLiveServiceClient extends \Grpc\BaseStub {

    /**
     * @param string $hostname hostname
     * @param array $opts channel options
     * @param \Grpc\Channel $channel (optional) re-use channel object
     */
    public function __construct($hostname, $opts, $channel = null) {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * @param \Services\MatchFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListMatches(\Services\MatchFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/services.ACBLiveService/ListMatches',
        $argument,
        ['\Openapi\MatchArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetMatch(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/services.ACBLiveService/GetMatch',
        $argument,
        ['\Openapi\MatchDetail', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function StreamMatch(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_serverStreamRequest('/services.ACBLiveService/StreamMatch',
        $argument,
        ['\Openapi\MatchDetail', 'decode'],
        $metadata, $options);
    }

}
