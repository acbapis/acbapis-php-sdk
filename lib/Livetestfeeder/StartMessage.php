<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: livetestfeeder/livetestfeeder.proto

namespace Livetestfeeder;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>livetestfeeder.StartMessage</code>
 */
class StartMessage extends \Google\Protobuf\Internal\Message
{
    /**
     * Match Id
     *
     * Generated from protobuf field <code>int64 id = 1;</code>
     */
    private $id = 0;
    /**
     * In milliseconds, delay between actions. If 0 the match will be replayed at normal speed
     *
     * Generated from protobuf field <code>int32 delay = 2;</code>
     */
    private $delay = 0;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type int|string $id
     *           Match Id
     *     @type int $delay
     *           In milliseconds, delay between actions. If 0 the match will be replayed at normal speed
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Livetestfeeder\Livetestfeeder::initOnce();
        parent::__construct($data);
    }

    /**
     * Match Id
     *
     * Generated from protobuf field <code>int64 id = 1;</code>
     * @return int|string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Match Id
     *
     * Generated from protobuf field <code>int64 id = 1;</code>
     * @param int|string $var
     * @return $this
     */
    public function setId($var)
    {
        GPBUtil::checkInt64($var);
        $this->id = $var;

        return $this;
    }

    /**
     * In milliseconds, delay between actions. If 0 the match will be replayed at normal speed
     *
     * Generated from protobuf field <code>int32 delay = 2;</code>
     * @return int
     */
    public function getDelay()
    {
        return $this->delay;
    }

    /**
     * In milliseconds, delay between actions. If 0 the match will be replayed at normal speed
     *
     * Generated from protobuf field <code>int32 delay = 2;</code>
     * @param int $var
     * @return $this
     */
    public function setDelay($var)
    {
        GPBUtil::checkInt32($var);
        $this->delay = $var;

        return $this;
    }

}

