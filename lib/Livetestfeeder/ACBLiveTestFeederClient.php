<?php
// GENERATED CODE -- DO NOT EDIT!

namespace Livetestfeeder;

/**
 *
 * ACBLiveTestFeeder gestiona el replay de un partido ya jugado emulando un directo.
 *
 * El replay se realiza de manera que:
 *
 * 1. se lee una acción y la siguiente
 * 2. se publica la primera acción
 * 3. se crea un timer que esperará la diferencia entre la siguiente jugada y la actual
 * 4. la publicación se realiza en formato AD (actions)
 * 5. Una se haya llegado al final del partido, se creará un timer durante 30min aprox para
 * simular el proceso final de cierre.
 * 6. Una vez finalizado el timer, se cierra el partido y se liberan los recursos.
 *
 * El servicio recogerá el pbp y boxscore de competdb (formato GS), transformará los mensajes a
 * formato AD e iniciará el replay del partido publicando las acciones en la cola AMQP.
 */
class ACBLiveTestFeederClient extends \Grpc\BaseStub {

    /**
     * @param string $hostname hostname
     * @param array $opts channel options
     * @param \Grpc\Channel $channel (optional) re-use channel object
     */
    public function __construct($hostname, $opts, $channel = null) {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * StartMatchReplay inicia el replay del partido identificado por su id y devuelve
     * el id asociado al replay creado.
     * @param \Livetestfeeder\StartMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function StartMatchReplay(\Livetestfeeder\StartMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/livetestfeeder.ACBLiveTestFeeder/StartMatchReplay',
        $argument,
        ['\Common\UuidMessage', 'decode'],
        $metadata, $options);
    }

    /**
     * ImportMatchReplay inicia el replay del partido a partir de un fichero XML generado
     * por FLS v6 y devuelve el id asociado al replay creado
     * @param \Livetestfeeder\ImportMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ImportMatchReplay(\Livetestfeeder\ImportMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/livetestfeeder.ACBLiveTestFeeder/ImportMatchReplay',
        $argument,
        ['\Common\UuidMessage', 'decode'],
        $metadata, $options);
    }

    /**
     * ImportGeniusMatchReplay inicia el replay del partido a partir ficheros JSON generados
     * por el livestream API de Genius Sports y devuelve el id asociado al replay creado
     * @param \Livetestfeeder\GeniusMatch $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ImportGeniusMatchReplay(\Livetestfeeder\GeniusMatch $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/livetestfeeder.ACBLiveTestFeeder/ImportGeniusMatchReplay',
        $argument,
        ['\Common\UuidMessage', 'decode'],
        $metadata, $options);
    }

    /**
     * StopMatchReplay detiene el replay identificado por el id del Replay y actualiza
     * la información en la base de datos. No elimina el replay, solamente lo detiene.
     * @param \Common\UuidMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function StopMatchReplay(\Common\UuidMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/livetestfeeder.ACBLiveTestFeeder/StopMatchReplay',
        $argument,
        ['\Common\UuidMessage', 'decode'],
        $metadata, $options);
    }

    /**
     * ResumeMatchReplay continua el replay identificado por su id. Si el Replay no se
     * encuentra detenido, la llamada a esta método no tiene efecto.
     * @param \Common\UuidMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ResumeMatchReplay(\Common\UuidMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/livetestfeeder.ACBLiveTestFeeder/ResumeMatchReplay',
        $argument,
        ['\Common\UuidMessage', 'decode'],
        $metadata, $options);
    }

    /**
     * RestartMatchReplay reinicia el replay y vuelve a enviar los datos desde el comienzo
     * como si no se hubiera enviado nada.
     * @param \Common\UuidMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function RestartMatchReplay(\Common\UuidMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/livetestfeeder.ACBLiveTestFeeder/RestartMatchReplay',
        $argument,
        ['\Common\UuidMessage', 'decode'],
        $metadata, $options);
    }

    /**
     * DeleteMatchReplay deletes the replay identified by its id. If the replay is active (running)
     * it will be stopped.
     * @param \Common\UuidMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteMatchReplay(\Common\UuidMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/livetestfeeder.ACBLiveTestFeeder/DeleteMatchReplay',
        $argument,
        ['\Common\UuidMessage', 'decode'],
        $metadata, $options);
    }

    /**
     * TransformMessageGS2AD will receive a message in the format used by GeniusSports Stream API and
     * will return it transformed to the message used by the service AD.
     * @param \Livetestfeeder\GSMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function TransformMessageGS2AD(\Livetestfeeder\GSMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/livetestfeeder.ACBLiveTestFeeder/TransformMessageGS2AD',
        $argument,
        ['\Livetestfeeder\ADMessage', 'decode'],
        $metadata, $options);
    }

}
