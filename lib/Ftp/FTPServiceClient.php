<?php
// GENERATED CODE -- DO NOT EDIT!

namespace Ftp;

/**
 */
class FTPServiceClient extends \Grpc\BaseStub {

    /**
     * @param string $hostname hostname
     * @param array $opts channel options
     * @param \Grpc\Channel $channel (optional) re-use channel object
     */
    public function __construct($hostname, $opts, $channel = null) {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * @param \Ftp\FTPMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function Upload(\Ftp\FTPMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/ftp.FTPService/Upload',
        $argument,
        ['\Ftp\FTPResponse', 'decode'],
        $metadata, $options);
    }

}
