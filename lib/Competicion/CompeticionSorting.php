<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: competicion/compapi.proto

namespace Competicion;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Sorting fields, every fields accepts 'DESC' or 'ASC'
 *
 * Generated from protobuf message <code>competicion.CompeticionSorting</code>
 */
class CompeticionSorting extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>string id = 1;</code>
     */
    private $id = '';
    /**
     * Generated from protobuf field <code>string inicio = 2;</code>
     */
    private $inicio = '';
    /**
     * Generated from protobuf field <code>string fin = 3;</code>
     */
    private $fin = '';
    /**
     * Generated from protobuf field <code>string descriptor = 4;</code>
     */
    private $descriptor = '';
    /**
     * Generated from protobuf field <code>string activo = 5;</code>
     */
    private $activo = '';
    /**
     * Generated from protobuf field <code>string publicable = 6;</code>
     */
    private $publicable = '';

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type string $id
     *     @type string $inicio
     *     @type string $fin
     *     @type string $descriptor
     *     @type string $activo
     *     @type string $publicable
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Competicion\Compapi::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>string id = 1;</code>
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Generated from protobuf field <code>string id = 1;</code>
     * @param string $var
     * @return $this
     */
    public function setId($var)
    {
        GPBUtil::checkString($var, True);
        $this->id = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string inicio = 2;</code>
     * @return string
     */
    public function getInicio()
    {
        return $this->inicio;
    }

    /**
     * Generated from protobuf field <code>string inicio = 2;</code>
     * @param string $var
     * @return $this
     */
    public function setInicio($var)
    {
        GPBUtil::checkString($var, True);
        $this->inicio = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string fin = 3;</code>
     * @return string
     */
    public function getFin()
    {
        return $this->fin;
    }

    /**
     * Generated from protobuf field <code>string fin = 3;</code>
     * @param string $var
     * @return $this
     */
    public function setFin($var)
    {
        GPBUtil::checkString($var, True);
        $this->fin = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string descriptor = 4;</code>
     * @return string
     */
    public function getDescriptor()
    {
        return $this->descriptor;
    }

    /**
     * Generated from protobuf field <code>string descriptor = 4;</code>
     * @param string $var
     * @return $this
     */
    public function setDescriptor($var)
    {
        GPBUtil::checkString($var, True);
        $this->descriptor = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string activo = 5;</code>
     * @return string
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Generated from protobuf field <code>string activo = 5;</code>
     * @param string $var
     * @return $this
     */
    public function setActivo($var)
    {
        GPBUtil::checkString($var, True);
        $this->activo = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string publicable = 6;</code>
     * @return string
     */
    public function getPublicable()
    {
        return $this->publicable;
    }

    /**
     * Generated from protobuf field <code>string publicable = 6;</code>
     * @param string $var
     * @return $this
     */
    public function setPublicable($var)
    {
        GPBUtil::checkString($var, True);
        $this->publicable = $var;

        return $this;
    }

}

