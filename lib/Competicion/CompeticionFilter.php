<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: competicion/compapi.proto

namespace Competicion;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>competicion.CompeticionFilter</code>
 */
class CompeticionFilter extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>int64 offset = 1;</code>
     */
    private $offset = 0;
    /**
     * Generated from protobuf field <code>int64 limit = 2;</code>
     */
    private $limit = 0;
    /**
     * Generated from protobuf field <code>int64 id = 3;</code>
     */
    private $id = 0;
    /**
     * Generated from protobuf field <code>int64 inicioDesde = 4;</code>
     */
    private $inicioDesde = 0;
    /**
     * Generated from protobuf field <code>int64 inicioHasta = 5;</code>
     */
    private $inicioHasta = 0;
    /**
     * Generated from protobuf field <code>int64 finDesde = 6;</code>
     */
    private $finDesde = 0;
    /**
     * Generated from protobuf field <code>int64 finHasta = 7;</code>
     */
    private $finHasta = 0;
    /**
     * Time filter. Return competiciones where inicio < time < fin
     *
     * Generated from protobuf field <code>int64 time = 8;</code>
     */
    private $time = 0;
    /**
     * Filter by descriptor
     *
     * Generated from protobuf field <code>string terms = 9;</code>
     */
    private $terms = '';
    /**
     * Activo filter, 1 for true, 2 for false
     *
     * Generated from protobuf field <code>int32 activo = 10;</code>
     */
    private $activo = 0;
    /**
     * Wanted fields separated by comma
     *
     * Generated from protobuf field <code>string fields = 11;</code>
     */
    private $fields = '';
    /**
     * Generated from protobuf field <code>.competicion.CompeticionSorting sorting = 12;</code>
     */
    private $sorting = null;
    /**
     * Generated from protobuf field <code>int32 publicable = 13;</code>
     */
    private $publicable = 0;
    /**
     * If true, returns all records
     *
     * Generated from protobuf field <code>bool unfiltered = 30;</code>
     */
    private $unfiltered = false;
    /**
     * Generated from protobuf field <code>bool showDeleted = 31;</code>
     */
    private $showDeleted = false;
    /**
     * Generated from protobuf field <code>bool deletedOnly = 32;</code>
     */
    private $deletedOnly = false;
    /**
     * Generated from protobuf field <code>repeated int64 idNotIn = 33;</code>
     */
    private $idNotIn;
    /**
     * Generated from protobuf field <code>repeated int64 ids = 34;</code>
     */
    private $ids;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type int|string $offset
     *     @type int|string $limit
     *     @type int|string $id
     *     @type int|string $inicioDesde
     *     @type int|string $inicioHasta
     *     @type int|string $finDesde
     *     @type int|string $finHasta
     *     @type int|string $time
     *           Time filter. Return competiciones where inicio < time < fin
     *     @type string $terms
     *           Filter by descriptor
     *     @type int $activo
     *           Activo filter, 1 for true, 2 for false
     *     @type string $fields
     *           Wanted fields separated by comma
     *     @type \Competicion\CompeticionSorting $sorting
     *     @type int $publicable
     *     @type bool $unfiltered
     *           If true, returns all records
     *     @type bool $showDeleted
     *     @type bool $deletedOnly
     *     @type int[]|string[]|\Google\Protobuf\Internal\RepeatedField $idNotIn
     *     @type int[]|string[]|\Google\Protobuf\Internal\RepeatedField $ids
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Competicion\Compapi::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>int64 offset = 1;</code>
     * @return int|string
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     * Generated from protobuf field <code>int64 offset = 1;</code>
     * @param int|string $var
     * @return $this
     */
    public function setOffset($var)
    {
        GPBUtil::checkInt64($var);
        $this->offset = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 limit = 2;</code>
     * @return int|string
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * Generated from protobuf field <code>int64 limit = 2;</code>
     * @param int|string $var
     * @return $this
     */
    public function setLimit($var)
    {
        GPBUtil::checkInt64($var);
        $this->limit = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 id = 3;</code>
     * @return int|string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Generated from protobuf field <code>int64 id = 3;</code>
     * @param int|string $var
     * @return $this
     */
    public function setId($var)
    {
        GPBUtil::checkInt64($var);
        $this->id = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 inicioDesde = 4;</code>
     * @return int|string
     */
    public function getInicioDesde()
    {
        return $this->inicioDesde;
    }

    /**
     * Generated from protobuf field <code>int64 inicioDesde = 4;</code>
     * @param int|string $var
     * @return $this
     */
    public function setInicioDesde($var)
    {
        GPBUtil::checkInt64($var);
        $this->inicioDesde = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 inicioHasta = 5;</code>
     * @return int|string
     */
    public function getInicioHasta()
    {
        return $this->inicioHasta;
    }

    /**
     * Generated from protobuf field <code>int64 inicioHasta = 5;</code>
     * @param int|string $var
     * @return $this
     */
    public function setInicioHasta($var)
    {
        GPBUtil::checkInt64($var);
        $this->inicioHasta = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 finDesde = 6;</code>
     * @return int|string
     */
    public function getFinDesde()
    {
        return $this->finDesde;
    }

    /**
     * Generated from protobuf field <code>int64 finDesde = 6;</code>
     * @param int|string $var
     * @return $this
     */
    public function setFinDesde($var)
    {
        GPBUtil::checkInt64($var);
        $this->finDesde = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 finHasta = 7;</code>
     * @return int|string
     */
    public function getFinHasta()
    {
        return $this->finHasta;
    }

    /**
     * Generated from protobuf field <code>int64 finHasta = 7;</code>
     * @param int|string $var
     * @return $this
     */
    public function setFinHasta($var)
    {
        GPBUtil::checkInt64($var);
        $this->finHasta = $var;

        return $this;
    }

    /**
     * Time filter. Return competiciones where inicio < time < fin
     *
     * Generated from protobuf field <code>int64 time = 8;</code>
     * @return int|string
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Time filter. Return competiciones where inicio < time < fin
     *
     * Generated from protobuf field <code>int64 time = 8;</code>
     * @param int|string $var
     * @return $this
     */
    public function setTime($var)
    {
        GPBUtil::checkInt64($var);
        $this->time = $var;

        return $this;
    }

    /**
     * Filter by descriptor
     *
     * Generated from protobuf field <code>string terms = 9;</code>
     * @return string
     */
    public function getTerms()
    {
        return $this->terms;
    }

    /**
     * Filter by descriptor
     *
     * Generated from protobuf field <code>string terms = 9;</code>
     * @param string $var
     * @return $this
     */
    public function setTerms($var)
    {
        GPBUtil::checkString($var, True);
        $this->terms = $var;

        return $this;
    }

    /**
     * Activo filter, 1 for true, 2 for false
     *
     * Generated from protobuf field <code>int32 activo = 10;</code>
     * @return int
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Activo filter, 1 for true, 2 for false
     *
     * Generated from protobuf field <code>int32 activo = 10;</code>
     * @param int $var
     * @return $this
     */
    public function setActivo($var)
    {
        GPBUtil::checkInt32($var);
        $this->activo = $var;

        return $this;
    }

    /**
     * Wanted fields separated by comma
     *
     * Generated from protobuf field <code>string fields = 11;</code>
     * @return string
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * Wanted fields separated by comma
     *
     * Generated from protobuf field <code>string fields = 11;</code>
     * @param string $var
     * @return $this
     */
    public function setFields($var)
    {
        GPBUtil::checkString($var, True);
        $this->fields = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>.competicion.CompeticionSorting sorting = 12;</code>
     * @return \Competicion\CompeticionSorting
     */
    public function getSorting()
    {
        return $this->sorting;
    }

    /**
     * Generated from protobuf field <code>.competicion.CompeticionSorting sorting = 12;</code>
     * @param \Competicion\CompeticionSorting $var
     * @return $this
     */
    public function setSorting($var)
    {
        GPBUtil::checkMessage($var, \Competicion\CompeticionSorting::class);
        $this->sorting = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int32 publicable = 13;</code>
     * @return int
     */
    public function getPublicable()
    {
        return $this->publicable;
    }

    /**
     * Generated from protobuf field <code>int32 publicable = 13;</code>
     * @param int $var
     * @return $this
     */
    public function setPublicable($var)
    {
        GPBUtil::checkInt32($var);
        $this->publicable = $var;

        return $this;
    }

    /**
     * If true, returns all records
     *
     * Generated from protobuf field <code>bool unfiltered = 30;</code>
     * @return bool
     */
    public function getUnfiltered()
    {
        return $this->unfiltered;
    }

    /**
     * If true, returns all records
     *
     * Generated from protobuf field <code>bool unfiltered = 30;</code>
     * @param bool $var
     * @return $this
     */
    public function setUnfiltered($var)
    {
        GPBUtil::checkBool($var);
        $this->unfiltered = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>bool showDeleted = 31;</code>
     * @return bool
     */
    public function getShowDeleted()
    {
        return $this->showDeleted;
    }

    /**
     * Generated from protobuf field <code>bool showDeleted = 31;</code>
     * @param bool $var
     * @return $this
     */
    public function setShowDeleted($var)
    {
        GPBUtil::checkBool($var);
        $this->showDeleted = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>bool deletedOnly = 32;</code>
     * @return bool
     */
    public function getDeletedOnly()
    {
        return $this->deletedOnly;
    }

    /**
     * Generated from protobuf field <code>bool deletedOnly = 32;</code>
     * @param bool $var
     * @return $this
     */
    public function setDeletedOnly($var)
    {
        GPBUtil::checkBool($var);
        $this->deletedOnly = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>repeated int64 idNotIn = 33;</code>
     * @return \Google\Protobuf\Internal\RepeatedField
     */
    public function getIdNotIn()
    {
        return $this->idNotIn;
    }

    /**
     * Generated from protobuf field <code>repeated int64 idNotIn = 33;</code>
     * @param int[]|string[]|\Google\Protobuf\Internal\RepeatedField $var
     * @return $this
     */
    public function setIdNotIn($var)
    {
        $arr = GPBUtil::checkRepeatedField($var, \Google\Protobuf\Internal\GPBType::INT64);
        $this->idNotIn = $arr;

        return $this;
    }

    /**
     * Generated from protobuf field <code>repeated int64 ids = 34;</code>
     * @return \Google\Protobuf\Internal\RepeatedField
     */
    public function getIds()
    {
        return $this->ids;
    }

    /**
     * Generated from protobuf field <code>repeated int64 ids = 34;</code>
     * @param int[]|string[]|\Google\Protobuf\Internal\RepeatedField $var
     * @return $this
     */
    public function setIds($var)
    {
        $arr = GPBUtil::checkRepeatedField($var, \Google\Protobuf\Internal\GPBType::INT64);
        $this->ids = $arr;

        return $this;
    }

}

