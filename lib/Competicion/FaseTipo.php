<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: competicion/compapi.proto

namespace Competicion;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * FaseTipo Messages
 *
 * Generated from protobuf message <code>competicion.FaseTipo</code>
 */
class FaseTipo extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>int64 id = 1;</code>
     */
    private $id = 0;
    /**
     * Generated from protobuf field <code>int64 tipoId = 2;</code>
     */
    private $tipoId = 0;
    /**
     * Generated from protobuf field <code>bool activo = 3;</code>
     */
    private $activo = false;
    /**
     * Generated from protobuf field <code>string label = 4;</code>
     */
    private $label = '';
    /**
     * Generated from protobuf field <code>string comentario = 5;</code>
     */
    private $comentario = '';
    /**
     * Generated from protobuf field <code>int32 publicable = 6;</code>
     */
    private $publicable = 0;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type int|string $id
     *     @type int|string $tipoId
     *     @type bool $activo
     *     @type string $label
     *     @type string $comentario
     *     @type int $publicable
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Competicion\Compapi::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>int64 id = 1;</code>
     * @return int|string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Generated from protobuf field <code>int64 id = 1;</code>
     * @param int|string $var
     * @return $this
     */
    public function setId($var)
    {
        GPBUtil::checkInt64($var);
        $this->id = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 tipoId = 2;</code>
     * @return int|string
     */
    public function getTipoId()
    {
        return $this->tipoId;
    }

    /**
     * Generated from protobuf field <code>int64 tipoId = 2;</code>
     * @param int|string $var
     * @return $this
     */
    public function setTipoId($var)
    {
        GPBUtil::checkInt64($var);
        $this->tipoId = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>bool activo = 3;</code>
     * @return bool
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Generated from protobuf field <code>bool activo = 3;</code>
     * @param bool $var
     * @return $this
     */
    public function setActivo($var)
    {
        GPBUtil::checkBool($var);
        $this->activo = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string label = 4;</code>
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Generated from protobuf field <code>string label = 4;</code>
     * @param string $var
     * @return $this
     */
    public function setLabel($var)
    {
        GPBUtil::checkString($var, True);
        $this->label = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string comentario = 5;</code>
     * @return string
     */
    public function getComentario()
    {
        return $this->comentario;
    }

    /**
     * Generated from protobuf field <code>string comentario = 5;</code>
     * @param string $var
     * @return $this
     */
    public function setComentario($var)
    {
        GPBUtil::checkString($var, True);
        $this->comentario = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int32 publicable = 6;</code>
     * @return int
     */
    public function getPublicable()
    {
        return $this->publicable;
    }

    /**
     * Generated from protobuf field <code>int32 publicable = 6;</code>
     * @param int $var
     * @return $this
     */
    public function setPublicable($var)
    {
        GPBUtil::checkInt32($var);
        $this->publicable = $var;

        return $this;
    }

}

