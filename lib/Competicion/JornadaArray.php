<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: competicion/compapi.proto

namespace Competicion;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>competicion.JornadaArray</code>
 */
class JornadaArray extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>repeated .competicion.Jornada jornadas = 1;</code>
     */
    private $jornadas;
    /**
     * Generated from protobuf field <code>int32 total = 2;</code>
     */
    private $total = 0;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type \Competicion\Jornada[]|\Google\Protobuf\Internal\RepeatedField $jornadas
     *     @type int $total
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Competicion\Compapi::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>repeated .competicion.Jornada jornadas = 1;</code>
     * @return \Google\Protobuf\Internal\RepeatedField
     */
    public function getJornadas()
    {
        return $this->jornadas;
    }

    /**
     * Generated from protobuf field <code>repeated .competicion.Jornada jornadas = 1;</code>
     * @param \Competicion\Jornada[]|\Google\Protobuf\Internal\RepeatedField $var
     * @return $this
     */
    public function setJornadas($var)
    {
        $arr = GPBUtil::checkRepeatedField($var, \Google\Protobuf\Internal\GPBType::MESSAGE, \Competicion\Jornada::class);
        $this->jornadas = $arr;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int32 total = 2;</code>
     * @return int
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Generated from protobuf field <code>int32 total = 2;</code>
     * @param int $var
     * @return $this
     */
    public function setTotal($var)
    {
        GPBUtil::checkInt32($var);
        $this->total = $var;

        return $this;
    }

}

