<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: competicion/compapi.proto

namespace Competicion;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Equipo Messages
 *
 * Generated from protobuf message <code>competicion.Equipo</code>
 */
class Equipo extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>int64 id = 1;</code>
     */
    private $id = 0;
    /**
     * Generated from protobuf field <code>int64 alta = 2;</code>
     */
    private $alta = 0;
    /**
     * Generated from protobuf field <code>int64 baja = 3;</code>
     */
    private $baja = 0;
    /**
     * Generated from protobuf field <code>int64 clubId = 4;</code>
     */
    private $clubId = 0;
    /**
     * Generated from protobuf field <code>.competicion.Club club = 5;</code>
     */
    private $club = null;
    /**
     * Generated from protobuf field <code>int64 pabellonId = 6;</code>
     */
    private $pabellonId = 0;
    /**
     * Generated from protobuf field <code>bool activo = 7;</code>
     */
    private $activo = false;
    /**
     * Generated from protobuf field <code>int32 publicable = 8;</code>
     */
    private $publicable = 0;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type int|string $id
     *     @type int|string $alta
     *     @type int|string $baja
     *     @type int|string $clubId
     *     @type \Competicion\Club $club
     *     @type int|string $pabellonId
     *     @type bool $activo
     *     @type int $publicable
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Competicion\Compapi::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>int64 id = 1;</code>
     * @return int|string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Generated from protobuf field <code>int64 id = 1;</code>
     * @param int|string $var
     * @return $this
     */
    public function setId($var)
    {
        GPBUtil::checkInt64($var);
        $this->id = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 alta = 2;</code>
     * @return int|string
     */
    public function getAlta()
    {
        return $this->alta;
    }

    /**
     * Generated from protobuf field <code>int64 alta = 2;</code>
     * @param int|string $var
     * @return $this
     */
    public function setAlta($var)
    {
        GPBUtil::checkInt64($var);
        $this->alta = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 baja = 3;</code>
     * @return int|string
     */
    public function getBaja()
    {
        return $this->baja;
    }

    /**
     * Generated from protobuf field <code>int64 baja = 3;</code>
     * @param int|string $var
     * @return $this
     */
    public function setBaja($var)
    {
        GPBUtil::checkInt64($var);
        $this->baja = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 clubId = 4;</code>
     * @return int|string
     */
    public function getClubId()
    {
        return $this->clubId;
    }

    /**
     * Generated from protobuf field <code>int64 clubId = 4;</code>
     * @param int|string $var
     * @return $this
     */
    public function setClubId($var)
    {
        GPBUtil::checkInt64($var);
        $this->clubId = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>.competicion.Club club = 5;</code>
     * @return \Competicion\Club
     */
    public function getClub()
    {
        return $this->club;
    }

    /**
     * Generated from protobuf field <code>.competicion.Club club = 5;</code>
     * @param \Competicion\Club $var
     * @return $this
     */
    public function setClub($var)
    {
        GPBUtil::checkMessage($var, \Competicion\Club::class);
        $this->club = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 pabellonId = 6;</code>
     * @return int|string
     */
    public function getPabellonId()
    {
        return $this->pabellonId;
    }

    /**
     * Generated from protobuf field <code>int64 pabellonId = 6;</code>
     * @param int|string $var
     * @return $this
     */
    public function setPabellonId($var)
    {
        GPBUtil::checkInt64($var);
        $this->pabellonId = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>bool activo = 7;</code>
     * @return bool
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Generated from protobuf field <code>bool activo = 7;</code>
     * @param bool $var
     * @return $this
     */
    public function setActivo($var)
    {
        GPBUtil::checkBool($var);
        $this->activo = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int32 publicable = 8;</code>
     * @return int
     */
    public function getPublicable()
    {
        return $this->publicable;
    }

    /**
     * Generated from protobuf field <code>int32 publicable = 8;</code>
     * @param int $var
     * @return $this
     */
    public function setPublicable($var)
    {
        GPBUtil::checkInt32($var);
        $this->publicable = $var;

        return $this;
    }

}

