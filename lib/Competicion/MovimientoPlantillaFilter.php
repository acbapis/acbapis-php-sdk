<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: competicion/compapi.proto

namespace Competicion;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>competicion.MovimientoPlantillaFilter</code>
 */
class MovimientoPlantillaFilter extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>int64 offset = 1;</code>
     */
    private $offset = 0;
    /**
     * Generated from protobuf field <code>int64 limit = 2;</code>
     */
    private $limit = 0;
    /**
     * Generated from protobuf field <code>int64 id = 3;</code>
     */
    private $id = 0;
    /**
     * Generated from protobuf field <code>int64 licenciaEquipoId = 4;</code>
     */
    private $licenciaEquipoId = 0;
    /**
     * Generated from protobuf field <code>int64 equipoId = 5;</code>
     */
    private $equipoId = 0;
    /**
     * Generated from protobuf field <code>repeated int64 equipoIds = 6;</code>
     */
    private $equipoIds;
    /**
     * Generated from protobuf field <code>int64 licenciaId = 7;</code>
     */
    private $licenciaId = 0;
    /**
     * Generated from protobuf field <code>repeated int64 licenciaIds = 8;</code>
     */
    private $licenciaIds;
    /**
     * Generated from protobuf field <code>string tipoEstadoInicial = 9;</code>
     */
    private $tipoEstadoInicial = '';
    /**
     * Generated from protobuf field <code>string tipoEstadoFinal = 10;</code>
     */
    private $tipoEstadoFinal = '';
    /**
     * Contabiliza filter. 1 for true, 2 for false
     *
     * Generated from protobuf field <code>int32 contabiliza = 11;</code>
     */
    private $contabiliza = 0;
    /**
     * Activo filter. 1 for active (true), 2 for inactive (false)
     *
     * Generated from protobuf field <code>int32 activo = 12;</code>
     */
    private $activo = 0;
    /**
     * Wanted fields separated by comma
     *
     * Generated from protobuf field <code>string fields = 13;</code>
     */
    private $fields = '';
    /**
     * Generated from protobuf field <code>.competicion.MovimientoPlantillaSorting sorting = 14;</code>
     */
    private $sorting = null;
    /**
     * Generated from protobuf field <code>int32 publicable = 15;</code>
     */
    private $publicable = 0;
    /**
     * If true, returns all records
     *
     * Generated from protobuf field <code>bool unfiltered = 30;</code>
     */
    private $unfiltered = false;
    /**
     * Generated from protobuf field <code>bool showDeleted = 31;</code>
     */
    private $showDeleted = false;
    /**
     * Generated from protobuf field <code>bool deletedOnly = 32;</code>
     */
    private $deletedOnly = false;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type int|string $offset
     *     @type int|string $limit
     *     @type int|string $id
     *     @type int|string $licenciaEquipoId
     *     @type int|string $equipoId
     *     @type int[]|string[]|\Google\Protobuf\Internal\RepeatedField $equipoIds
     *     @type int|string $licenciaId
     *     @type int[]|string[]|\Google\Protobuf\Internal\RepeatedField $licenciaIds
     *     @type string $tipoEstadoInicial
     *     @type string $tipoEstadoFinal
     *     @type int $contabiliza
     *           Contabiliza filter. 1 for true, 2 for false
     *     @type int $activo
     *           Activo filter. 1 for active (true), 2 for inactive (false)
     *     @type string $fields
     *           Wanted fields separated by comma
     *     @type \Competicion\MovimientoPlantillaSorting $sorting
     *     @type int $publicable
     *     @type bool $unfiltered
     *           If true, returns all records
     *     @type bool $showDeleted
     *     @type bool $deletedOnly
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Competicion\Compapi::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>int64 offset = 1;</code>
     * @return int|string
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     * Generated from protobuf field <code>int64 offset = 1;</code>
     * @param int|string $var
     * @return $this
     */
    public function setOffset($var)
    {
        GPBUtil::checkInt64($var);
        $this->offset = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 limit = 2;</code>
     * @return int|string
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * Generated from protobuf field <code>int64 limit = 2;</code>
     * @param int|string $var
     * @return $this
     */
    public function setLimit($var)
    {
        GPBUtil::checkInt64($var);
        $this->limit = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 id = 3;</code>
     * @return int|string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Generated from protobuf field <code>int64 id = 3;</code>
     * @param int|string $var
     * @return $this
     */
    public function setId($var)
    {
        GPBUtil::checkInt64($var);
        $this->id = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 licenciaEquipoId = 4;</code>
     * @return int|string
     */
    public function getLicenciaEquipoId()
    {
        return $this->licenciaEquipoId;
    }

    /**
     * Generated from protobuf field <code>int64 licenciaEquipoId = 4;</code>
     * @param int|string $var
     * @return $this
     */
    public function setLicenciaEquipoId($var)
    {
        GPBUtil::checkInt64($var);
        $this->licenciaEquipoId = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 equipoId = 5;</code>
     * @return int|string
     */
    public function getEquipoId()
    {
        return $this->equipoId;
    }

    /**
     * Generated from protobuf field <code>int64 equipoId = 5;</code>
     * @param int|string $var
     * @return $this
     */
    public function setEquipoId($var)
    {
        GPBUtil::checkInt64($var);
        $this->equipoId = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>repeated int64 equipoIds = 6;</code>
     * @return \Google\Protobuf\Internal\RepeatedField
     */
    public function getEquipoIds()
    {
        return $this->equipoIds;
    }

    /**
     * Generated from protobuf field <code>repeated int64 equipoIds = 6;</code>
     * @param int[]|string[]|\Google\Protobuf\Internal\RepeatedField $var
     * @return $this
     */
    public function setEquipoIds($var)
    {
        $arr = GPBUtil::checkRepeatedField($var, \Google\Protobuf\Internal\GPBType::INT64);
        $this->equipoIds = $arr;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 licenciaId = 7;</code>
     * @return int|string
     */
    public function getLicenciaId()
    {
        return $this->licenciaId;
    }

    /**
     * Generated from protobuf field <code>int64 licenciaId = 7;</code>
     * @param int|string $var
     * @return $this
     */
    public function setLicenciaId($var)
    {
        GPBUtil::checkInt64($var);
        $this->licenciaId = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>repeated int64 licenciaIds = 8;</code>
     * @return \Google\Protobuf\Internal\RepeatedField
     */
    public function getLicenciaIds()
    {
        return $this->licenciaIds;
    }

    /**
     * Generated from protobuf field <code>repeated int64 licenciaIds = 8;</code>
     * @param int[]|string[]|\Google\Protobuf\Internal\RepeatedField $var
     * @return $this
     */
    public function setLicenciaIds($var)
    {
        $arr = GPBUtil::checkRepeatedField($var, \Google\Protobuf\Internal\GPBType::INT64);
        $this->licenciaIds = $arr;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string tipoEstadoInicial = 9;</code>
     * @return string
     */
    public function getTipoEstadoInicial()
    {
        return $this->tipoEstadoInicial;
    }

    /**
     * Generated from protobuf field <code>string tipoEstadoInicial = 9;</code>
     * @param string $var
     * @return $this
     */
    public function setTipoEstadoInicial($var)
    {
        GPBUtil::checkString($var, True);
        $this->tipoEstadoInicial = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string tipoEstadoFinal = 10;</code>
     * @return string
     */
    public function getTipoEstadoFinal()
    {
        return $this->tipoEstadoFinal;
    }

    /**
     * Generated from protobuf field <code>string tipoEstadoFinal = 10;</code>
     * @param string $var
     * @return $this
     */
    public function setTipoEstadoFinal($var)
    {
        GPBUtil::checkString($var, True);
        $this->tipoEstadoFinal = $var;

        return $this;
    }

    /**
     * Contabiliza filter. 1 for true, 2 for false
     *
     * Generated from protobuf field <code>int32 contabiliza = 11;</code>
     * @return int
     */
    public function getContabiliza()
    {
        return $this->contabiliza;
    }

    /**
     * Contabiliza filter. 1 for true, 2 for false
     *
     * Generated from protobuf field <code>int32 contabiliza = 11;</code>
     * @param int $var
     * @return $this
     */
    public function setContabiliza($var)
    {
        GPBUtil::checkInt32($var);
        $this->contabiliza = $var;

        return $this;
    }

    /**
     * Activo filter. 1 for active (true), 2 for inactive (false)
     *
     * Generated from protobuf field <code>int32 activo = 12;</code>
     * @return int
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Activo filter. 1 for active (true), 2 for inactive (false)
     *
     * Generated from protobuf field <code>int32 activo = 12;</code>
     * @param int $var
     * @return $this
     */
    public function setActivo($var)
    {
        GPBUtil::checkInt32($var);
        $this->activo = $var;

        return $this;
    }

    /**
     * Wanted fields separated by comma
     *
     * Generated from protobuf field <code>string fields = 13;</code>
     * @return string
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * Wanted fields separated by comma
     *
     * Generated from protobuf field <code>string fields = 13;</code>
     * @param string $var
     * @return $this
     */
    public function setFields($var)
    {
        GPBUtil::checkString($var, True);
        $this->fields = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>.competicion.MovimientoPlantillaSorting sorting = 14;</code>
     * @return \Competicion\MovimientoPlantillaSorting
     */
    public function getSorting()
    {
        return $this->sorting;
    }

    /**
     * Generated from protobuf field <code>.competicion.MovimientoPlantillaSorting sorting = 14;</code>
     * @param \Competicion\MovimientoPlantillaSorting $var
     * @return $this
     */
    public function setSorting($var)
    {
        GPBUtil::checkMessage($var, \Competicion\MovimientoPlantillaSorting::class);
        $this->sorting = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int32 publicable = 15;</code>
     * @return int
     */
    public function getPublicable()
    {
        return $this->publicable;
    }

    /**
     * Generated from protobuf field <code>int32 publicable = 15;</code>
     * @param int $var
     * @return $this
     */
    public function setPublicable($var)
    {
        GPBUtil::checkInt32($var);
        $this->publicable = $var;

        return $this;
    }

    /**
     * If true, returns all records
     *
     * Generated from protobuf field <code>bool unfiltered = 30;</code>
     * @return bool
     */
    public function getUnfiltered()
    {
        return $this->unfiltered;
    }

    /**
     * If true, returns all records
     *
     * Generated from protobuf field <code>bool unfiltered = 30;</code>
     * @param bool $var
     * @return $this
     */
    public function setUnfiltered($var)
    {
        GPBUtil::checkBool($var);
        $this->unfiltered = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>bool showDeleted = 31;</code>
     * @return bool
     */
    public function getShowDeleted()
    {
        return $this->showDeleted;
    }

    /**
     * Generated from protobuf field <code>bool showDeleted = 31;</code>
     * @param bool $var
     * @return $this
     */
    public function setShowDeleted($var)
    {
        GPBUtil::checkBool($var);
        $this->showDeleted = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>bool deletedOnly = 32;</code>
     * @return bool
     */
    public function getDeletedOnly()
    {
        return $this->deletedOnly;
    }

    /**
     * Generated from protobuf field <code>bool deletedOnly = 32;</code>
     * @param bool $var
     * @return $this
     */
    public function setDeletedOnly($var)
    {
        GPBUtil::checkBool($var);
        $this->deletedOnly = $var;

        return $this;
    }

}

