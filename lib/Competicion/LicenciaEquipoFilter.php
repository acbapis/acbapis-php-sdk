<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: competicion/compapi.proto

namespace Competicion;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>competicion.LicenciaEquipoFilter</code>
 */
class LicenciaEquipoFilter extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>int64 equipoId = 1;</code>
     */
    private $equipoId = 0;
    /**
     * Generated from protobuf field <code>repeated int64 equipoIds = 2;</code>
     */
    private $equipoIds;
    /**
     * Generated from protobuf field <code>int64 licenciaId = 3;</code>
     */
    private $licenciaId = 0;
    /**
     * Generated from protobuf field <code>string tipoEstadoPlantilla = 4;</code>
     */
    private $tipoEstadoPlantilla = '';
    /**
     * Generated from protobuf field <code>int64 edicionId = 5;</code>
     */
    private $edicionId = 0;
    /**
     * Generated from protobuf field <code>repeated int64 edicionIds = 6;</code>
     */
    private $edicionIds;
    /**
     * Time filter. Return licencias_equipos where fecha_alta < time < fecha_baja
     *
     * Generated from protobuf field <code>int64 time = 7;</code>
     */
    private $time = 0;
    /**
     * Active filter. 1 for active (true), 2 for inactive (false)
     *
     * Generated from protobuf field <code>int32 activo = 8;</code>
     */
    private $activo = 0;
    /**
     * Wanted fields separated by comma
     *
     * Generated from protobuf field <code>string fields = 9;</code>
     */
    private $fields = '';
    /**
     * Generated from protobuf field <code>.competicion.LicenciaEquipoSorting sorting = 10;</code>
     */
    private $sorting = null;
    /**
     * Generated from protobuf field <code>int64 fechaAltaAntes = 11;</code>
     */
    private $fechaAltaAntes = 0;
    /**
     * Generated from protobuf field <code>int64 stateId = 12;</code>
     */
    private $stateId = 0;
    /**
     * Generated from protobuf field <code>repeated int64 stateIds = 13;</code>
     */
    private $stateIds;
    /**
     * Generated from protobuf field <code>int32 publicable = 14;</code>
     */
    private $publicable = 0;
    /**
     * Generated from protobuf field <code>int64 offset = 15;</code>
     */
    private $offset = 0;
    /**
     * Generated from protobuf field <code>int64 limit = 16;</code>
     */
    private $limit = 0;
    /**
     * If true, returns all records
     *
     * Generated from protobuf field <code>bool unfiltered = 30;</code>
     */
    private $unfiltered = false;
    /**
     * Generated from protobuf field <code>bool showDeleted = 31;</code>
     */
    private $showDeleted = false;
    /**
     * Generated from protobuf field <code>bool deletedOnly = 32;</code>
     */
    private $deletedOnly = false;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type int|string $equipoId
     *     @type int[]|string[]|\Google\Protobuf\Internal\RepeatedField $equipoIds
     *     @type int|string $licenciaId
     *     @type string $tipoEstadoPlantilla
     *     @type int|string $edicionId
     *     @type int[]|string[]|\Google\Protobuf\Internal\RepeatedField $edicionIds
     *     @type int|string $time
     *           Time filter. Return licencias_equipos where fecha_alta < time < fecha_baja
     *     @type int $activo
     *           Active filter. 1 for active (true), 2 for inactive (false)
     *     @type string $fields
     *           Wanted fields separated by comma
     *     @type \Competicion\LicenciaEquipoSorting $sorting
     *     @type int|string $fechaAltaAntes
     *     @type int|string $stateId
     *     @type int[]|string[]|\Google\Protobuf\Internal\RepeatedField $stateIds
     *     @type int $publicable
     *     @type int|string $offset
     *     @type int|string $limit
     *     @type bool $unfiltered
     *           If true, returns all records
     *     @type bool $showDeleted
     *     @type bool $deletedOnly
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Competicion\Compapi::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>int64 equipoId = 1;</code>
     * @return int|string
     */
    public function getEquipoId()
    {
        return $this->equipoId;
    }

    /**
     * Generated from protobuf field <code>int64 equipoId = 1;</code>
     * @param int|string $var
     * @return $this
     */
    public function setEquipoId($var)
    {
        GPBUtil::checkInt64($var);
        $this->equipoId = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>repeated int64 equipoIds = 2;</code>
     * @return \Google\Protobuf\Internal\RepeatedField
     */
    public function getEquipoIds()
    {
        return $this->equipoIds;
    }

    /**
     * Generated from protobuf field <code>repeated int64 equipoIds = 2;</code>
     * @param int[]|string[]|\Google\Protobuf\Internal\RepeatedField $var
     * @return $this
     */
    public function setEquipoIds($var)
    {
        $arr = GPBUtil::checkRepeatedField($var, \Google\Protobuf\Internal\GPBType::INT64);
        $this->equipoIds = $arr;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 licenciaId = 3;</code>
     * @return int|string
     */
    public function getLicenciaId()
    {
        return $this->licenciaId;
    }

    /**
     * Generated from protobuf field <code>int64 licenciaId = 3;</code>
     * @param int|string $var
     * @return $this
     */
    public function setLicenciaId($var)
    {
        GPBUtil::checkInt64($var);
        $this->licenciaId = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string tipoEstadoPlantilla = 4;</code>
     * @return string
     */
    public function getTipoEstadoPlantilla()
    {
        return $this->tipoEstadoPlantilla;
    }

    /**
     * Generated from protobuf field <code>string tipoEstadoPlantilla = 4;</code>
     * @param string $var
     * @return $this
     */
    public function setTipoEstadoPlantilla($var)
    {
        GPBUtil::checkString($var, True);
        $this->tipoEstadoPlantilla = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 edicionId = 5;</code>
     * @return int|string
     */
    public function getEdicionId()
    {
        return $this->edicionId;
    }

    /**
     * Generated from protobuf field <code>int64 edicionId = 5;</code>
     * @param int|string $var
     * @return $this
     */
    public function setEdicionId($var)
    {
        GPBUtil::checkInt64($var);
        $this->edicionId = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>repeated int64 edicionIds = 6;</code>
     * @return \Google\Protobuf\Internal\RepeatedField
     */
    public function getEdicionIds()
    {
        return $this->edicionIds;
    }

    /**
     * Generated from protobuf field <code>repeated int64 edicionIds = 6;</code>
     * @param int[]|string[]|\Google\Protobuf\Internal\RepeatedField $var
     * @return $this
     */
    public function setEdicionIds($var)
    {
        $arr = GPBUtil::checkRepeatedField($var, \Google\Protobuf\Internal\GPBType::INT64);
        $this->edicionIds = $arr;

        return $this;
    }

    /**
     * Time filter. Return licencias_equipos where fecha_alta < time < fecha_baja
     *
     * Generated from protobuf field <code>int64 time = 7;</code>
     * @return int|string
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Time filter. Return licencias_equipos where fecha_alta < time < fecha_baja
     *
     * Generated from protobuf field <code>int64 time = 7;</code>
     * @param int|string $var
     * @return $this
     */
    public function setTime($var)
    {
        GPBUtil::checkInt64($var);
        $this->time = $var;

        return $this;
    }

    /**
     * Active filter. 1 for active (true), 2 for inactive (false)
     *
     * Generated from protobuf field <code>int32 activo = 8;</code>
     * @return int
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Active filter. 1 for active (true), 2 for inactive (false)
     *
     * Generated from protobuf field <code>int32 activo = 8;</code>
     * @param int $var
     * @return $this
     */
    public function setActivo($var)
    {
        GPBUtil::checkInt32($var);
        $this->activo = $var;

        return $this;
    }

    /**
     * Wanted fields separated by comma
     *
     * Generated from protobuf field <code>string fields = 9;</code>
     * @return string
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * Wanted fields separated by comma
     *
     * Generated from protobuf field <code>string fields = 9;</code>
     * @param string $var
     * @return $this
     */
    public function setFields($var)
    {
        GPBUtil::checkString($var, True);
        $this->fields = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>.competicion.LicenciaEquipoSorting sorting = 10;</code>
     * @return \Competicion\LicenciaEquipoSorting
     */
    public function getSorting()
    {
        return $this->sorting;
    }

    /**
     * Generated from protobuf field <code>.competicion.LicenciaEquipoSorting sorting = 10;</code>
     * @param \Competicion\LicenciaEquipoSorting $var
     * @return $this
     */
    public function setSorting($var)
    {
        GPBUtil::checkMessage($var, \Competicion\LicenciaEquipoSorting::class);
        $this->sorting = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 fechaAltaAntes = 11;</code>
     * @return int|string
     */
    public function getFechaAltaAntes()
    {
        return $this->fechaAltaAntes;
    }

    /**
     * Generated from protobuf field <code>int64 fechaAltaAntes = 11;</code>
     * @param int|string $var
     * @return $this
     */
    public function setFechaAltaAntes($var)
    {
        GPBUtil::checkInt64($var);
        $this->fechaAltaAntes = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 stateId = 12;</code>
     * @return int|string
     */
    public function getStateId()
    {
        return $this->stateId;
    }

    /**
     * Generated from protobuf field <code>int64 stateId = 12;</code>
     * @param int|string $var
     * @return $this
     */
    public function setStateId($var)
    {
        GPBUtil::checkInt64($var);
        $this->stateId = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>repeated int64 stateIds = 13;</code>
     * @return \Google\Protobuf\Internal\RepeatedField
     */
    public function getStateIds()
    {
        return $this->stateIds;
    }

    /**
     * Generated from protobuf field <code>repeated int64 stateIds = 13;</code>
     * @param int[]|string[]|\Google\Protobuf\Internal\RepeatedField $var
     * @return $this
     */
    public function setStateIds($var)
    {
        $arr = GPBUtil::checkRepeatedField($var, \Google\Protobuf\Internal\GPBType::INT64);
        $this->stateIds = $arr;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int32 publicable = 14;</code>
     * @return int
     */
    public function getPublicable()
    {
        return $this->publicable;
    }

    /**
     * Generated from protobuf field <code>int32 publicable = 14;</code>
     * @param int $var
     * @return $this
     */
    public function setPublicable($var)
    {
        GPBUtil::checkInt32($var);
        $this->publicable = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 offset = 15;</code>
     * @return int|string
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     * Generated from protobuf field <code>int64 offset = 15;</code>
     * @param int|string $var
     * @return $this
     */
    public function setOffset($var)
    {
        GPBUtil::checkInt64($var);
        $this->offset = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 limit = 16;</code>
     * @return int|string
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * Generated from protobuf field <code>int64 limit = 16;</code>
     * @param int|string $var
     * @return $this
     */
    public function setLimit($var)
    {
        GPBUtil::checkInt64($var);
        $this->limit = $var;

        return $this;
    }

    /**
     * If true, returns all records
     *
     * Generated from protobuf field <code>bool unfiltered = 30;</code>
     * @return bool
     */
    public function getUnfiltered()
    {
        return $this->unfiltered;
    }

    /**
     * If true, returns all records
     *
     * Generated from protobuf field <code>bool unfiltered = 30;</code>
     * @param bool $var
     * @return $this
     */
    public function setUnfiltered($var)
    {
        GPBUtil::checkBool($var);
        $this->unfiltered = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>bool showDeleted = 31;</code>
     * @return bool
     */
    public function getShowDeleted()
    {
        return $this->showDeleted;
    }

    /**
     * Generated from protobuf field <code>bool showDeleted = 31;</code>
     * @param bool $var
     * @return $this
     */
    public function setShowDeleted($var)
    {
        GPBUtil::checkBool($var);
        $this->showDeleted = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>bool deletedOnly = 32;</code>
     * @return bool
     */
    public function getDeletedOnly()
    {
        return $this->deletedOnly;
    }

    /**
     * Generated from protobuf field <code>bool deletedOnly = 32;</code>
     * @param bool $var
     * @return $this
     */
    public function setDeletedOnly($var)
    {
        GPBUtil::checkBool($var);
        $this->deletedOnly = $var;

        return $this;
    }

}

