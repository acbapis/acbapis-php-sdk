<?php
// GENERATED CODE -- DO NOT EDIT!

namespace Competicion;

/**
 *
 * CompeticionService defines the methods exposed by this service
 *
 */
class CompeticionServiceClient extends \Grpc\BaseStub {

    /**
     * @param string $hostname hostname
     * @param array $opts channel options
     * @param \Grpc\Channel $channel (optional) re-use channel object
     */
    public function __construct($hostname, $opts, $channel = null) {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * Start Cargo endpoints
     * @param \Competicion\Cargo $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateCargo(\Competicion\Cargo $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/CreateCargo',
        $argument,
        ['\Competicion\Cargo', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\Cargo $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateCargo(\Competicion\Cargo $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/UpdateCargo',
        $argument,
        ['\Competicion\Cargo', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindCargoByID(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/FindCargoByID',
        $argument,
        ['\Competicion\Cargo', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\CargoFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListCargos(\Competicion\CargoFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/ListCargos',
        $argument,
        ['\Competicion\CargoArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\CargoFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function StreamCargos(\Competicion\CargoFilter $argument,
      $metadata = [], $options = []) {
        return $this->_serverStreamRequest('/competicion.CompeticionService/StreamCargos',
        $argument,
        ['\Competicion\Cargo', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteCargo(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/DeleteCargo',
        $argument,
        ['\Competicion\Cargo', 'decode'],
        $metadata, $options);
    }

    /**
     * Start Clasificacion endpoints
     * @param \Competicion\Clasificacion $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateClasificacion(\Competicion\Clasificacion $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/CreateClasificacion',
        $argument,
        ['\Competicion\Clasificacion', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\Clasificacion $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateClasificacion(\Competicion\Clasificacion $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/UpdateClasificacion',
        $argument,
        ['\Competicion\Clasificacion', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\ClasificacionFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindClasificacionByID(\Competicion\ClasificacionFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/FindClasificacionByID',
        $argument,
        ['\Competicion\Clasificacion', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\ClasificacionFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListClasificaciones(\Competicion\ClasificacionFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/ListClasificaciones',
        $argument,
        ['\Competicion\ClasificacionArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\ClasificacionFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function StreamClasificaciones(\Competicion\ClasificacionFilter $argument,
      $metadata = [], $options = []) {
        return $this->_serverStreamRequest('/competicion.CompeticionService/StreamClasificaciones',
        $argument,
        ['\Competicion\Clasificacion', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\EdicionFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindCurrentClasificacionByEdicion(\Competicion\EdicionFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/FindCurrentClasificacionByEdicion',
        $argument,
        ['\Competicion\ClasificacionArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteClasificacion(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/DeleteClasificacion',
        $argument,
        ['\Competicion\Clasificacion', 'decode'],
        $metadata, $options);
    }

    /**
     * Start Club endpoints
     * @param \Competicion\Club $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateClub(\Competicion\Club $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/CreateClub',
        $argument,
        ['\Competicion\Club', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\Club $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateClub(\Competicion\Club $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/UpdateClub',
        $argument,
        ['\Competicion\Club', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\ClubFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindClubByID(\Competicion\ClubFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/FindClubByID',
        $argument,
        ['\Competicion\Club', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\ClubFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListClubs(\Competicion\ClubFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/ListClubs',
        $argument,
        ['\Competicion\ClubArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\ClubFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function StreamClubs(\Competicion\ClubFilter $argument,
      $metadata = [], $options = []) {
        return $this->_serverStreamRequest('/competicion.CompeticionService/StreamClubs',
        $argument,
        ['\Competicion\Club', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteClub(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/DeleteClub',
        $argument,
        ['\Competicion\Club', 'decode'],
        $metadata, $options);
    }

    /**
     * Start Competicion endpoints
     * @param \Competicion\Competicion $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateCompeticion(\Competicion\Competicion $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/CreateCompeticion',
        $argument,
        ['\Competicion\Competicion', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\Competicion $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateCompeticion(\Competicion\Competicion $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/UpdateCompeticion',
        $argument,
        ['\Competicion\Competicion', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\CompeticionFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindCompeticionByID(\Competicion\CompeticionFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/FindCompeticionByID',
        $argument,
        ['\Competicion\Competicion', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\CompeticionFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListCompeticiones(\Competicion\CompeticionFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/ListCompeticiones',
        $argument,
        ['\Competicion\CompeticionArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\CompeticionFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function StreamCompeticiones(\Competicion\CompeticionFilter $argument,
      $metadata = [], $options = []) {
        return $this->_serverStreamRequest('/competicion.CompeticionService/StreamCompeticiones',
        $argument,
        ['\Competicion\Competicion', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteCompeticion(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/DeleteCompeticion',
        $argument,
        ['\Competicion\Competicion', 'decode'],
        $metadata, $options);
    }

    /**
     * Start Contrato endpoints
     * 	rpc FindContratoByID(common.IdMessage) returns (Contrato) {
     * 		option (google.api.http) = {
     * 			get: "/v1/contrato/{id}"
     * 		};
     * 	}
     * End Contrato endpoints
     *
     * Start ContratoSituacion endpoints
     * @param \Competicion\ContratoSituacion $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateContratoSituacion(\Competicion\ContratoSituacion $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/CreateContratoSituacion',
        $argument,
        ['\Competicion\ContratoSituacion', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\ContratoSituacion $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateContratoSituacion(\Competicion\ContratoSituacion $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/UpdateContratoSituacion',
        $argument,
        ['\Competicion\ContratoSituacion', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindContratoSituacionByID(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/FindContratoSituacionByID',
        $argument,
        ['\Competicion\ContratoSituacion', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\ContratoSituacionFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListContratoSituaciones(\Competicion\ContratoSituacionFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/ListContratoSituaciones',
        $argument,
        ['\Competicion\ContratoSituacionArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\ContratoSituacionFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function StreamContratoSituaciones(\Competicion\ContratoSituacionFilter $argument,
      $metadata = [], $options = []) {
        return $this->_serverStreamRequest('/competicion.CompeticionService/StreamContratoSituaciones',
        $argument,
        ['\Competicion\ContratoSituacion', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteContratoSituacion(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/DeleteContratoSituacion',
        $argument,
        ['\Competicion\ContratoSituacion', 'decode'],
        $metadata, $options);
    }

    /**
     * Start EdadConvenio endpoints
     * @param \Competicion\EdadConvenioFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CalculateEdadConvenio(\Competicion\EdadConvenioFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/CalculateEdadConvenio',
        $argument,
        ['\Competicion\EdadConvenio', 'decode'],
        $metadata, $options);
    }

    /**
     * Start Edicion endpoints
     * @param \Competicion\Edicion $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateEdicion(\Competicion\Edicion $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/CreateEdicion',
        $argument,
        ['\Competicion\Edicion', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\Edicion $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateEdicion(\Competicion\Edicion $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/UpdateEdicion',
        $argument,
        ['\Competicion\Edicion', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\EdicionFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindEdicionByID(\Competicion\EdicionFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/FindEdicionByID',
        $argument,
        ['\Competicion\Edicion', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\EdicionFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListEdiciones(\Competicion\EdicionFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/ListEdiciones',
        $argument,
        ['\Competicion\EdicionArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\EdicionFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function StreamEdiciones(\Competicion\EdicionFilter $argument,
      $metadata = [], $options = []) {
        return $this->_serverStreamRequest('/competicion.CompeticionService/StreamEdiciones',
        $argument,
        ['\Competicion\Edicion', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteEdicion(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/DeleteEdicion',
        $argument,
        ['\Competicion\Edicion', 'decode'],
        $metadata, $options);
    }

    /**
     * Start Equipacion endpoints
     * @param \Competicion\Equipacion $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateEquipacion(\Competicion\Equipacion $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/CreateEquipacion',
        $argument,
        ['\Competicion\Equipacion', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\Equipacion $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateEquipacion(\Competicion\Equipacion $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/UpdateEquipacion',
        $argument,
        ['\Competicion\Equipacion', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindEquipacionByID(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/FindEquipacionByID',
        $argument,
        ['\Competicion\Equipacion', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\EquipacionFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListEquipaciones(\Competicion\EquipacionFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/ListEquipaciones',
        $argument,
        ['\Competicion\EquipacionArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\EquipacionFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function StreamEquipaciones(\Competicion\EquipacionFilter $argument,
      $metadata = [], $options = []) {
        return $this->_serverStreamRequest('/competicion.CompeticionService/StreamEquipaciones',
        $argument,
        ['\Competicion\Equipacion', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteEquipacion(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/DeleteEquipacion',
        $argument,
        ['\Competicion\Equipacion', 'decode'],
        $metadata, $options);
    }

    /**
     * Start Equipo endpoints
     * @param \Competicion\Equipo $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateEquipo(\Competicion\Equipo $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/CreateEquipo',
        $argument,
        ['\Competicion\Equipo', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\Equipo $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateEquipo(\Competicion\Equipo $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/UpdateEquipo',
        $argument,
        ['\Competicion\Equipo', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\EquipoFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindEquipoByID(\Competicion\EquipoFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/FindEquipoByID',
        $argument,
        ['\Competicion\Equipo', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\EquipoFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListEquipos(\Competicion\EquipoFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/ListEquipos',
        $argument,
        ['\Competicion\EquipoArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\EquipoFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function StreamEquipos(\Competicion\EquipoFilter $argument,
      $metadata = [], $options = []) {
        return $this->_serverStreamRequest('/competicion.CompeticionService/StreamEquipos',
        $argument,
        ['\Competicion\Equipo', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListPartidosByEquipo(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/ListPartidosByEquipo',
        $argument,
        ['\Competicion\PartidoArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteEquipo(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/DeleteEquipo',
        $argument,
        ['\Competicion\Equipo', 'decode'],
        $metadata, $options);
    }

    /**
     * Start EquiposEdiciones endpoints
     * @param \Competicion\EquiposEdiciones $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateEquipoEdicion(\Competicion\EquiposEdiciones $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/CreateEquipoEdicion',
        $argument,
        ['\Competicion\EquiposEdiciones', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\EquiposEdiciones $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateEquipoEdicion(\Competicion\EquiposEdiciones $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/UpdateEquipoEdicion',
        $argument,
        ['\Competicion\EquiposEdiciones', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\EquiposEdicionesFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListEquiposEdiciones(\Competicion\EquiposEdicionesFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/ListEquiposEdiciones',
        $argument,
        ['\Competicion\EquiposEdicionesArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\EquiposEdicionesFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function StreamEquiposEdiciones(\Competicion\EquiposEdicionesFilter $argument,
      $metadata = [], $options = []) {
        return $this->_serverStreamRequest('/competicion.CompeticionService/StreamEquiposEdiciones',
        $argument,
        ['\Competicion\EquiposEdiciones', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteEquipoEdicion(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/DeleteEquipoEdicion',
        $argument,
        ['\Competicion\EquiposEdiciones', 'decode'],
        $metadata, $options);
    }

    /**
     * Start Fase endpoints
     * @param \Competicion\Fase $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateFase(\Competicion\Fase $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/CreateFase',
        $argument,
        ['\Competicion\Fase', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\Fase $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateFase(\Competicion\Fase $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/UpdateFase',
        $argument,
        ['\Competicion\Fase', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindFaseByID(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/FindFaseByID',
        $argument,
        ['\Competicion\Fase', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\FaseFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListFases(\Competicion\FaseFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/ListFases',
        $argument,
        ['\Competicion\FaseArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\FaseFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function StreamFases(\Competicion\FaseFilter $argument,
      $metadata = [], $options = []) {
        return $this->_serverStreamRequest('/competicion.CompeticionService/StreamFases',
        $argument,
        ['\Competicion\Fase', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteFase(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/DeleteFase',
        $argument,
        ['\Competicion\Fase', 'decode'],
        $metadata, $options);
    }

    /**
     * Start FaseTipo endpoints
     * @param \Competicion\FaseTipo $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateFaseTipo(\Competicion\FaseTipo $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/CreateFaseTipo',
        $argument,
        ['\Competicion\FaseTipo', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\FaseTipo $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateFaseTipo(\Competicion\FaseTipo $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/UpdateFaseTipo',
        $argument,
        ['\Competicion\FaseTipo', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\FaseTipoFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindFaseTipoByID(\Competicion\FaseTipoFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/FindFaseTipoByID',
        $argument,
        ['\Competicion\FaseTipo', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\FaseTipoFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListFaseTipos(\Competicion\FaseTipoFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/ListFaseTipos',
        $argument,
        ['\Competicion\FaseTipoArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\FaseTipoFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function StreamFaseTipos(\Competicion\FaseTipoFilter $argument,
      $metadata = [], $options = []) {
        return $this->_serverStreamRequest('/competicion.CompeticionService/StreamFaseTipos',
        $argument,
        ['\Competicion\FaseTipo', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteFaseTipo(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/DeleteFaseTipo',
        $argument,
        ['\Competicion\FaseTipo', 'decode'],
        $metadata, $options);
    }

    /**
     * Start Jornada endpoints
     * @param \Competicion\Jornada $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateJornada(\Competicion\Jornada $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/CreateJornada',
        $argument,
        ['\Competicion\Jornada', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\Jornada $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateJornada(\Competicion\Jornada $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/UpdateJornada',
        $argument,
        ['\Competicion\Jornada', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\JornadaFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindJornadaByID(\Competicion\JornadaFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/FindJornadaByID',
        $argument,
        ['\Competicion\Jornada', 'decode'],
        $metadata, $options);
    }

    /**
     * FindJornadaActual returns the ongoing or next Jornada for an Edicion
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindJornadaActual(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/FindJornadaActual',
        $argument,
        ['\Competicion\Jornada', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\JornadaFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListJornadas(\Competicion\JornadaFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/ListJornadas',
        $argument,
        ['\Competicion\JornadaArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\JornadaFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function StreamJornadas(\Competicion\JornadaFilter $argument,
      $metadata = [], $options = []) {
        return $this->_serverStreamRequest('/competicion.CompeticionService/StreamJornadas',
        $argument,
        ['\Competicion\Jornada', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteJornada(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/DeleteJornada',
        $argument,
        ['\Competicion\Jornada', 'decode'],
        $metadata, $options);
    }

    /**
     * Start Licencia endpoints
     * @param \Competicion\Licencia $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateLicencia(\Competicion\Licencia $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/CreateLicencia',
        $argument,
        ['\Competicion\Licencia', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\Licencia $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateLicencia(\Competicion\Licencia $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/UpdateLicencia',
        $argument,
        ['\Competicion\Licencia', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\LicenciaFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindLicenciaByID(\Competicion\LicenciaFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/FindLicenciaByID',
        $argument,
        ['\Competicion\Licencia', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\LicenciaFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListLicencias(\Competicion\LicenciaFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/ListLicencias',
        $argument,
        ['\Competicion\LicenciaArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\LicenciaFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function StreamLicencias(\Competicion\LicenciaFilter $argument,
      $metadata = [], $options = []) {
        return $this->_serverStreamRequest('/competicion.CompeticionService/StreamLicencias',
        $argument,
        ['\Competicion\Licencia', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteLicencia(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/DeleteLicencia',
        $argument,
        ['\Competicion\Licencia', 'decode'],
        $metadata, $options);
    }

    /**
     * Start LicenciaEquipo endpoints
     * @param \Competicion\LicenciaEquipo $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateLicenciaEquipo(\Competicion\LicenciaEquipo $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/CreateLicenciaEquipo',
        $argument,
        ['\Competicion\LicenciaEquipo', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\LicenciaEquipo $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateLicenciaEquipo(\Competicion\LicenciaEquipo $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/UpdateLicenciaEquipo',
        $argument,
        ['\Competicion\LicenciaEquipo', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindLicenciaEquipoByID(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/FindLicenciaEquipoByID',
        $argument,
        ['\Competicion\LicenciaEquipo', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\LicenciaEquipoFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListLicenciaEquipos(\Competicion\LicenciaEquipoFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/ListLicenciaEquipos',
        $argument,
        ['\Competicion\LicenciaEquipoArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\LicenciaEquipoFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function StreamLicenciaEquipos(\Competicion\LicenciaEquipoFilter $argument,
      $metadata = [], $options = []) {
        return $this->_serverStreamRequest('/competicion.CompeticionService/StreamLicenciaEquipos',
        $argument,
        ['\Competicion\LicenciaEquipo', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\LicenciaEquipoYearsFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetLicenciaEquipoYears(\Competicion\LicenciaEquipoYearsFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/GetLicenciaEquipoYears',
        $argument,
        ['\Competicion\LicenciaEquipoYears', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteLicenciaEquipo(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/DeleteLicenciaEquipo',
        $argument,
        ['\Competicion\LicenciaEquipo', 'decode'],
        $metadata, $options);
    }

    /**
     * Start LicenciaPartido endpoints
     * @param \Competicion\LicenciaPartido $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateLicenciaPartido(\Competicion\LicenciaPartido $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/CreateLicenciaPartido',
        $argument,
        ['\Competicion\LicenciaPartido', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\LicenciaPartido $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateLicenciaPartido(\Competicion\LicenciaPartido $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/UpdateLicenciaPartido',
        $argument,
        ['\Competicion\LicenciaPartido', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\LicenciaPartidoFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListLicenciasByPartido(\Competicion\LicenciaPartidoFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/ListLicenciasByPartido',
        $argument,
        ['\Competicion\LicenciaPartidoArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteLicenciaPartido(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/DeleteLicenciaPartido',
        $argument,
        ['\Competicion\LicenciaPartido', 'decode'],
        $metadata, $options);
    }

    /**
     * Start LicenciaTipo endpoints
     * @param \Competicion\LicenciaTipo $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateLicenciaTipo(\Competicion\LicenciaTipo $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/CreateLicenciaTipo',
        $argument,
        ['\Competicion\LicenciaTipo', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\LicenciaTipo $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateLicenciaTipo(\Competicion\LicenciaTipo $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/UpdateLicenciaTipo',
        $argument,
        ['\Competicion\LicenciaTipo', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindLicenciaTipoByID(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/FindLicenciaTipoByID',
        $argument,
        ['\Competicion\LicenciaTipo', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\LicenciaTipoFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListLicenciaTipos(\Competicion\LicenciaTipoFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/ListLicenciaTipos',
        $argument,
        ['\Competicion\LicenciaTipoArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\LicenciaTipoFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function StreamLicenciaTipos(\Competicion\LicenciaTipoFilter $argument,
      $metadata = [], $options = []) {
        return $this->_serverStreamRequest('/competicion.CompeticionService/StreamLicenciaTipos',
        $argument,
        ['\Competicion\LicenciaTipo', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteLicenciaTipo(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/DeleteLicenciaTipo',
        $argument,
        ['\Competicion\LicenciaTipo', 'decode'],
        $metadata, $options);
    }

    /**
     * Start MovimientoPlantilla endpoints
     * @param \Competicion\MovimientoPlantilla $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateMovimientoPlantilla(\Competicion\MovimientoPlantilla $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/CreateMovimientoPlantilla',
        $argument,
        ['\Competicion\MovimientoPlantilla', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\MovimientoPlantilla $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateMovimientoPlantilla(\Competicion\MovimientoPlantilla $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/UpdateMovimientoPlantilla',
        $argument,
        ['\Competicion\MovimientoPlantilla', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\MovimientoPlantillaFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindMovimientoPlantillaByID(\Competicion\MovimientoPlantillaFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/FindMovimientoPlantillaByID',
        $argument,
        ['\Competicion\MovimientoPlantilla', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\MovimientoPlantillaFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListMovimientosPlantillas(\Competicion\MovimientoPlantillaFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/ListMovimientosPlantillas',
        $argument,
        ['\Competicion\MovimientoPlantillaArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\MovimientoPlantillaFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function StreamMovimientosPlantillas(\Competicion\MovimientoPlantillaFilter $argument,
      $metadata = [], $options = []) {
        return $this->_serverStreamRequest('/competicion.CompeticionService/StreamMovimientosPlantillas',
        $argument,
        ['\Competicion\MovimientoPlantilla', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteMovimientoPlantilla(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/DeleteMovimientoPlantilla',
        $argument,
        ['\Competicion\MovimientoPlantilla', 'decode'],
        $metadata, $options);
    }

    /**
     * Start Nacionalidad endpoints
     * @param \Competicion\Nacionalidad $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateNacionalidad(\Competicion\Nacionalidad $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/CreateNacionalidad',
        $argument,
        ['\Competicion\Nacionalidad', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\Nacionalidad $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateNacionalidad(\Competicion\Nacionalidad $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/UpdateNacionalidad',
        $argument,
        ['\Competicion\Nacionalidad', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindNacionalidadByID(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/FindNacionalidadByID',
        $argument,
        ['\Competicion\Nacionalidad', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\NacionalidadFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListNacionalidades(\Competicion\NacionalidadFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/ListNacionalidades',
        $argument,
        ['\Competicion\NacionalidadArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\NacionalidadFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function StreamNacionalidades(\Competicion\NacionalidadFilter $argument,
      $metadata = [], $options = []) {
        return $this->_serverStreamRequest('/competicion.CompeticionService/StreamNacionalidades',
        $argument,
        ['\Competicion\Nacionalidad', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteNacionalidad(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/DeleteNacionalidad',
        $argument,
        ['\Competicion\Nacionalidad', 'decode'],
        $metadata, $options);
    }

    /**
     * Start Pabellon endpoints
     * @param \Competicion\Pabellon $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreatePabellon(\Competicion\Pabellon $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/CreatePabellon',
        $argument,
        ['\Competicion\Pabellon', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\Pabellon $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdatePabellon(\Competicion\Pabellon $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/UpdatePabellon',
        $argument,
        ['\Competicion\Pabellon', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\PabellonFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindPabellonByID(\Competicion\PabellonFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/FindPabellonByID',
        $argument,
        ['\Competicion\Pabellon', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\PabellonFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListPabellones(\Competicion\PabellonFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/ListPabellones',
        $argument,
        ['\Competicion\PabellonArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\PabellonFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function StreamPabellones(\Competicion\PabellonFilter $argument,
      $metadata = [], $options = []) {
        return $this->_serverStreamRequest('/competicion.CompeticionService/StreamPabellones',
        $argument,
        ['\Competicion\Pabellon', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeletePabellon(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/DeletePabellon',
        $argument,
        ['\Competicion\Pabellon', 'decode'],
        $metadata, $options);
    }

    /**
     * Start Partido endpoints
     * @param \Competicion\Partido $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreatePartido(\Competicion\Partido $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/CreatePartido',
        $argument,
        ['\Competicion\Partido', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\Partido $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdatePartido(\Competicion\Partido $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/UpdatePartido',
        $argument,
        ['\Competicion\Partido', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\PartidoFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindPartidoByID(\Competicion\PartidoFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/FindPartidoByID',
        $argument,
        ['\Competicion\Partido', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\PartidoFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListPartidos(\Competicion\PartidoFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/ListPartidos',
        $argument,
        ['\Competicion\PartidoArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\PartidoFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function StreamPartidos(\Competicion\PartidoFilter $argument,
      $metadata = [], $options = []) {
        return $this->_serverStreamRequest('/competicion.CompeticionService/StreamPartidos',
        $argument,
        ['\Competicion\Partido', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeletePartido(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/DeletePartido',
        $argument,
        ['\Competicion\Partido', 'decode'],
        $metadata, $options);
    }

    /**
     * Start PartidoConfiguracion endpoints
     * @param \Competicion\PartidoConfiguracion $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreatePartidoConfiguracion(\Competicion\PartidoConfiguracion $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/CreatePartidoConfiguracion',
        $argument,
        ['\Competicion\PartidoConfiguracion', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\PartidoConfiguracion $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdatePartidoConfiguracion(\Competicion\PartidoConfiguracion $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/UpdatePartidoConfiguracion',
        $argument,
        ['\Competicion\PartidoConfiguracion', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindPartidoConfiguracionByID(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/FindPartidoConfiguracionByID',
        $argument,
        ['\Competicion\PartidoConfiguracion', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\PartidoConfiguracionFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListPartidoConfiguraciones(\Competicion\PartidoConfiguracionFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/ListPartidoConfiguraciones',
        $argument,
        ['\Competicion\PartidoConfiguracionArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\PartidoConfiguracionFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function StreamPartidoConfiguraciones(\Competicion\PartidoConfiguracionFilter $argument,
      $metadata = [], $options = []) {
        return $this->_serverStreamRequest('/competicion.CompeticionService/StreamPartidoConfiguraciones',
        $argument,
        ['\Competicion\PartidoConfiguracion', 'decode'],
        $metadata, $options);
    }

    /**
     * Start PartidosEquipaciones endpoints
     * @param \Competicion\PartidosEquipaciones $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreatePartidoEquipacion(\Competicion\PartidosEquipaciones $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/CreatePartidoEquipacion',
        $argument,
        ['\Competicion\PartidosEquipaciones', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\PartidosEquipaciones $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdatePartidoEquipacion(\Competicion\PartidosEquipaciones $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/UpdatePartidoEquipacion',
        $argument,
        ['\Competicion\PartidosEquipaciones', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindPartidosEquipacionesByID(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/FindPartidosEquipacionesByID',
        $argument,
        ['\Competicion\PartidosEquipaciones', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\PartidosEquipacionesFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListEquipacionesByPartido(\Competicion\PartidosEquipacionesFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/ListEquipacionesByPartido',
        $argument,
        ['\Competicion\PartidosEquipacionesArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeletePartidoEquipacion(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/DeletePartidoEquipacion',
        $argument,
        ['\Competicion\PartidosEquipaciones', 'decode'],
        $metadata, $options);
    }

    /**
     * Start Persona endpoints
     * @param \Competicion\Persona $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreatePersona(\Competicion\Persona $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/CreatePersona',
        $argument,
        ['\Competicion\Persona', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\Persona $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdatePersona(\Competicion\Persona $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/UpdatePersona',
        $argument,
        ['\Competicion\Persona', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\PersonaFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindPersonaByID(\Competicion\PersonaFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/FindPersonaByID',
        $argument,
        ['\Competicion\Persona', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\PersonaFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListPersonas(\Competicion\PersonaFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/ListPersonas',
        $argument,
        ['\Competicion\PersonaArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\PersonaFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function StreamPersonas(\Competicion\PersonaFilter $argument,
      $metadata = [], $options = []) {
        return $this->_serverStreamRequest('/competicion.CompeticionService/StreamPersonas',
        $argument,
        ['\Competicion\Persona', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeletePersona(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/DeletePersona',
        $argument,
        ['\Competicion\Persona', 'decode'],
        $metadata, $options);
    }

    /**
     * SearchDuplicatePersona search if a person already exists in
     * database using several criteria
     * @param \Competicion\Persona $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function SearchDuplicatePersona(\Competicion\Persona $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/SearchDuplicatePersona',
        $argument,
        ['\Competicion\PersonaArray', 'decode'],
        $metadata, $options);
    }

    /**
     * Start PosicionLicenciaTipo endpoints
     * @param \Competicion\PosicionLicenciaTipo $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreatePosicionLicenciaTipo(\Competicion\PosicionLicenciaTipo $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/CreatePosicionLicenciaTipo',
        $argument,
        ['\Competicion\PosicionLicenciaTipo', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\PosicionLicenciaTipo $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdatePosicionLicenciaTipo(\Competicion\PosicionLicenciaTipo $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/UpdatePosicionLicenciaTipo',
        $argument,
        ['\Competicion\PosicionLicenciaTipo', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindPosicionLicenciaTipoByID(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/FindPosicionLicenciaTipoByID',
        $argument,
        ['\Competicion\PosicionLicenciaTipo', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\PosicionLicenciaTipoFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListPosicionLicenciaTipos(\Competicion\PosicionLicenciaTipoFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/ListPosicionLicenciaTipos',
        $argument,
        ['\Competicion\PosicionLicenciaTipoArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\PosicionLicenciaTipoFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function StreamPosicionLicenciaTipos(\Competicion\PosicionLicenciaTipoFilter $argument,
      $metadata = [], $options = []) {
        return $this->_serverStreamRequest('/competicion.CompeticionService/StreamPosicionLicenciaTipos',
        $argument,
        ['\Competicion\PosicionLicenciaTipo', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeletePosicionLicenciaTipo(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/DeletePosicionLicenciaTipo',
        $argument,
        ['\Competicion\PosicionLicenciaTipo', 'decode'],
        $metadata, $options);
    }

    /**
     * Start StatePlantillaTipo endpoints
     * @param \Competicion\StatePlantillaTipo $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateStatePlantillaTipo(\Competicion\StatePlantillaTipo $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/CreateStatePlantillaTipo',
        $argument,
        ['\Competicion\StatePlantillaTipo', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\StatePlantillaTipo $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateStatePlantillaTipo(\Competicion\StatePlantillaTipo $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/UpdateStatePlantillaTipo',
        $argument,
        ['\Competicion\StatePlantillaTipo', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindStatePlantillaTipoByID(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/FindStatePlantillaTipoByID',
        $argument,
        ['\Competicion\StatePlantillaTipo', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\StatePlantillaTipoFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListStatePlantillaTipos(\Competicion\StatePlantillaTipoFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/ListStatePlantillaTipos',
        $argument,
        ['\Competicion\StatePlantillaTipoArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteStatePlantillaTipo(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/DeleteStatePlantillaTipo',
        $argument,
        ['\Competicion\StatePlantillaTipo', 'decode'],
        $metadata, $options);
    }

    /**
     * Start StatusLicenciaTipo endpoints
     * @param \Competicion\StatusLicenciaTipo $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateStatusLicenciaTipo(\Competicion\StatusLicenciaTipo $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/CreateStatusLicenciaTipo',
        $argument,
        ['\Competicion\StatusLicenciaTipo', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\StatusLicenciaTipo $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateStatusLicenciaTipo(\Competicion\StatusLicenciaTipo $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/UpdateStatusLicenciaTipo',
        $argument,
        ['\Competicion\StatusLicenciaTipo', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindStatusLicenciaTipoByID(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/FindStatusLicenciaTipoByID',
        $argument,
        ['\Competicion\StatusLicenciaTipo', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\StatusLicenciaTipoFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListStatusLicenciaTipos(\Competicion\StatusLicenciaTipoFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/ListStatusLicenciaTipos',
        $argument,
        ['\Competicion\StatusLicenciaTipoArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\StatusLicenciaTipoFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function StreamStatusLicenciaTipos(\Competicion\StatusLicenciaTipoFilter $argument,
      $metadata = [], $options = []) {
        return $this->_serverStreamRequest('/competicion.CompeticionService/StreamStatusLicenciaTipos',
        $argument,
        ['\Competicion\StatusLicenciaTipo', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteStatusLicenciaTipo(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/DeleteStatusLicenciaTipo',
        $argument,
        ['\Competicion\StatusLicenciaTipo', 'decode'],
        $metadata, $options);
    }

    /**
     * Start StatusPlantillaTipo endpoints
     * @param \Competicion\StatusPlantillaTipo $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateStatusPlantillaTipo(\Competicion\StatusPlantillaTipo $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/CreateStatusPlantillaTipo',
        $argument,
        ['\Competicion\StatusPlantillaTipo', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\StatusPlantillaTipo $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateStatusPlantillaTipo(\Competicion\StatusPlantillaTipo $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/UpdateStatusPlantillaTipo',
        $argument,
        ['\Competicion\StatusPlantillaTipo', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindStatusPlantillaTipoByID(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/FindStatusPlantillaTipoByID',
        $argument,
        ['\Competicion\StatusPlantillaTipo', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\StatusPlantillaTipoFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListStatusPlantillaTipos(\Competicion\StatusPlantillaTipoFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/ListStatusPlantillaTipos',
        $argument,
        ['\Competicion\StatusPlantillaTipoArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\StatusPlantillaTipoFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function StreamStatusPlantillaTipos(\Competicion\StatusPlantillaTipoFilter $argument,
      $metadata = [], $options = []) {
        return $this->_serverStreamRequest('/competicion.CompeticionService/StreamStatusPlantillaTipos',
        $argument,
        ['\Competicion\StatusPlantillaTipo', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteStatusPlantillaTipo(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/DeleteStatusPlantillaTipo',
        $argument,
        ['\Competicion\StatusPlantillaTipo', 'decode'],
        $metadata, $options);
    }

    /**
     * Start Temporada endpoints
     * @param \Competicion\Temporada $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateTemporada(\Competicion\Temporada $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/CreateTemporada',
        $argument,
        ['\Competicion\Temporada', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\Temporada $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateTemporada(\Competicion\Temporada $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/UpdateTemporada',
        $argument,
        ['\Competicion\Temporada', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindTemporadaByID(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/FindTemporadaByID',
        $argument,
        ['\Competicion\Temporada', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\TemporadaFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListTemporadas(\Competicion\TemporadaFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/ListTemporadas',
        $argument,
        ['\Competicion\TemporadaArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\TemporadaFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function StreamTemporadas(\Competicion\TemporadaFilter $argument,
      $metadata = [], $options = []) {
        return $this->_serverStreamRequest('/competicion.CompeticionService/StreamTemporadas',
        $argument,
        ['\Competicion\Temporada', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteTemporada(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/DeleteTemporada',
        $argument,
        ['\Competicion\Temporada', 'decode'],
        $metadata, $options);
    }

    /**
     * Start TipoCargo endpoints
     * @param \Competicion\TipoCargo $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateTipoCargo(\Competicion\TipoCargo $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/CreateTipoCargo',
        $argument,
        ['\Competicion\TipoCargo', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\TipoCargo $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateTipoCargo(\Competicion\TipoCargo $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/UpdateTipoCargo',
        $argument,
        ['\Competicion\TipoCargo', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindTipoCargoByID(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/FindTipoCargoByID',
        $argument,
        ['\Competicion\TipoCargo', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\TipoCargoFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListTipoCargos(\Competicion\TipoCargoFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/ListTipoCargos',
        $argument,
        ['\Competicion\TipoCargoArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\TipoCargoFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function StreamTipoCargos(\Competicion\TipoCargoFilter $argument,
      $metadata = [], $options = []) {
        return $this->_serverStreamRequest('/competicion.CompeticionService/StreamTipoCargos',
        $argument,
        ['\Competicion\TipoCargo', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteTipoCargo(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/DeleteTipoCargo',
        $argument,
        ['\Competicion\TipoCargo', 'decode'],
        $metadata, $options);
    }

    /**
     * Start TipoCategoriaLicencia endpoints
     * @param \Competicion\TipoCategoriaLicencia $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateTipoCategoriaLicencia(\Competicion\TipoCategoriaLicencia $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/CreateTipoCategoriaLicencia',
        $argument,
        ['\Competicion\TipoCategoriaLicencia', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\TipoCategoriaLicencia $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateTipoCategoriaLicencia(\Competicion\TipoCategoriaLicencia $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/UpdateTipoCategoriaLicencia',
        $argument,
        ['\Competicion\TipoCategoriaLicencia', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindTipoCategoriaLicenciaByID(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/FindTipoCategoriaLicenciaByID',
        $argument,
        ['\Competicion\TipoCategoriaLicencia', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\TipoCategoriaLicenciaFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListTipoCategoriaLicencias(\Competicion\TipoCategoriaLicenciaFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/ListTipoCategoriaLicencias',
        $argument,
        ['\Competicion\TipoCategoriaLicenciaArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\TipoCategoriaLicenciaFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function StreamTipoCategoriaLicencias(\Competicion\TipoCategoriaLicenciaFilter $argument,
      $metadata = [], $options = []) {
        return $this->_serverStreamRequest('/competicion.CompeticionService/StreamTipoCategoriaLicencias',
        $argument,
        ['\Competicion\TipoCategoriaLicencia', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteTipoCategoriaLicencia(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/DeleteTipoCategoriaLicencia',
        $argument,
        ['\Competicion\TipoCategoriaLicencia', 'decode'],
        $metadata, $options);
    }

    /**
     * Start TipoIdentificacion endpoints
     * @param \Competicion\TipoIdentificacion $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateTipoIdentificacion(\Competicion\TipoIdentificacion $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/CreateTipoIdentificacion',
        $argument,
        ['\Competicion\TipoIdentificacion', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\TipoIdentificacion $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateTipoIdentificacion(\Competicion\TipoIdentificacion $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/UpdateTipoIdentificacion',
        $argument,
        ['\Competicion\TipoIdentificacion', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindTipoIdentificacionByID(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/FindTipoIdentificacionByID',
        $argument,
        ['\Competicion\TipoIdentificacion', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\TipoIdentificacionFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListTipoIdentificaciones(\Competicion\TipoIdentificacionFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/ListTipoIdentificaciones',
        $argument,
        ['\Competicion\TipoIdentificacionArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\TipoIdentificacionFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function StreamTipoIdentificaciones(\Competicion\TipoIdentificacionFilter $argument,
      $metadata = [], $options = []) {
        return $this->_serverStreamRequest('/competicion.CompeticionService/StreamTipoIdentificaciones',
        $argument,
        ['\Competicion\TipoIdentificacion', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteTipoIdentificacion(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/DeleteTipoIdentificacion',
        $argument,
        ['\Competicion\TipoIdentificacion', 'decode'],
        $metadata, $options);
    }

    /**
     * Start TipoLicenciaPartido endpoints
     * @param \Competicion\TipoLicenciaPartido $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateTipoLicenciaPartido(\Competicion\TipoLicenciaPartido $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/CreateTipoLicenciaPartido',
        $argument,
        ['\Competicion\TipoLicenciaPartido', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\TipoLicenciaPartido $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateTipoLicenciaPartido(\Competicion\TipoLicenciaPartido $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/UpdateTipoLicenciaPartido',
        $argument,
        ['\Competicion\TipoLicenciaPartido', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindTipoLicenciaPartidoByID(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/FindTipoLicenciaPartidoByID',
        $argument,
        ['\Competicion\TipoLicenciaPartido', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\TipoLicenciaPartidoFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListTipoLicenciaPartidos(\Competicion\TipoLicenciaPartidoFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/ListTipoLicenciaPartidos',
        $argument,
        ['\Competicion\TipoLicenciaPartidoArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteTipoLicenciaPartido(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/DeleteTipoLicenciaPartido',
        $argument,
        ['\Competicion\TipoLicenciaPartido', 'decode'],
        $metadata, $options);
    }

    /**
     * Start TramoContrato endpoints
     * 	rpc FindTramoContratoByID(common.IdMessage) returns (TramoContrato) {
     * 		option (google.api.http) = {
     * 			get: "/v1/tramocontrato/{id}"
     * 		};
     * 	}
     * End TramoContrato endpoints
     *
     * Start Transfer endpoints
     * 	rpc FindTransferByID(common.IdMessage) returns (Transfer) {
     * 		option (google.api.http) = {
     * 			get: "/v1/transfer/{id}"
     * 		};
     * 	}
     * End Transfer endpoints
     *
     * Start Vinculacion endpoints
     * @param \Competicion\Vinculacion $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateVinculacion(\Competicion\Vinculacion $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/CreateVinculacion',
        $argument,
        ['\Competicion\Vinculacion', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\Vinculacion $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateVinculacion(\Competicion\Vinculacion $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/UpdateVinculacion',
        $argument,
        ['\Competicion\Vinculacion', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindVinculacionByID(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/FindVinculacionByID',
        $argument,
        ['\Competicion\Vinculacion', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\VinculacionFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListVinculaciones(\Competicion\VinculacionFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/ListVinculaciones',
        $argument,
        ['\Competicion\VinculacionArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\VinculacionFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function StreamVinculaciones(\Competicion\VinculacionFilter $argument,
      $metadata = [], $options = []) {
        return $this->_serverStreamRequest('/competicion.CompeticionService/StreamVinculaciones',
        $argument,
        ['\Competicion\Vinculacion', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteVinculacion(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/DeleteVinculacion',
        $argument,
        ['\Competicion\Vinculacion', 'decode'],
        $metadata, $options);
    }

    /**
     * Start EntidadTipo endpoints
     * @param \Competicion\EntidadTipo $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateEntidadTipo(\Competicion\EntidadTipo $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/CreateEntidadTipo',
        $argument,
        ['\Competicion\EntidadTipo', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\EntidadTipo $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateEntidadTipo(\Competicion\EntidadTipo $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/UpdateEntidadTipo',
        $argument,
        ['\Competicion\EntidadTipo', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\EntidadTipoFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindEntidadTipoByID(\Competicion\EntidadTipoFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/FindEntidadTipoByID',
        $argument,
        ['\Competicion\EntidadTipo', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\EntidadTipoFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListEntidadTipos(\Competicion\EntidadTipoFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/ListEntidadTipos',
        $argument,
        ['\Competicion\EntidadTipoArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteEntidadTipo(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/DeleteEntidadTipo',
        $argument,
        ['\Competicion\EntidadTipo', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Competicion\CuadroHonorFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetCuadroHonor(\Competicion\CuadroHonorFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/competicion.CompeticionService/GetCuadroHonor',
        $argument,
        ['\Competicion\CuadroHonor', 'decode'],
        $metadata, $options);
    }

}
