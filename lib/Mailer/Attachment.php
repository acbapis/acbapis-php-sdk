<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: ans/agents/mailer/mailer.proto

namespace Mailer;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>mailer.Attachment</code>
 */
class Attachment extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>string filename = 1;</code>
     */
    private $filename = '';
    /**
     * Generated from protobuf field <code>bytes body = 2;</code>
     */
    private $body = '';

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type string $filename
     *     @type string $body
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Ans\Agents\Mailer\Mailer::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>string filename = 1;</code>
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Generated from protobuf field <code>string filename = 1;</code>
     * @param string $var
     * @return $this
     */
    public function setFilename($var)
    {
        GPBUtil::checkString($var, True);
        $this->filename = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>bytes body = 2;</code>
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Generated from protobuf field <code>bytes body = 2;</code>
     * @param string $var
     * @return $this
     */
    public function setBody($var)
    {
        GPBUtil::checkString($var, False);
        $this->body = $var;

        return $this;
    }

}

