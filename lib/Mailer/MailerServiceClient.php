<?php
// GENERATED CODE -- DO NOT EDIT!

namespace Mailer;

/**
 */
class MailerServiceClient extends \Grpc\BaseStub {

    /**
     * @param string $hostname hostname
     * @param array $opts channel options
     * @param \Grpc\Channel $channel (optional) re-use channel object
     */
    public function __construct($hostname, $opts, $channel = null) {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * @param \Mailer\Email $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function Send(\Mailer\Email $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/mailer.MailerService/Send',
        $argument,
        ['\Mailer\EmailResponse', 'decode'],
        $metadata, $options);
    }

}
