<?php
// GENERATED CODE -- DO NOT EDIT!

namespace Av;

/**
 */
class AVServiceClient extends \Grpc\BaseStub {

    /**
     * @param string $hostname hostname
     * @param array $opts channel options
     * @param \Grpc\Channel $channel (optional) re-use channel object
     */
    public function __construct($hostname, $opts, $channel = null) {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * @param \Av\Channel $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateChannel(\Av\Channel $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/av.AVService/CreateChannel',
        $argument,
        ['\Av\Channel', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Av\Channel $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateChannel(\Av\Channel $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/av.AVService/UpdateChannel',
        $argument,
        ['\Av\Channel', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindChannelByID(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/av.AVService/FindChannelByID',
        $argument,
        ['\Av\Channel', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Av\ChannelFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListChannels(\Av\ChannelFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/av.AVService/ListChannels',
        $argument,
        ['\Av\ChannelArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteChannel(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/av.AVService/DeleteChannel',
        $argument,
        ['\Av\Channel', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Av\Broadcast $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateBroadcast(\Av\Broadcast $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/av.AVService/CreateBroadcast',
        $argument,
        ['\Av\Broadcast', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Av\Broadcast $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateBroadcast(\Av\Broadcast $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/av.AVService/UpdateBroadcast',
        $argument,
        ['\Av\Broadcast', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindBroadcastByID(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/av.AVService/FindBroadcastByID',
        $argument,
        ['\Av\Broadcast', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Av\BroadcastFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListBroadcasts(\Av\BroadcastFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/av.AVService/ListBroadcasts',
        $argument,
        ['\Av\BroadcastArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteBroadcast(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/av.AVService/DeleteBroadcast',
        $argument,
        ['\Av\Broadcast', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Av\BroadcastType $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateBroadcastType(\Av\BroadcastType $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/av.AVService/CreateBroadcastType',
        $argument,
        ['\Av\BroadcastType', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Av\BroadcastType $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateBroadcastType(\Av\BroadcastType $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/av.AVService/UpdateBroadcastType',
        $argument,
        ['\Av\BroadcastType', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindBroadcastTypeByID(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/av.AVService/FindBroadcastTypeByID',
        $argument,
        ['\Av\BroadcastType', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Av\BroadcastTypeFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListBroadcastTypes(\Av\BroadcastTypeFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/av.AVService/ListBroadcastTypes',
        $argument,
        ['\Av\BroadcastTypeArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteBroadcastType(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/av.AVService/DeleteBroadcastType',
        $argument,
        ['\Av\BroadcastType', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Av\TvElection $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateTvElection(\Av\TvElection $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/av.AVService/CreateTvElection',
        $argument,
        ['\Av\TvElection', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Av\TvElection $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateTvElection(\Av\TvElection $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/av.AVService/UpdateTvElection',
        $argument,
        ['\Av\TvElection', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindTvElectionByID(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/av.AVService/FindTvElectionByID',
        $argument,
        ['\Av\TvElection', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Av\TvElectionFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListTvElections(\Av\TvElectionFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/av.AVService/ListTvElections',
        $argument,
        ['\Av\TvElectionArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteTvElection(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/av.AVService/DeleteTvElection',
        $argument,
        ['\Av\TvElection', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Av\Booking $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateBooking(\Av\Booking $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/av.AVService/CreateBooking',
        $argument,
        ['\Av\Booking', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Av\Booking $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateBooking(\Av\Booking $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/av.AVService/UpdateBooking',
        $argument,
        ['\Av\Booking', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindBookingByID(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/av.AVService/FindBookingByID',
        $argument,
        ['\Av\Booking', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Av\BookingFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListBookings(\Av\BookingFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/av.AVService/ListBookings',
        $argument,
        ['\Av\BookingArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteBooking(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/av.AVService/DeleteBooking',
        $argument,
        ['\Av\Booking', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Av\BookingType $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateBookingType(\Av\BookingType $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/av.AVService/CreateBookingType',
        $argument,
        ['\Av\BookingType', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Av\BookingType $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateBookingType(\Av\BookingType $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/av.AVService/UpdateBookingType',
        $argument,
        ['\Av\BookingType', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindBookingTypeByID(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/av.AVService/FindBookingTypeByID',
        $argument,
        ['\Av\BookingType', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Av\BookingTypeFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListBookingTypes(\Av\BookingTypeFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/av.AVService/ListBookingTypes',
        $argument,
        ['\Av\BookingTypeArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteBookingType(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/av.AVService/DeleteBookingType',
        $argument,
        ['\Av\BookingType', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Av\Broadcaster $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateBroadcaster(\Av\Broadcaster $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/av.AVService/CreateBroadcaster',
        $argument,
        ['\Av\Broadcaster', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Av\Broadcaster $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateBroadcaster(\Av\Broadcaster $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/av.AVService/UpdateBroadcaster',
        $argument,
        ['\Av\Broadcaster', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindBroadcasterByID(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/av.AVService/FindBroadcasterByID',
        $argument,
        ['\Av\Broadcaster', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Av\BroadcasterFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListBroadcasters(\Av\BroadcasterFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/av.AVService/ListBroadcasters',
        $argument,
        ['\Av\BroadcasterArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteBroadcaster(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/av.AVService/DeleteBroadcaster',
        $argument,
        ['\Av\Broadcaster', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Av\Platform $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreatePlatform(\Av\Platform $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/av.AVService/CreatePlatform',
        $argument,
        ['\Av\Platform', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Av\Platform $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdatePlatform(\Av\Platform $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/av.AVService/UpdatePlatform',
        $argument,
        ['\Av\Platform', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindPlatformByID(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/av.AVService/FindPlatformByID',
        $argument,
        ['\Av\Platform', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Av\PlatformFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListPlatforms(\Av\PlatformFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/av.AVService/ListPlatforms',
        $argument,
        ['\Av\PlatformArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeletePlatform(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/av.AVService/DeletePlatform',
        $argument,
        ['\Av\Platform', 'decode'],
        $metadata, $options);
    }

}
