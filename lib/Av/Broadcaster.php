<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: av/av.proto

namespace Av;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>av.Broadcaster</code>
 */
class Broadcaster extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>int64 id = 1;</code>
     */
    private $id = 0;
    /**
     * Generated from protobuf field <code>string idt = 2;</code>
     */
    private $idt = '';
    /**
     * Generated from protobuf field <code>string name = 3;</code>
     */
    private $name = '';
    /**
     * Generated from protobuf field <code>int64 dateIni = 4;</code>
     */
    private $dateIni = 0;
    /**
     * Generated from protobuf field <code>int64 dateEnd = 5;</code>
     */
    private $dateEnd = 0;
    /**
     * Generated from protobuf field <code>int64 countryID = 6;</code>
     */
    private $countryID = 0;
    /**
     * Generated from protobuf field <code>string territories = 7;</code>
     */
    private $territories = '';
    /**
     * Generated from protobuf field <code>string channels = 8;</code>
     */
    private $channels = '';
    /**
     * Generated from protobuf field <code>string alias = 9;</code>
     */
    private $alias = '';
    /**
     * Generated from protobuf field <code>string address = 10;</code>
     */
    private $address = '';
    /**
     * Generated from protobuf field <code>string emails = 11;</code>
     */
    private $emails = '';
    /**
     * Generated from protobuf field <code>string tfns = 12;</code>
     */
    private $tfns = '';
    /**
     * Generated from protobuf field <code>string contacts = 13;</code>
     */
    private $contacts = '';
    /**
     * Generated from protobuf field <code>string notes = 14;</code>
     */
    private $notes = '';
    /**
     * Generated from protobuf field <code>bool active = 15;</code>
     */
    private $active = false;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type int|string $id
     *     @type string $idt
     *     @type string $name
     *     @type int|string $dateIni
     *     @type int|string $dateEnd
     *     @type int|string $countryID
     *     @type string $territories
     *     @type string $channels
     *     @type string $alias
     *     @type string $address
     *     @type string $emails
     *     @type string $tfns
     *     @type string $contacts
     *     @type string $notes
     *     @type bool $active
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Av\Av::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>int64 id = 1;</code>
     * @return int|string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Generated from protobuf field <code>int64 id = 1;</code>
     * @param int|string $var
     * @return $this
     */
    public function setId($var)
    {
        GPBUtil::checkInt64($var);
        $this->id = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string idt = 2;</code>
     * @return string
     */
    public function getIdt()
    {
        return $this->idt;
    }

    /**
     * Generated from protobuf field <code>string idt = 2;</code>
     * @param string $var
     * @return $this
     */
    public function setIdt($var)
    {
        GPBUtil::checkString($var, True);
        $this->idt = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string name = 3;</code>
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Generated from protobuf field <code>string name = 3;</code>
     * @param string $var
     * @return $this
     */
    public function setName($var)
    {
        GPBUtil::checkString($var, True);
        $this->name = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 dateIni = 4;</code>
     * @return int|string
     */
    public function getDateIni()
    {
        return $this->dateIni;
    }

    /**
     * Generated from protobuf field <code>int64 dateIni = 4;</code>
     * @param int|string $var
     * @return $this
     */
    public function setDateIni($var)
    {
        GPBUtil::checkInt64($var);
        $this->dateIni = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 dateEnd = 5;</code>
     * @return int|string
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * Generated from protobuf field <code>int64 dateEnd = 5;</code>
     * @param int|string $var
     * @return $this
     */
    public function setDateEnd($var)
    {
        GPBUtil::checkInt64($var);
        $this->dateEnd = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 countryID = 6;</code>
     * @return int|string
     */
    public function getCountryID()
    {
        return $this->countryID;
    }

    /**
     * Generated from protobuf field <code>int64 countryID = 6;</code>
     * @param int|string $var
     * @return $this
     */
    public function setCountryID($var)
    {
        GPBUtil::checkInt64($var);
        $this->countryID = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string territories = 7;</code>
     * @return string
     */
    public function getTerritories()
    {
        return $this->territories;
    }

    /**
     * Generated from protobuf field <code>string territories = 7;</code>
     * @param string $var
     * @return $this
     */
    public function setTerritories($var)
    {
        GPBUtil::checkString($var, True);
        $this->territories = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string channels = 8;</code>
     * @return string
     */
    public function getChannels()
    {
        return $this->channels;
    }

    /**
     * Generated from protobuf field <code>string channels = 8;</code>
     * @param string $var
     * @return $this
     */
    public function setChannels($var)
    {
        GPBUtil::checkString($var, True);
        $this->channels = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string alias = 9;</code>
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * Generated from protobuf field <code>string alias = 9;</code>
     * @param string $var
     * @return $this
     */
    public function setAlias($var)
    {
        GPBUtil::checkString($var, True);
        $this->alias = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string address = 10;</code>
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Generated from protobuf field <code>string address = 10;</code>
     * @param string $var
     * @return $this
     */
    public function setAddress($var)
    {
        GPBUtil::checkString($var, True);
        $this->address = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string emails = 11;</code>
     * @return string
     */
    public function getEmails()
    {
        return $this->emails;
    }

    /**
     * Generated from protobuf field <code>string emails = 11;</code>
     * @param string $var
     * @return $this
     */
    public function setEmails($var)
    {
        GPBUtil::checkString($var, True);
        $this->emails = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string tfns = 12;</code>
     * @return string
     */
    public function getTfns()
    {
        return $this->tfns;
    }

    /**
     * Generated from protobuf field <code>string tfns = 12;</code>
     * @param string $var
     * @return $this
     */
    public function setTfns($var)
    {
        GPBUtil::checkString($var, True);
        $this->tfns = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string contacts = 13;</code>
     * @return string
     */
    public function getContacts()
    {
        return $this->contacts;
    }

    /**
     * Generated from protobuf field <code>string contacts = 13;</code>
     * @param string $var
     * @return $this
     */
    public function setContacts($var)
    {
        GPBUtil::checkString($var, True);
        $this->contacts = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string notes = 14;</code>
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Generated from protobuf field <code>string notes = 14;</code>
     * @param string $var
     * @return $this
     */
    public function setNotes($var)
    {
        GPBUtil::checkString($var, True);
        $this->notes = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>bool active = 15;</code>
     * @return bool
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Generated from protobuf field <code>bool active = 15;</code>
     * @param bool $var
     * @return $this
     */
    public function setActive($var)
    {
        GPBUtil::checkBool($var);
        $this->active = $var;

        return $this;
    }

}

