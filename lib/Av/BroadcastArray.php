<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: av/av.proto

namespace Av;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>av.BroadcastArray</code>
 */
class BroadcastArray extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>repeated .av.Broadcast broadcastings = 1;</code>
     */
    private $broadcastings;
    /**
     * Generated from protobuf field <code>int32 total = 2;</code>
     */
    private $total = 0;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type \Av\Broadcast[]|\Google\Protobuf\Internal\RepeatedField $broadcastings
     *     @type int $total
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Av\Av::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>repeated .av.Broadcast broadcastings = 1;</code>
     * @return \Google\Protobuf\Internal\RepeatedField
     */
    public function getBroadcastings()
    {
        return $this->broadcastings;
    }

    /**
     * Generated from protobuf field <code>repeated .av.Broadcast broadcastings = 1;</code>
     * @param \Av\Broadcast[]|\Google\Protobuf\Internal\RepeatedField $var
     * @return $this
     */
    public function setBroadcastings($var)
    {
        $arr = GPBUtil::checkRepeatedField($var, \Google\Protobuf\Internal\GPBType::MESSAGE, \Av\Broadcast::class);
        $this->broadcastings = $arr;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int32 total = 2;</code>
     * @return int
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Generated from protobuf field <code>int32 total = 2;</code>
     * @param int $var
     * @return $this
     */
    public function setTotal($var)
    {
        GPBUtil::checkInt32($var);
        $this->total = $var;

        return $this;
    }

}

