<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: av/av.proto

namespace Av;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>av.BroadcastFilter</code>
 */
class BroadcastFilter extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>int64 offset = 1;</code>
     */
    private $offset = 0;
    /**
     * Generated from protobuf field <code>int64 limit = 2;</code>
     */
    private $limit = 0;
    /**
     * Generated from protobuf field <code>int64 id = 3;</code>
     */
    private $id = 0;
    /**
     * Generated from protobuf field <code>int64 competitionID = 4;</code>
     */
    private $competitionID = 0;
    /**
     * Generated from protobuf field <code>int64 editionID = 5;</code>
     */
    private $editionID = 0;
    /**
     * Generated from protobuf field <code>int64 weekID = 6;</code>
     */
    private $weekID = 0;
    /**
     * Generated from protobuf field <code>string competitionStr = 7;</code>
     */
    private $competitionStr = '';
    /**
     * Generated from protobuf field <code>int32 editionNum = 8;</code>
     */
    private $editionNum = 0;
    /**
     * Generated from protobuf field <code>string editionStr = 9;</code>
     */
    private $editionStr = '';
    /**
     * Generated from protobuf field <code>int32 weekNum = 10;</code>
     */
    private $weekNum = 0;
    /**
     * Generated from protobuf field <code>string broadcastStr = 11;</code>
     */
    private $broadcastStr = '';
    /**
     * Generated from protobuf field <code>int64 gameID = 12;</code>
     */
    private $gameID = 0;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type int|string $offset
     *     @type int|string $limit
     *     @type int|string $id
     *     @type int|string $competitionID
     *     @type int|string $editionID
     *     @type int|string $weekID
     *     @type string $competitionStr
     *     @type int $editionNum
     *     @type string $editionStr
     *     @type int $weekNum
     *     @type string $broadcastStr
     *     @type int|string $gameID
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Av\Av::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>int64 offset = 1;</code>
     * @return int|string
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     * Generated from protobuf field <code>int64 offset = 1;</code>
     * @param int|string $var
     * @return $this
     */
    public function setOffset($var)
    {
        GPBUtil::checkInt64($var);
        $this->offset = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 limit = 2;</code>
     * @return int|string
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * Generated from protobuf field <code>int64 limit = 2;</code>
     * @param int|string $var
     * @return $this
     */
    public function setLimit($var)
    {
        GPBUtil::checkInt64($var);
        $this->limit = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 id = 3;</code>
     * @return int|string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Generated from protobuf field <code>int64 id = 3;</code>
     * @param int|string $var
     * @return $this
     */
    public function setId($var)
    {
        GPBUtil::checkInt64($var);
        $this->id = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 competitionID = 4;</code>
     * @return int|string
     */
    public function getCompetitionID()
    {
        return $this->competitionID;
    }

    /**
     * Generated from protobuf field <code>int64 competitionID = 4;</code>
     * @param int|string $var
     * @return $this
     */
    public function setCompetitionID($var)
    {
        GPBUtil::checkInt64($var);
        $this->competitionID = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 editionID = 5;</code>
     * @return int|string
     */
    public function getEditionID()
    {
        return $this->editionID;
    }

    /**
     * Generated from protobuf field <code>int64 editionID = 5;</code>
     * @param int|string $var
     * @return $this
     */
    public function setEditionID($var)
    {
        GPBUtil::checkInt64($var);
        $this->editionID = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 weekID = 6;</code>
     * @return int|string
     */
    public function getWeekID()
    {
        return $this->weekID;
    }

    /**
     * Generated from protobuf field <code>int64 weekID = 6;</code>
     * @param int|string $var
     * @return $this
     */
    public function setWeekID($var)
    {
        GPBUtil::checkInt64($var);
        $this->weekID = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string competitionStr = 7;</code>
     * @return string
     */
    public function getCompetitionStr()
    {
        return $this->competitionStr;
    }

    /**
     * Generated from protobuf field <code>string competitionStr = 7;</code>
     * @param string $var
     * @return $this
     */
    public function setCompetitionStr($var)
    {
        GPBUtil::checkString($var, True);
        $this->competitionStr = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int32 editionNum = 8;</code>
     * @return int
     */
    public function getEditionNum()
    {
        return $this->editionNum;
    }

    /**
     * Generated from protobuf field <code>int32 editionNum = 8;</code>
     * @param int $var
     * @return $this
     */
    public function setEditionNum($var)
    {
        GPBUtil::checkInt32($var);
        $this->editionNum = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string editionStr = 9;</code>
     * @return string
     */
    public function getEditionStr()
    {
        return $this->editionStr;
    }

    /**
     * Generated from protobuf field <code>string editionStr = 9;</code>
     * @param string $var
     * @return $this
     */
    public function setEditionStr($var)
    {
        GPBUtil::checkString($var, True);
        $this->editionStr = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int32 weekNum = 10;</code>
     * @return int
     */
    public function getWeekNum()
    {
        return $this->weekNum;
    }

    /**
     * Generated from protobuf field <code>int32 weekNum = 10;</code>
     * @param int $var
     * @return $this
     */
    public function setWeekNum($var)
    {
        GPBUtil::checkInt32($var);
        $this->weekNum = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string broadcastStr = 11;</code>
     * @return string
     */
    public function getBroadcastStr()
    {
        return $this->broadcastStr;
    }

    /**
     * Generated from protobuf field <code>string broadcastStr = 11;</code>
     * @param string $var
     * @return $this
     */
    public function setBroadcastStr($var)
    {
        GPBUtil::checkString($var, True);
        $this->broadcastStr = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 gameID = 12;</code>
     * @return int|string
     */
    public function getGameID()
    {
        return $this->gameID;
    }

    /**
     * Generated from protobuf field <code>int64 gameID = 12;</code>
     * @param int|string $var
     * @return $this
     */
    public function setGameID($var)
    {
        GPBUtil::checkInt64($var);
        $this->gameID = $var;

        return $this;
    }

}

