<?php
// GENERATED CODE -- DO NOT EDIT!

namespace Legalitas;

/**
 */
class LegalitasServiceClient extends \Grpc\BaseStub {

    /**
     * @param string $hostname hostname
     * @param array $opts channel options
     * @param \Grpc\Channel $channel (optional) re-use channel object
     */
    public function __construct($hostname, $opts, $channel = null) {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * DocType
     * @param \Legalitas\DocType $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateDocType(\Legalitas\DocType $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/legalitas.LegalitasService/CreateDocType',
        $argument,
        ['\Legalitas\DocType', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Legalitas\DocType $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateDocType(\Legalitas\DocType $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/legalitas.LegalitasService/UpdateDocType',
        $argument,
        ['\Legalitas\DocType', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Legalitas\FindByIdFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindDocTypeById(\Legalitas\FindByIdFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/legalitas.LegalitasService/FindDocTypeById',
        $argument,
        ['\Legalitas\DocType', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Legalitas\DocTypeFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListDocTypes(\Legalitas\DocTypeFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/legalitas.LegalitasService/ListDocTypes',
        $argument,
        ['\Legalitas\DocTypeArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteDocType(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/legalitas.LegalitasService/DeleteDocType',
        $argument,
        ['\Common\IdMessage', 'decode'],
        $metadata, $options);
    }

    /**
     * LegalText
     * @param \Legalitas\LegalText $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateLegalText(\Legalitas\LegalText $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/legalitas.LegalitasService/CreateLegalText',
        $argument,
        ['\Legalitas\LegalText', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Legalitas\LegalText $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateLegalText(\Legalitas\LegalText $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/legalitas.LegalitasService/UpdateLegalText',
        $argument,
        ['\Legalitas\LegalText', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Legalitas\FindByIdFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindLegalTextById(\Legalitas\FindByIdFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/legalitas.LegalitasService/FindLegalTextById',
        $argument,
        ['\Legalitas\LegalText', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Legalitas\LegalTextFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListLegalTexts(\Legalitas\LegalTextFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/legalitas.LegalitasService/ListLegalTexts',
        $argument,
        ['\Legalitas\LegalTextArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteLegalText(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/legalitas.LegalitasService/DeleteLegalText',
        $argument,
        ['\Common\IdMessage', 'decode'],
        $metadata, $options);
    }

    /**
     * Acceptance
     * @param \Legalitas\Acceptance $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateAcceptance(\Legalitas\Acceptance $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/legalitas.LegalitasService/CreateAcceptance',
        $argument,
        ['\Legalitas\Acceptance', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Legalitas\Acceptance $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateAcceptance(\Legalitas\Acceptance $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/legalitas.LegalitasService/UpdateAcceptance',
        $argument,
        ['\Legalitas\Acceptance', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Legalitas\AcceptanceFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindAcceptance(\Legalitas\AcceptanceFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/legalitas.LegalitasService/FindAcceptance',
        $argument,
        ['\Legalitas\Acceptance', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Legalitas\FindByIdFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindAcceptanceById(\Legalitas\FindByIdFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/legalitas.LegalitasService/FindAcceptanceById',
        $argument,
        ['\Legalitas\Acceptance', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Legalitas\AcceptanceFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListAcceptances(\Legalitas\AcceptanceFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/legalitas.LegalitasService/ListAcceptances',
        $argument,
        ['\Legalitas\AcceptanceArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteAcceptance(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/legalitas.LegalitasService/DeleteAcceptance',
        $argument,
        ['\Common\IdMessage', 'decode'],
        $metadata, $options);
    }

    /**
     * Context
     * @param \Legalitas\Context $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateContext(\Legalitas\Context $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/legalitas.LegalitasService/CreateContext',
        $argument,
        ['\Legalitas\Context', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Legalitas\Context $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateContext(\Legalitas\Context $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/legalitas.LegalitasService/UpdateContext',
        $argument,
        ['\Legalitas\Context', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Legalitas\FindByUuidFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindContextById(\Legalitas\FindByUuidFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/legalitas.LegalitasService/FindContextById',
        $argument,
        ['\Legalitas\Context', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Legalitas\ContextFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListContexts(\Legalitas\ContextFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/legalitas.LegalitasService/ListContexts',
        $argument,
        ['\Legalitas\ContextArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\UuidMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteContext(\Common\UuidMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/legalitas.LegalitasService/DeleteContext',
        $argument,
        ['\Common\UuidMessage', 'decode'],
        $metadata, $options);
    }

    /**
     * Format
     * @param \Legalitas\Format $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateFormat(\Legalitas\Format $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/legalitas.LegalitasService/CreateFormat',
        $argument,
        ['\Legalitas\Format', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Legalitas\Format $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateFormat(\Legalitas\Format $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/legalitas.LegalitasService/UpdateFormat',
        $argument,
        ['\Legalitas\Format', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Legalitas\FindByIdFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FindFormatById(\Legalitas\FindByIdFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/legalitas.LegalitasService/FindFormatById',
        $argument,
        ['\Legalitas\Format', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Legalitas\FormatFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListFormats(\Legalitas\FormatFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/legalitas.LegalitasService/ListFormats',
        $argument,
        ['\Legalitas\FormatArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteFormat(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/legalitas.LegalitasService/DeleteFormat',
        $argument,
        ['\Common\IdMessage', 'decode'],
        $metadata, $options);
    }

}
