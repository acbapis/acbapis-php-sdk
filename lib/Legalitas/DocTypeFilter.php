<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: legalitas/legalitas.proto

namespace Legalitas;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>legalitas.DocTypeFilter</code>
 */
class DocTypeFilter extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>int64 offset = 1;</code>
     */
    private $offset = 0;
    /**
     * Generated from protobuf field <code>int64 limit = 2;</code>
     */
    private $limit = 0;
    /**
     * Generated from protobuf field <code>int32 status = 3;</code>
     */
    private $status = 0;
    /**
     * Generated from protobuf field <code>string code = 4;</code>
     */
    private $code = '';

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type int|string $offset
     *     @type int|string $limit
     *     @type int $status
     *     @type string $code
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Legalitas\Legalitas::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>int64 offset = 1;</code>
     * @return int|string
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     * Generated from protobuf field <code>int64 offset = 1;</code>
     * @param int|string $var
     * @return $this
     */
    public function setOffset($var)
    {
        GPBUtil::checkInt64($var);
        $this->offset = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 limit = 2;</code>
     * @return int|string
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * Generated from protobuf field <code>int64 limit = 2;</code>
     * @param int|string $var
     * @return $this
     */
    public function setLimit($var)
    {
        GPBUtil::checkInt64($var);
        $this->limit = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int32 status = 3;</code>
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Generated from protobuf field <code>int32 status = 3;</code>
     * @param int $var
     * @return $this
     */
    public function setStatus($var)
    {
        GPBUtil::checkInt32($var);
        $this->status = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string code = 4;</code>
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Generated from protobuf field <code>string code = 4;</code>
     * @param string $var
     * @return $this
     */
    public function setCode($var)
    {
        GPBUtil::checkString($var, True);
        $this->code = $var;

        return $this;
    }

}

