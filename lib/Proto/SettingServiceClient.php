<?php
// GENERATED CODE -- DO NOT EDIT!

namespace Proto;

/**
 */
class SettingServiceClient extends \Grpc\BaseStub {

    /**
     * @param string $hostname hostname
     * @param array $opts channel options
     * @param \Grpc\Channel $channel (optional) re-use channel object
     */
    public function __construct($hostname, $opts, $channel = null) {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * @param \Proto\Setting $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateSetting(\Proto\Setting $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/proto.SettingService/CreateSetting',
        $argument,
        ['\Proto\Setting', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Proto\Setting $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function UpdateSetting(\Proto\Setting $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/proto.SettingService/UpdateSetting',
        $argument,
        ['\Proto\Setting', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Proto\Setting $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetSetting(\Proto\Setting $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/proto.SettingService/GetSetting',
        $argument,
        ['\Proto\Setting', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Proto\Bucket $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListSettings(\Proto\Bucket $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/proto.SettingService/ListSettings',
        $argument,
        ['\Proto\SettingArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Proto\Setting $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteSetting(\Proto\Setting $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/proto.SettingService/DeleteSetting',
        $argument,
        ['\Proto\ErrorMsg', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Proto\Bucket $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteBucket(\Proto\Bucket $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/proto.SettingService/DeleteBucket',
        $argument,
        ['\Proto\ErrorMsg', 'decode'],
        $metadata, $options);
    }

}
