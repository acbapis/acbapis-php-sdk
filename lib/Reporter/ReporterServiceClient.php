<?php
// GENERATED CODE -- DO NOT EDIT!

namespace Reporter;

/**
 */
class ReporterServiceClient extends \Grpc\BaseStub {

    /**
     * @param string $hostname hostname
     * @param array $opts channel options
     * @param \Grpc\Channel $channel (optional) re-use channel object
     */
    public function __construct($hostname, $opts, $channel = null) {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * @param \Reporter\ReportJornadaReq $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ReportJornada(\Reporter\ReportJornadaReq $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/reporter.ReporterService/ReportJornada',
        $argument,
        ['\Reporter\ReportJornadaResp', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Reporter\IndividualesRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ClasificacionIndividual(\Reporter\IndividualesRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/reporter.ReporterService/ClasificacionIndividual',
        $argument,
        ['\Reporter\FileReportResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Reporter\IndividualesRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ClasificacionesIndividuales(\Reporter\IndividualesRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/reporter.ReporterService/ClasificacionesIndividuales',
        $argument,
        ['\Reporter\FileReportResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * ResultadoPartido corresponde al fichero XML RESULTADO_PARTIDO
     * @param \Reporter\BaseRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ResultadoPartido(\Reporter\BaseRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/reporter.ReporterService/ResultadoPartido',
        $argument,
        ['\Reporter\FileReportResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * MatchBoxscores corresponde al fichero XML PARTIDOS_PLANTILLAS
     * @param \Reporter\BaseRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function MatchBoxscore(\Reporter\BaseRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/reporter.ReporterService/MatchBoxscore',
        $argument,
        ['\Reporter\FileReportResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Reporter\BaseRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function Clasificacion(\Reporter\BaseRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/reporter.ReporterService/Clasificacion',
        $argument,
        ['\Reporter\FileReportResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Reporter\AcumuladoEquipoRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function AcumuladosEquipo(\Reporter\AcumuladoEquipoRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/reporter.ReporterService/AcumuladosEquipo',
        $argument,
        ['\Reporter\FileReportResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Reporter\IndividualesRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ClasificacionHistorica(\Reporter\IndividualesRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/reporter.ReporterService/ClasificacionHistorica',
        $argument,
        ['\Reporter\FileReportResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Reporter\IndividualesRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ClasificacionesHistoricas(\Reporter\IndividualesRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/reporter.ReporterService/ClasificacionesHistoricas',
        $argument,
        ['\Reporter\FileReportResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Reporter\BaseRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function RankingArbitral(\Reporter\BaseRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/reporter.ReporterService/RankingArbitral',
        $argument,
        ['\Reporter\FileReportResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Reporter\BaseRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function StatsEquipos(\Reporter\BaseRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/reporter.ReporterService/StatsEquipos',
        $argument,
        ['\Reporter\FileReportResponse', 'decode'],
        $metadata, $options);
    }

}
