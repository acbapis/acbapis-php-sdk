<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: reporter/reporter.proto

namespace Reporter;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>reporter.IndividualesRequest</code>
 */
class IndividualesRequest extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>int64 seasonId = 1;</code>
     */
    private $seasonId = 0;
    /**
     * Generated from protobuf field <code>int64 competitionId = 2;</code>
     */
    private $competitionId = 0;
    /**
     * Generated from protobuf field <code>int64 editionId = 3;</code>
     */
    private $editionId = 0;
    /**
     * Generated from protobuf field <code>int64 roundId = 4;</code>
     */
    private $roundId = 0;
    /**
     * Generated from protobuf field <code>int64 teamId = 5;</code>
     */
    private $teamId = 0;
    /**
     **
     * typeId :
     * 141: assists
     * 128: 1pt throws
     * 14:  1pt percentage
     * 129: 2pt throws
     * 12:  2pt percentage
     * 130: 3pt throws
     * 10:  3pt percentage
     * 142: fouls
     * 143: fouls received
     * 134: dunks
     * 158: plus/minus (más/menos)
     * 140: turnovers
     * 149: points
     * 138: defensive rebounds
     * 135: offensive rebounds
     * 151: rebounds
     * 137: steals
     * 150: seconds played
     * 136: blocks
     * 139: blocks received
     * 152: valoración
     *
     * Generated from protobuf field <code>int64 typeId = 6;</code>
     */
    private $typeId = 0;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type int|string $seasonId
     *     @type int|string $competitionId
     *     @type int|string $editionId
     *     @type int|string $roundId
     *     @type int|string $teamId
     *     @type int|string $typeId
     *          *
     *           typeId :
     *           141: assists
     *           128: 1pt throws
     *           14:  1pt percentage
     *           129: 2pt throws
     *           12:  2pt percentage
     *           130: 3pt throws
     *           10:  3pt percentage
     *           142: fouls
     *           143: fouls received
     *           134: dunks
     *           158: plus/minus (más/menos)
     *           140: turnovers
     *           149: points
     *           138: defensive rebounds
     *           135: offensive rebounds
     *           151: rebounds
     *           137: steals
     *           150: seconds played
     *           136: blocks
     *           139: blocks received
     *           152: valoración
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Reporter\Reporter::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>int64 seasonId = 1;</code>
     * @return int|string
     */
    public function getSeasonId()
    {
        return $this->seasonId;
    }

    /**
     * Generated from protobuf field <code>int64 seasonId = 1;</code>
     * @param int|string $var
     * @return $this
     */
    public function setSeasonId($var)
    {
        GPBUtil::checkInt64($var);
        $this->seasonId = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 competitionId = 2;</code>
     * @return int|string
     */
    public function getCompetitionId()
    {
        return $this->competitionId;
    }

    /**
     * Generated from protobuf field <code>int64 competitionId = 2;</code>
     * @param int|string $var
     * @return $this
     */
    public function setCompetitionId($var)
    {
        GPBUtil::checkInt64($var);
        $this->competitionId = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 editionId = 3;</code>
     * @return int|string
     */
    public function getEditionId()
    {
        return $this->editionId;
    }

    /**
     * Generated from protobuf field <code>int64 editionId = 3;</code>
     * @param int|string $var
     * @return $this
     */
    public function setEditionId($var)
    {
        GPBUtil::checkInt64($var);
        $this->editionId = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 roundId = 4;</code>
     * @return int|string
     */
    public function getRoundId()
    {
        return $this->roundId;
    }

    /**
     * Generated from protobuf field <code>int64 roundId = 4;</code>
     * @param int|string $var
     * @return $this
     */
    public function setRoundId($var)
    {
        GPBUtil::checkInt64($var);
        $this->roundId = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 teamId = 5;</code>
     * @return int|string
     */
    public function getTeamId()
    {
        return $this->teamId;
    }

    /**
     * Generated from protobuf field <code>int64 teamId = 5;</code>
     * @param int|string $var
     * @return $this
     */
    public function setTeamId($var)
    {
        GPBUtil::checkInt64($var);
        $this->teamId = $var;

        return $this;
    }

    /**
     **
     * typeId :
     * 141: assists
     * 128: 1pt throws
     * 14:  1pt percentage
     * 129: 2pt throws
     * 12:  2pt percentage
     * 130: 3pt throws
     * 10:  3pt percentage
     * 142: fouls
     * 143: fouls received
     * 134: dunks
     * 158: plus/minus (más/menos)
     * 140: turnovers
     * 149: points
     * 138: defensive rebounds
     * 135: offensive rebounds
     * 151: rebounds
     * 137: steals
     * 150: seconds played
     * 136: blocks
     * 139: blocks received
     * 152: valoración
     *
     * Generated from protobuf field <code>int64 typeId = 6;</code>
     * @return int|string
     */
    public function getTypeId()
    {
        return $this->typeId;
    }

    /**
     **
     * typeId :
     * 141: assists
     * 128: 1pt throws
     * 14:  1pt percentage
     * 129: 2pt throws
     * 12:  2pt percentage
     * 130: 3pt throws
     * 10:  3pt percentage
     * 142: fouls
     * 143: fouls received
     * 134: dunks
     * 158: plus/minus (más/menos)
     * 140: turnovers
     * 149: points
     * 138: defensive rebounds
     * 135: offensive rebounds
     * 151: rebounds
     * 137: steals
     * 150: seconds played
     * 136: blocks
     * 139: blocks received
     * 152: valoración
     *
     * Generated from protobuf field <code>int64 typeId = 6;</code>
     * @param int|string $var
     * @return $this
     */
    public function setTypeId($var)
    {
        GPBUtil::checkInt64($var);
        $this->typeId = $var;

        return $this;
    }

}

