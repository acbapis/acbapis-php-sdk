<?php
// GENERATED CODE -- DO NOT EDIT!

namespace News;

/**
 */
class NewsServiceClient extends \Grpc\BaseStub {

    /**
     * @param string $hostname hostname
     * @param array $opts channel options
     * @param \Grpc\Channel $channel (optional) re-use channel object
     */
    public function __construct($hostname, $opts, $channel = null) {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * @param \News\NewsFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListNews(\News\NewsFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/news.NewsService/ListNews',
        $argument,
        ['\News\NewsArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \News\NewsListGroupedFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListNewsGrouped(\News\NewsListGroupedFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/news.NewsService/ListNewsGrouped',
        $argument,
        ['\News\NewsListGrouped', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetNewsContent(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/news.NewsService/GetNewsContent',
        $argument,
        ['\News\NewsContent', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \News\NewsFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DirectListNews(\News\NewsFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/news.NewsService/DirectListNews',
        $argument,
        ['\News\NewsArray', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \News\NewsListGroupedFilter $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DirectListNewsGrouped(\News\NewsListGroupedFilter $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/news.NewsService/DirectListNewsGrouped',
        $argument,
        ['\News\NewsListGrouped', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Common\IdMessage $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DirectGetNewsContent(\Common\IdMessage $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/news.NewsService/DirectGetNewsContent',
        $argument,
        ['\News\NewsContent', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \News\NewsItem $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function AddNews(\News\NewsItem $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/news.NewsService/AddNews',
        $argument,
        ['\News\AddNewsResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \News\NewsArray $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function AddNewsBatch(\News\NewsArray $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/news.NewsService/AddNewsBatch',
        $argument,
        ['\News\AddNewsResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function AddNewsStream($metadata = [], $options = []) {
        return $this->_clientStreamRequest('/news.NewsService/AddNewsStream',
        ['\News\AddNewsResponse','decode'],
        $metadata, $options);
    }

}
