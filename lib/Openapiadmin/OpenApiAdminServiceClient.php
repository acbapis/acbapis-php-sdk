<?php
// GENERATED CODE -- DO NOT EDIT!

namespace Openapiadmin;

/**
 */
class OpenApiAdminServiceClient extends \Grpc\BaseStub {

    /**
     * @param string $hostname hostname
     * @param array $opts channel options
     * @param \Grpc\Channel $channel (optional) re-use channel object
     */
    public function __construct($hostname, $opts, $channel = null) {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * @param \Openapiadmin\FlushCacheRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function FlushCache(\Openapiadmin\FlushCacheRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/openapiadmin.OpenApiAdminService/FlushCache',
        $argument,
        ['\Common\EmptyMessage', 'decode'],
        $metadata, $options);
    }

}
