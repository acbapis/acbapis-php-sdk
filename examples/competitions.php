<?php

require dirname(__FILE__).'/vendor/autoload.php';

$server = "oa.dev.acbdata.net:59800";
$client = new \Openapi\OpenApiServiceClient($server, [
    "credentials" => \Grpc\ChannelCredentials::createInsecure(),
]);

$competitionFilter = new \Openapi\CompetitionFilter();

list($competitions, $status) = $client->ListCompetitions($competitionFilter)->wait();

foreach ($competitions->getCompetitions() as $competition) {
        echo "[" . $competition->getId() . "] " . $competition->getCompetitionStr() . "\n";
}

// Close the connection
$client->close();
