<?php

require dirname(__FILE__).'/vendor/autoload.php';

$server = "oa.dev.acbdata.net:59800";
$client = new \Openapi\OpenApiServiceClient($server, [
    "credentials" => \Grpc\ChannelCredentials::createInsecure(),
]);

$matchFilter = new \Openapi\MatchFilter();
$matchFilter->setRunning(true);

list($matches, $status) = $client->ListMatches($matchFilter)->wait();

echo "Partidos en juego:\n";
foreach ($matches->getMatches() as $match) {
        echo $match->getMatchNum() . "º " . $match->getLocalTeam() . "-" . $match->getVisitingTeam() . "\n";
}

// Close the connection
$client->close();
