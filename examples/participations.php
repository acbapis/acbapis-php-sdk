<?php

require dirname(__FILE__).'/vendor/autoload.php';

$server = "oa.acbapis.com:59800";
$client = new \Openapi\OpenApiServiceClient($server, [
    "credentials" => \Grpc\ChannelCredentials::createInsecure(),
]);

$filter = new \Openapi\ParticipationFilter();
$filter->setEditionId(920);

list($participations, $status) = $client->ListParticipations($filter)->wait();

echo "Equipos participantes edición 920:\n";
foreach ($participations->getParticipations() as $p) {
	$team = $p->getTeam();
	$arenaStr = "";
	// Always check for null
	if ($p->getArena() != null) {
		$arenaStr = $p->getArena()->getArenaStr();
	}

	echo "- ".$team->getTeamStr()." [".$arenaStr."]"."\n";
}

$filter = new \Openapi\ParticipationFilter();
$filter->setEditionId(920);
$filter->setTeamId(9);

list($participations, $status) = $client->ListParticipations($filter)->wait();

echo "\nParticipación Real Madrid en la edición 920:\n";
foreach ($participations->getParticipations() as $p) {
	$team = $p->getTeam();
	$arena = $p->getArena();
	if ($arena == null) {
		$arena = new \Openapi\Arena();
	}
	$dates = $p->getDateTime();

	echo "Pabellon: ".$arena->getArenaStr()."\n";
	echo "Capacidad: ".$arena->getCapacity()."\n";

	foreach ($dates as $date) {
		if ($date->getType() == \Openapi\DateTime_Type::WORKINGDAY) {
			echo "Horario entre semana: ".dayNumToText($date->getDay())." a las ".$date->getTime()."\n";
		}
		if ($date->getType() == \Openapi\DateTime_Type::HOLIDAY) {
			echo "Horario fin de semana: ".dayNumToText($date->getDay())." a las ".$date->getTime()."\n";
		}
	}
}

function dayNumToText($num) {
	switch ($num) {
		case 1: return "lunes";
		case 2: return "martes";
		case 3: return "miércoles";
		case 4: return "jueves";
		case 5: return "viernes";
		case 6: return "sábado";
		case 7: return "domingo";
		default: return "";
	}
}

// Close the connection
$client->close();
