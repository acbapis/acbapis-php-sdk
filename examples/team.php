<?php

require dirname(__FILE__).'/vendor/autoload.php';

$server = "oa.dev.acbdata.net:59800";
$client = new \Openapi\OpenApiServiceClient($server, [
    "credentials" => \Grpc\ChannelCredentials::createInsecure(),
]);

$findByIdFilter = new \Openapi\FindByIdFilter();
$findByIdFilter->setId(3);

list($team, $status) = $client->FindTeamByID($findByIdFilter)->wait();

echo "ID: " . $team->getId() . "\n";
echo "Nombre: " . $team->getTeamStr() . "\n";
echo "Abreviatura: " . $team->getTeamAbbrev() . "\n";

foreach ($team->getImages() as $image) {
        echo $image->getType() . ": " . $image->getUrl() . "\n";
}

// Close the connection
$client->close();
