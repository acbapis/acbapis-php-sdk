<?php

require dirname(__FILE__).'/vendor/autoload.php';

$server = "oa.dev.acbdata.net:59800";
$client = new \Openapi\OpenApiServiceClient($server, [
    "credentials" => \Grpc\ChannelCredentials::createInsecure(),
]);

$emptyFilter = new \Common\EmptyMessage();

list($settings, $status) = $client->GetSettings($emptyFilter)->wait();

echo "Competición en curso: " . $settings->getCompetitionId() . "\n";
echo "Edición en curso: " . $settings->getEditionId() . "\n";
echo "Jornada en curso: " . $settings->getWeekId() . "\n";

// Close the connection
$client->close();
