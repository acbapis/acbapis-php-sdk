<?php

require dirname(__FILE__).'/vendor/autoload.php';

$server = "oa.dev.acbdata.net:59800";
$client = new \Openapi\OpenApiServiceClient($server, [
    "credentials" => \Grpc\ChannelCredentials::createInsecure(),
]);

// Obtener solo el Play By Play
$pbpFilter = new \Openapi\GetPbpFilter();
$pbpFilter->setMatchID(18126);

list($pbp, $status) = $client->GetPbp($pbpFilter)->wait();

echo "Play By Play partido ID: " . $pbp->getMatchId() . "\n";

echo $pbp->getHomeTeamName() . " - " . $pbp->getVisitorTeamName() . "\n";

foreach ($pbp->getPlays() as $play) {
        $num = $play->getPlayNum();
        $dorsal = $play->getShirtNumber();
        $texto = $play->getText();
        echo "$num º: Dorsal $dorsal $texto" . "\n";
}

// Obtener todos los datos del partido
$findByIdFilter = new \Openapi\FindMatchByIdFilter();
$findByIdFilter->setId(18126);
$findByIdFilter->setPbp(true);

list($match, $status) = $client->FindMatchByID($findByIdFilter)->wait();

echo "\nPlay By Play partido ID: " . $match->getId() . "\n";

foreach ($match->getPlays() as $play) {
        $num = $play->getPlayNum();
        $dorsal = $play->getShirtNumber();
        $texto = $play->getText();
        echo "$num º: Dorsal $dorsal $texto" . "\n";
}

// Close the connection
$client->close();
