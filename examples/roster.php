<?php

require dirname(__FILE__).'/vendor/autoload.php';

$server = "oa.dev.acbdata.net:59800";
$client = new \Openapi\OpenApiServiceClient($server, [
    "credentials" => \Grpc\ChannelCredentials::createInsecure(),
]);

$rosterFilter = new \Openapi\LicenseTeamFilter();
$rosterFilter->setTeamId(2);

list($roster, $status) = $client->FindRosterByTeam($rosterFilter)->wait();

echo "Plantilla FC Barcelona Lassa:\n";

// LicenseTeam guarda la relación entre una licencia y un equipo
foreach ($roster->getLicenseTeams() as $licenseTeam) {
        $licenseStr = "";
        $licenseType = "";
        $dorsal = "";

        $license = $licenseTeam->getLicense();
        if ($license) {
                $licenseStr = $license->getLicenseStr();
                $dorsal = $license->getShirtNumber();

                if ($license->getType()) {
                        $licenseType = $license->getType()->getComment();
                }
        }

        echo "$dorsal - $licenseStr [$licenseType]\n";
}

// Close the connection
$client->close();
