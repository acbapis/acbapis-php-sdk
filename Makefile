.DEFAULT_GOAL := all

DEST_PROD_DIR ?= ../../../SDK/openapi-php-sdk

all: build git newprod

build:
	@echo "Built new php sdk version"

git:
	git add . && git commit -am "Updated lib" && git push

newprod:
	@for d in lib/Openapi lib/Common lib/Status lib/Google lib/GPBMetadata; do \
		cp -a $$d $(DEST_PROD_DIR)/lib ; \
	done
	@ for d in examples; do \
		cp -a $$d $(DEST_PROD_DIR)/examples ; \
	done
